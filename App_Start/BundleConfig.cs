﻿using System.Web;
using System.Web.Optimization;

namespace Suppliro
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Content/js/jquery-1.11.1.min.js",
                        "~/Content/js/jquery-{version}.js",
                        "~/Content/js/smoothscroll.js",
                        "~/Content/js/query.debouncedresize.js",
                        "~/Content/js/retina.min.js",
                        "~/Content/js/jquery.placeholder.js",
                        "~/Content/js/jquery.hoverIntent.min.js",
                        "~/Content/js/twitter/jquery.tweet.min.js",
                        "~/Content/js/jquery.flexslider-min.js",
                        "~/Content/js/owl.carousel.min.js",
                        "~/Content/js/jquery.unobtrusive-ajax.min.js",
                        "~/Content/js/main.js",
                        "~/Content/js/site.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Content/js/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Content/js/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Content/js/bootstrap.js",
                      "~/Content/js/respond.min.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/font-awesome.min.css",
                      "~/Content/Site.css",
                      "~/Content/style.css",
                      "~/Content/responsive.css",
                      "~/Content/jquery-ui.css"));
        }
    }
}
