﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Suppliro.Search
{
    public class SearchModel
    {
        public int ProductId { get; set; }

        public string Code { get; set; }

        public string SupplierCode { get; set; }

        public string CategoryCode { get; set; }        

        public double MarkedUpPrice { get; set; }

        public double MinOrder { get; set; }

        public string Unit { get; set; }

        public double UoMValue { get; set; }

        public string UoMDefaultPriceValue { get; set; }

        public string UoMDefaultPriceValueDecimal { get; set; }
    }
}