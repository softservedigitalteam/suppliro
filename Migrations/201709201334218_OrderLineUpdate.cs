namespace Suppliro.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OrderLineUpdate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.OrderLineEdited", "OrderLineId", c => c.Int(nullable: false));
            AddColumn("dbo.SupplierOrderLineEdited", "SupplierOrderLineId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.SupplierOrderLineEdited", "SupplierOrderLineId");
            DropColumn("dbo.OrderLineEdited", "OrderLineId");
        }
    }
}
