namespace Suppliro.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            //CreateTable(
            //    "dbo.Cart",
            //    c => new
            //        {
            //            RecordId = c.Int(nullable: false, identity: true),
            //            CartId = c.String(),
            //            ProductId = c.Int(nullable: false),
            //            ProductCode = c.String(),
            //            Count = c.Int(nullable: false),
            //            DateCreated = c.DateTime(nullable: false),
            //        })
            //    .PrimaryKey(t => t.RecordId)
            //    .ForeignKey("dbo.Product", t => t.ProductId)
            //    .Index(t => t.ProductId);
            
            //CreateTable(
            //    "dbo.Product",
            //    c => new
            //        {
            //            Id = c.Int(nullable: false, identity: true),
            //            Code = c.String(),
            //            DisplayName = c.String(),
            //            Description = c.String(),
            //            OriginalPrice = c.Double(),
            //            MarkUpPercentage = c.Double(nullable: false),
            //            MarkedUpPrice = c.Double(nullable: false),
            //            UoMId = c.Int(nullable: false),
            //            Unit = c.String(),
            //            UoMDefaultId = c.Int(nullable: false),
            //            UoMDefaultCode = c.String(),
            //            UoMDefaultPrice = c.Double(nullable: false),
            //            UoMDefaultPriceValue = c.String(),
            //            MinOrder = c.Double(nullable: false),
            //            Vat = c.Double(nullable: false),
            //            InStock = c.Boolean(nullable: false),
            //            CategoryId = c.Int(nullable: false),
            //            CategoryName = c.String(),
            //            SupplierId = c.Int(nullable: false),
            //        })
            //    .PrimaryKey(t => t.Id)
            //    .ForeignKey("dbo.Category", t => t.CategoryId)
            //    .ForeignKey("dbo.Supplier", t => t.SupplierId)
            //    .Index(t => t.CategoryId)
            //    .Index(t => t.SupplierId);
            
            //CreateTable(
            //    "dbo.Category",
            //    c => new
            //        {
            //            Id = c.Int(nullable: false, identity: true),
            //            Name = c.String(nullable: false),
            //            Description = c.String(),
            //            ParentId = c.Int(),
            //        })
            //    .PrimaryKey(t => t.Id)
            //    .ForeignKey("dbo.Category", t => t.ParentId)
            //    .Index(t => t.ParentId);
            
            //CreateTable(
            //    "dbo.Supplier",
            //    c => new
            //        {
            //            Id = c.Int(nullable: false, identity: true),
            //            UserId = c.String(maxLength: 128),
            //            TradingName = c.String(),
            //            RegisteredName = c.String(nullable: false),
            //            VATNumber = c.String(),
            //            TelNumber = c.String(),
            //            CellNumber = c.String(),
            //            EmailAddress = c.String(nullable: false),
            //            FaxNumber = c.String(),
            //            IsActive = c.Boolean(nullable: false),
            //            PhysicalAddress1 = c.String(),
            //            PhysicalAddress2 = c.String(),
            //            PhysicalPostalCode = c.String(),
            //            PhysicalRegion = c.String(),
            //            PhysicalTown = c.String(),
            //            PostalAddress1 = c.String(),
            //            PostalAddress2 = c.String(),
            //            PostalPostalCode = c.String(),
            //            PostalRegion = c.String(),
            //            PostalTown = c.String(),
            //        })
            //    .PrimaryKey(t => t.Id)
            //    .ForeignKey("dbo.AspNetUsers", t => t.UserId)
            //    .Index(t => t.UserId);
            
            //CreateTable(
            //    "dbo.AspNetUsers",
            //    c => new
            //        {
            //            Id = c.String(nullable: false, maxLength: 128),
            //            UserName = c.String(nullable: false, maxLength: 256),
            //            PhoneNumber = c.String(),
            //            IsSupplier = c.Boolean(nullable: false),
            //            Email = c.String(maxLength: 256),
            //            EmailConfirmed = c.Boolean(nullable: false),
            //            PasswordHash = c.String(),
            //            SecurityStamp = c.String(),
            //            PhoneNumberConfirmed = c.Boolean(nullable: false),
            //            TwoFactorEnabled = c.Boolean(nullable: false),
            //            LockoutEndDateUtc = c.DateTime(),
            //            LockoutEnabled = c.Boolean(nullable: false),
            //            AccessFailedCount = c.Int(nullable: false),
            //        })
            //    .PrimaryKey(t => t.Id)
            //    .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            //CreateTable(
            //    "dbo.AspNetUserClaims",
            //    c => new
            //        {
            //            Id = c.Int(nullable: false, identity: true),
            //            UserId = c.String(nullable: false, maxLength: 128),
            //            ClaimType = c.String(),
            //            ClaimValue = c.String(),
            //        })
            //    .PrimaryKey(t => t.Id)
            //    .ForeignKey("dbo.AspNetUsers", t => t.UserId)
            //    .Index(t => t.UserId);
            
            //CreateTable(
            //    "dbo.AspNetUserLogins",
            //    c => new
            //        {
            //            LoginProvider = c.String(nullable: false, maxLength: 128),
            //            ProviderKey = c.String(nullable: false, maxLength: 128),
            //            UserId = c.String(nullable: false, maxLength: 128),
            //        })
            //    .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
            //    .ForeignKey("dbo.AspNetUsers", t => t.UserId)
            //    .Index(t => t.UserId);
            
            //CreateTable(
            //    "dbo.AspNetUserRoles",
            //    c => new
            //        {
            //            UserId = c.String(nullable: false, maxLength: 128),
            //            RoleId = c.String(nullable: false, maxLength: 128),
            //        })
            //    .PrimaryKey(t => new { t.UserId, t.RoleId })
            //    .ForeignKey("dbo.AspNetUsers", t => t.UserId)
            //    .ForeignKey("dbo.AspNetRoles", t => t.RoleId)
            //    .Index(t => t.UserId)
            //    .Index(t => t.RoleId);
            
            //CreateTable(
            //    "dbo.CompanyInformation",
            //    c => new
            //        {
            //            Id = c.Int(nullable: false, identity: true),
            //            TradingName = c.String(),
            //            RegisteredName = c.String(),
            //            VATNumber = c.String(),
            //            TelNumber = c.String(),
            //            VATPercentage = c.Double(nullable: false),
            //            AccountName = c.String(),
            //            AccountNumber = c.String(),
            //            BankName = c.String(),
            //            BranchCode = c.String(),
            //            BranchName = c.String(),
            //            Address1 = c.String(),
            //            Address2 = c.String(),
            //            PostalCode = c.String(),
            //            Region = c.String(),
            //            City = c.String(),
            //        })
            //    .PrimaryKey(t => t.Id);
            
            //CreateTable(
            //    "dbo.Employee",
            //    c => new
            //        {
            //            Id = c.Int(nullable: false, identity: true),
            //            CompanyId = c.Int(nullable: false),
            //            Name = c.String(),
            //            Surname = c.String(),
            //            IDNumber = c.Int(nullable: false),
            //            Email = c.String(),
            //            PersonalEmail = c.String(),
            //            Position = c.String(),
            //            IsDirector = c.Boolean(nullable: false),
            //            CelNumber = c.String(),
            //            TelNumber = c.String(),
            //            CompanyInformation_Id = c.Int(),
            //        })
            //    .PrimaryKey(t => t.Id)
            //    .ForeignKey("dbo.CompanyInformation", t => t.CompanyInformation_Id)
            //    .Index(t => t.CompanyInformation_Id);
            
            //CreateTable(
            //    "dbo.Contact",
            //    c => new
            //        {
            //            Id = c.Int(nullable: false, identity: true),
            //            FullName = c.String(),
            //            CustomerId = c.Int(),
            //            Email = c.String(),
            //            Message = c.String(),
            //        })
            //    .PrimaryKey(t => t.Id)
            //    .ForeignKey("dbo.Customer", t => t.CustomerId)
            //    .Index(t => t.CustomerId);
            
            //CreateTable(
            //    "dbo.Customer",
            //    c => new
            //        {
            //            Id = c.Int(nullable: false, identity: true),
            //            UserId = c.String(maxLength: 128),
            //            CustomerCode = c.String(),
            //            Color = c.String(),
            //            TradingName = c.String(),
            //            RegisteredName = c.String(),
            //            VATNumber = c.String(),
            //            TelNumber = c.String(),
            //            CellNumber = c.String(),
            //            BuyersName = c.String(),
            //            BuyersIDNumber = c.String(),
            //            EmailAddress = c.String(),
            //            FaxNumber = c.String(),
            //            PhysicalAddress1 = c.String(),
            //            PhysicalAddress2 = c.String(),
            //            PhysicalPostalCode = c.String(),
            //            PhysicalRegion = c.String(),
            //            PhysicalTown = c.String(),
            //            PostalAddress1 = c.String(),
            //            PostalAddress2 = c.String(),
            //            PostalPostalCode = c.String(),
            //            PostalRegion = c.String(),
            //            PostalTown = c.String(),
            //        })
            //    .PrimaryKey(t => t.Id)
            //    .ForeignKey("dbo.AspNetUsers", t => t.UserId)
            //    .Index(t => t.UserId);
            
            //CreateTable(
            //    "dbo.InvoiceLine",
            //    c => new
            //        {
            //            Id = c.Int(nullable: false, identity: true),
            //            InvoiceId = c.Int(nullable: false),
            //            OrderId = c.Int(nullable: false),
            //            OrderLineId = c.Int(nullable: false),
            //            OrderNumber = c.String(),
            //            DateAdded = c.DateTime(nullable: false),
            //            ProductId = c.Int(nullable: false),
            //            ProductCode = c.String(),
            //            ProductUnit = c.String(),
            //            ProductUnitPrice = c.Double(nullable: false),
            //            Quantity = c.Double(nullable: false),
            //            SubTotal = c.Double(nullable: false),
            //            VATAmount = c.Double(nullable: false),
            //            TotalAmount = c.Double(nullable: false),
            //        })
            //    .PrimaryKey(t => t.Id)
            //    .ForeignKey("dbo.Invoice", t => t.InvoiceId)
            //    .Index(t => t.InvoiceId);
            
            //CreateTable(
            //    "dbo.InvoiceNumber",
            //    c => new
            //        {
            //            Id = c.Int(nullable: false, identity: true),
            //            Number = c.Int(nullable: false),
            //        })
            //    .PrimaryKey(t => t.Id);
            
            //CreateTable(
            //    "dbo.Invoice",
            //    c => new
            //        {
            //            Id = c.Int(nullable: false, identity: true),
            //            InvoiceNumber = c.String(),
            //            VATAmount = c.Double(nullable: false),
            //            SubTotalAmount = c.Double(nullable: false),
            //            TotalAmount = c.Double(nullable: false),
            //            DateCreated = c.DateTime(nullable: false),
            //            DateCompleted = c.DateTime(),
            //            DateEmailed = c.DateTime(),
            //            IsCompleted = c.Boolean(nullable: false),
            //            CustomerId = c.Int(nullable: false),
            //            CustomerCode = c.String(),
            //            CustomerTradingName = c.String(),
            //            CustomerRegisteredName = c.String(),
            //            CustomerVATNumber = c.String(),
            //            CustomerPhysicalAddress1 = c.String(),
            //            CustomerPhysicalAddress2 = c.String(),
            //            CustomerPhysicalPostalCode = c.String(),
            //            CustomerPhysicalRegion = c.String(),
            //            CustomerPhysicalTown = c.String(),
            //            CompanyId = c.Int(nullable: false),
            //            CompanyName = c.String(),
            //            CompanyVATNumber = c.String(),
            //            CompanyAddress1 = c.String(),
            //            CompanyAddress2 = c.String(),
            //            CompanyPostalCode = c.String(),
            //            CompanyRegion = c.String(),
            //            CompanyCity = c.String(),
            //        })
            //    .PrimaryKey(t => t.Id)
            //    .ForeignKey("dbo.CompanyInformation", t => t.CompanyId)
            //    .ForeignKey("dbo.Customer", t => t.CustomerId)
            //    .Index(t => t.CustomerId)
            //    .Index(t => t.CompanyId);
            
            //CreateTable(
            //    "dbo.OrderLine",
            //    c => new
            //        {
            //            Id = c.Int(nullable: false, identity: true),
            //            OrderId = c.Int(nullable: false),
            //            ProductId = c.Int(nullable: false),
            //            ProductCode = c.String(),
            //            SupplierId = c.Int(nullable: false),
            //            SupplierTradingName = c.String(),
            //            Unit = c.String(),
            //            Quantity = c.Double(nullable: false),
            //            UnitPrice = c.Double(nullable: false),
            //            Amount = c.Double(nullable: false),
            //            VATAmount = c.Double(nullable: false),
            //            IsCompleted = c.Boolean(nullable: false),
            //            IsOrdered = c.Boolean(nullable: false),
            //            IsReplaced = c.Boolean(nullable: false),
            //        })
            //    .PrimaryKey(t => t.Id)
            //    .ForeignKey("dbo.Order", t => t.OrderId)
            //    .ForeignKey("dbo.Product", t => t.ProductId)
            //    .ForeignKey("dbo.Supplier", t => t.SupplierId)
            //    .Index(t => t.OrderId)
            //    .Index(t => t.ProductId)
            //    .Index(t => t.SupplierId);
            
            //CreateTable(
            //    "dbo.Order",
            //    c => new
            //        {
            //            Id = c.Int(nullable: false, identity: true),
            //            OrderNumber = c.String(),
            //            DatePlaced = c.DateTime(nullable: false),
            //            SubTotal = c.Double(nullable: false),
            //            VATAmount = c.Double(nullable: false),
            //            Total = c.Double(nullable: false),
            //            UserId = c.String(),
            //            IsComplete = c.Boolean(nullable: false),
            //            DateCompleted = c.DateTime(),
            //            IsInvoiced = c.Boolean(nullable: false),
            //            PercentageComplete = c.Double(nullable: false),
            //            CustomerId = c.Int(nullable: false),
            //            CustomerName = c.String(),
            //            TradingName = c.String(),
            //            RegisteredName = c.String(),
            //            VATNumber = c.String(),
            //            TelNumber = c.String(),
            //            BuyersName = c.String(),
            //            BuyersIDNumber = c.String(),
            //            EmailAddress = c.String(),
            //            PhysicalAddress1 = c.String(),
            //            PhysicalAddress2 = c.String(),
            //            PhysicalPostalCode = c.String(),
            //            PhysicalRegion = c.String(),
            //            PhysicalTown = c.String(),
            //        })
            //    .PrimaryKey(t => t.Id)
            //    .ForeignKey("dbo.Customer", t => t.CustomerId)
            //    .Index(t => t.CustomerId);
            
            //CreateTable(
            //    "dbo.OrderNumber",
            //    c => new
            //        {
            //            OrderNumberId = c.Int(nullable: false, identity: true),
            //            Number = c.Int(nullable: false),
            //        })
            //    .PrimaryKey(t => t.OrderNumberId);
            
            //CreateTable(
            //    "dbo.PickUpForm",
            //    c => new
            //        {
            //            Id = c.Int(nullable: false, identity: true),
            //            DateCreated = c.DateTime(nullable: false),
            //            DateCompleted = c.DateTime(nullable: false),
            //            IsComplete = c.Boolean(nullable: false),
            //            IsEmailed = c.Boolean(nullable: false),
            //            DateMailed = c.DateTime(nullable: false),
            //        })
            //    .PrimaryKey(t => t.Id);
            
            //CreateTable(
            //    "dbo.PickUpFormLine",
            //    c => new
            //        {
            //            Id = c.Int(nullable: false, identity: true),
            //            PickUpFormId = c.Int(nullable: false),
            //            OrderId = c.Int(nullable: false),
            //            OrderLineId = c.Int(nullable: false),
            //            SupplierId = c.Int(nullable: false),
            //            SupplierTradingName = c.String(),
            //            CustomerId = c.Int(nullable: false),
            //            CustomerCode = c.String(),
            //            ProductId = c.Int(nullable: false),
            //            ProductCode = c.String(),
            //            Qty = c.Double(nullable: false),
            //        })
            //    .PrimaryKey(t => t.Id)
            //    .ForeignKey("dbo.Customer", t => t.CustomerId)
            //    .ForeignKey("dbo.Supplier", t => t.SupplierId)
            //    .ForeignKey("dbo.PickUpForm", t => t.PickUpFormId)
            //    .Index(t => t.PickUpFormId)
            //    .Index(t => t.SupplierId)
            //    .Index(t => t.CustomerId);
            
            //CreateTable(
            //    "dbo.ProductHistory",
            //    c => new
            //        {
            //            Id = c.Int(nullable: false, identity: true),
            //            ProductId = c.Int(nullable: false),
            //            Price = c.Double(nullable: false),
            //            Date = c.DateTime(nullable: false),
            //        })
            //    .PrimaryKey(t => t.Id);
            
            //CreateTable(
            //    "dbo.AspNetRoles",
            //    c => new
            //        {
            //            Id = c.String(nullable: false, maxLength: 128),
            //            Name = c.String(nullable: false, maxLength: 256),
            //        })
            //    .PrimaryKey(t => t.Id)
            //    .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            //CreateTable(
            //    "dbo.SignUp",
            //    c => new
            //        {
            //            Id = c.Int(nullable: false, identity: true),
            //            BusinessName = c.String(),
            //            ContactPerson = c.String(),
            //            Number = c.String(),
            //            Email = c.String(),
            //        })
            //    .PrimaryKey(t => t.Id);
            
            //CreateTable(
            //    "dbo.SupplierOrderLine",
            //    c => new
            //        {
            //            Id = c.Int(nullable: false, identity: true),
            //            SupplierOrderId = c.Int(nullable: false),
            //            OrderId = c.Int(nullable: false),
            //            OrderLineId = c.Int(nullable: false),
            //            OrderNumber = c.String(),
            //            CustomerId = c.Int(nullable: false),
            //            CustomerName = c.String(),
            //            CustomerColor = c.String(),
            //            IsReplaced = c.Boolean(nullable: false),
            //            ProductId = c.Int(nullable: false),
            //            ProductCode = c.String(),
            //            ProductPrice = c.Double(nullable: false),
            //            ProductUnit = c.String(),
            //            Quantity = c.Double(nullable: false),
            //        })
            //    .PrimaryKey(t => t.Id)
            //    .ForeignKey("dbo.Customer", t => t.CustomerId)
            //    .ForeignKey("dbo.SupplierOrder", t => t.SupplierOrderId)
            //    .Index(t => t.SupplierOrderId)
            //    .Index(t => t.CustomerId);
            
            //CreateTable(
            //    "dbo.SupplierOrderNumber",
            //    c => new
            //        {
            //            Id = c.Int(nullable: false, identity: true),
            //            Number = c.Int(nullable: false),
            //        })
            //    .PrimaryKey(t => t.Id);
            
            //CreateTable(
            //    "dbo.SupplierOrder",
            //    c => new
            //        {
            //            Id = c.Int(nullable: false, identity: true),
            //            SupplierId = c.Int(nullable: false),
            //            SupplierEmails = c.String(),
            //            OrderNumber = c.String(),
            //            DateCreated = c.DateTime(nullable: false),
            //            IsEmailed = c.Boolean(nullable: false),
            //            IsComplete = c.Boolean(nullable: false),
            //            DateMailed = c.DateTime(),
            //            SupplierInvoiceNumber = c.String(),
            //            DateCompleted = c.DateTime(),
            //        })
            //    .PrimaryKey(t => t.Id)
            //    .ForeignKey("dbo.Supplier", t => t.SupplierId)
            //    .Index(t => t.SupplierId);
            
            //CreateTable(
            //    "dbo.UoMMapper",
            //    c => new
            //        {
            //            Id = c.Int(nullable: false, identity: true),
            //            UoMId = c.Int(nullable: false),
            //            CompareToUoMId = c.Int(nullable: false),
            //            CompareToUoMCode = c.String(),
            //            Value = c.Double(nullable: false),
            //        })
            //    .PrimaryKey(t => t.Id)
            //    .ForeignKey("dbo.UoM", t => t.UoMId)
            //    .Index(t => t.UoMId);
            
            //CreateTable(
            //    "dbo.UoM",
            //    c => new
            //        {
            //            Id = c.Int(nullable: false, identity: true),
            //            Code = c.String(),
            //            DefaultUoMId = c.Int(nullable: false),
            //        })
            //    .PrimaryKey(t => t.Id);
            
            //CreateTable(
            //    "dbo.UploadedProduct",
            //    c => new
            //        {
            //            UploadedProductId = c.Int(nullable: false, identity: true),
            //            ProductId = c.Int(nullable: false),
            //            SupplierId = c.Int(nullable: false),
            //            Category = c.String(),
            //            Code = c.String(),
            //            Unit = c.String(),
            //            MinOrder = c.Decimal(nullable: false, precision: 18, scale: 2),
            //            Price = c.Double(nullable: false),
            //        })
            //    .PrimaryKey(t => t.UploadedProductId)
            //    .ForeignKey("dbo.Supplier", t => t.SupplierId)
            //    .Index(t => t.SupplierId);
            
        }
        
        public override void Down()
        {
            //DropForeignKey("dbo.UploadedProduct", "SupplierId", "dbo.Supplier");
            //DropForeignKey("dbo.UoMMapper", "UoMId", "dbo.UoM");
            //DropForeignKey("dbo.SupplierOrderLine", "SupplierOrderId", "dbo.SupplierOrder");
            //DropForeignKey("dbo.SupplierOrder", "SupplierId", "dbo.Supplier");
            //DropForeignKey("dbo.SupplierOrderLine", "CustomerId", "dbo.Customer");
            //DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            //DropForeignKey("dbo.PickUpFormLine", "PickUpFormId", "dbo.PickUpForm");
            //DropForeignKey("dbo.PickUpFormLine", "SupplierId", "dbo.Supplier");
            //DropForeignKey("dbo.PickUpFormLine", "CustomerId", "dbo.Customer");
            //DropForeignKey("dbo.OrderLine", "SupplierId", "dbo.Supplier");
            //DropForeignKey("dbo.OrderLine", "ProductId", "dbo.Product");
            //DropForeignKey("dbo.OrderLine", "OrderId", "dbo.Order");
            //DropForeignKey("dbo.Order", "CustomerId", "dbo.Customer");
            //DropForeignKey("dbo.InvoiceLine", "InvoiceId", "dbo.Invoice");
            //DropForeignKey("dbo.Invoice", "CustomerId", "dbo.Customer");
            //DropForeignKey("dbo.Invoice", "CompanyId", "dbo.CompanyInformation");
            //DropForeignKey("dbo.Contact", "CustomerId", "dbo.Customer");
            //DropForeignKey("dbo.Customer", "UserId", "dbo.AspNetUsers");
            //DropForeignKey("dbo.Employee", "CompanyInformation_Id", "dbo.CompanyInformation");
            //DropForeignKey("dbo.Cart", "ProductId", "dbo.Product");
            //DropForeignKey("dbo.Product", "SupplierId", "dbo.Supplier");
            //DropForeignKey("dbo.Supplier", "UserId", "dbo.AspNetUsers");
            //DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            //DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            //DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            //DropForeignKey("dbo.Product", "CategoryId", "dbo.Category");
            //DropForeignKey("dbo.Category", "ParentId", "dbo.Category");
            //DropIndex("dbo.UploadedProduct", new[] { "SupplierId" });
            //DropIndex("dbo.UoMMapper", new[] { "UoMId" });
            //DropIndex("dbo.SupplierOrder", new[] { "SupplierId" });
            //DropIndex("dbo.SupplierOrderLine", new[] { "CustomerId" });
            //DropIndex("dbo.SupplierOrderLine", new[] { "SupplierOrderId" });
            //DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            //DropIndex("dbo.PickUpFormLine", new[] { "CustomerId" });
            //DropIndex("dbo.PickUpFormLine", new[] { "SupplierId" });
            //DropIndex("dbo.PickUpFormLine", new[] { "PickUpFormId" });
            //DropIndex("dbo.Order", new[] { "CustomerId" });
            //DropIndex("dbo.OrderLine", new[] { "SupplierId" });
            //DropIndex("dbo.OrderLine", new[] { "ProductId" });
            //DropIndex("dbo.OrderLine", new[] { "OrderId" });
            //DropIndex("dbo.Invoice", new[] { "CompanyId" });
            //DropIndex("dbo.Invoice", new[] { "CustomerId" });
            //DropIndex("dbo.InvoiceLine", new[] { "InvoiceId" });
            //DropIndex("dbo.Customer", new[] { "UserId" });
            //DropIndex("dbo.Contact", new[] { "CustomerId" });
            //DropIndex("dbo.Employee", new[] { "CompanyInformation_Id" });
            //DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            //DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            //DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            //DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            //DropIndex("dbo.AspNetUsers", "UserNameIndex");
            //DropIndex("dbo.Supplier", new[] { "UserId" });
            //DropIndex("dbo.Category", new[] { "ParentId" });
            //DropIndex("dbo.Product", new[] { "SupplierId" });
            //DropIndex("dbo.Product", new[] { "CategoryId" });
            //DropIndex("dbo.Cart", new[] { "ProductId" });
            //DropTable("dbo.UploadedProduct");
            //DropTable("dbo.UoM");
            //DropTable("dbo.UoMMapper");
            //DropTable("dbo.SupplierOrder");
            //DropTable("dbo.SupplierOrderNumber");
            //DropTable("dbo.SupplierOrderLine");
            //DropTable("dbo.SignUp");
            //DropTable("dbo.AspNetRoles");
            //DropTable("dbo.ProductHistory");
            //DropTable("dbo.PickUpFormLine");
            //DropTable("dbo.PickUpForm");
            //DropTable("dbo.OrderNumber");
            //DropTable("dbo.Order");
            //DropTable("dbo.OrderLine");
            //DropTable("dbo.Invoice");
            //DropTable("dbo.InvoiceNumber");
            //DropTable("dbo.InvoiceLine");
            //DropTable("dbo.Customer");
            //DropTable("dbo.Contact");
            //DropTable("dbo.Employee");
            //DropTable("dbo.CompanyInformation");
            //DropTable("dbo.AspNetUserRoles");
            //DropTable("dbo.AspNetUserLogins");
            //DropTable("dbo.AspNetUserClaims");
            //DropTable("dbo.AspNetUsers");
            //DropTable("dbo.Supplier");
            //DropTable("dbo.Category");
            //DropTable("dbo.Product");
            //DropTable("dbo.Cart");
        }
    }
}
