namespace Suppliro.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SupplierOrderLinesClassEdit : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SupplierOrderLine", "Edited", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.SupplierOrderLine", "Edited");
        }
    }
}
