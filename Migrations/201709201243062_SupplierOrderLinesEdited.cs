namespace Suppliro.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SupplierOrderLinesEdited : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.SupplierOrderLineEdited",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SupplierOrderId = c.Int(nullable: false),
                        OrderId = c.Int(nullable: false),
                        OrderLineId = c.Int(nullable: false),
                        OrderNumber = c.String(),
                        CustomerId = c.Int(nullable: false),
                        CustomerName = c.String(),
                        CustomerColor = c.String(),
                        IsReplaced = c.Boolean(nullable: false),
                        DateReplaced = c.DateTime(),
                        ReplacedProductId = c.Int(),
                        ReplacedProductCode = c.String(),
                        ProductId = c.Int(nullable: false),
                        ProductCode = c.String(),
                        ProductPrice = c.Double(nullable: false),
                        ProductUnit = c.String(),
                        Quantity = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Customer", t => t.CustomerId)
                .Index(t => t.CustomerId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SupplierOrderLineEdited", "CustomerId", "dbo.Customer");
            DropIndex("dbo.SupplierOrderLineEdited", new[] { "CustomerId" });
            DropTable("dbo.SupplierOrderLineEdited");
        }
    }
}
