namespace Suppliro.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SupplierOrderClassEdit : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SupplierOrder", "Edited", c => c.Boolean(nullable: false));
            DropColumn("dbo.SupplierOrderLine", "Edited");
        }
        
        public override void Down()
        {
            AddColumn("dbo.SupplierOrderLine", "Edited", c => c.Boolean(nullable: false));
            DropColumn("dbo.SupplierOrder", "Edited");
        }
    }
}
