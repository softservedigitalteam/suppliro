namespace Suppliro.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class QuantityEdited : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.OrderLineEdited", "QuantityEdited", c => c.Boolean(nullable: false));
            AddColumn("dbo.SupplierOrderLineEdited", "QuantityEdited", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.SupplierOrderLineEdited", "QuantityEdited");
            DropColumn("dbo.OrderLineEdited", "QuantityEdited");
        }
    }
}
