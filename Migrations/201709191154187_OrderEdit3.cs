namespace Suppliro.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OrderEdit3 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.OrderLineEdited",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        OrderId = c.Int(nullable: false),
                        ProductId = c.Int(nullable: false),
                        ProductCode = c.String(),
                        SupplierId = c.Int(nullable: false),
                        SupplierTradingName = c.String(),
                        Unit = c.String(),
                        Quantity = c.Double(nullable: false),
                        UnitPrice = c.Double(nullable: false),
                        Amount = c.Double(nullable: false),
                        VATAmount = c.Double(nullable: false),
                        IsCompleted = c.Boolean(nullable: false),
                        IsOrdered = c.Boolean(nullable: false),
                        IsReplaced = c.Boolean(nullable: false),
                        ReplacedProductId = c.Int(),
                        ReplacedProductCode = c.String(),
                        DateReplaced = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Order", t => t.OrderId)
                .ForeignKey("dbo.Product", t => t.ProductId)
                .ForeignKey("dbo.Supplier", t => t.SupplierId)
                .Index(t => t.OrderId)
                .Index(t => t.ProductId)
                .Index(t => t.SupplierId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.OrderLineEdited", "SupplierId", "dbo.Supplier");
            DropForeignKey("dbo.OrderLineEdited", "ProductId", "dbo.Product");
            DropForeignKey("dbo.OrderLineEdited", "OrderId", "dbo.Order");
            DropIndex("dbo.OrderLineEdited", new[] { "SupplierId" });
            DropIndex("dbo.OrderLineEdited", new[] { "ProductId" });
            DropIndex("dbo.OrderLineEdited", new[] { "OrderId" });
            DropTable("dbo.OrderLineEdited");
        }
    }
}
