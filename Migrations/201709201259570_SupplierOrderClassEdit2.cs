namespace Suppliro.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SupplierOrderClassEdit2 : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.SupplierOrderLineEdited", "SupplierOrderId");
            AddForeignKey("dbo.SupplierOrderLineEdited", "SupplierOrderId", "dbo.SupplierOrder", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SupplierOrderLineEdited", "SupplierOrderId", "dbo.SupplierOrder");
            DropIndex("dbo.SupplierOrderLineEdited", new[] { "SupplierOrderId" });
        }
    }
}
