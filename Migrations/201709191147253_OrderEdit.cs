namespace Suppliro.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OrderEdit : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Order", "Edited", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Order", "Edited");
        }
    }
}
