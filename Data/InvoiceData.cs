﻿using PagedList;
using Suppliro.Context;
using Suppliro.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Suppliro.Data
{
    public class InvoiceData
    {
        private DBContext _db;

        public InvoiceData(DBContext context)
        {
            _db = context;
        }

        public string GenerateInvoiceNumber(string customerCode)
        {
            string invoiceNumber = "";

            //There should only be one order number from which we track
            InvoiceNumber _num = _db.InvoiceNumbers.FirstOrDefault();

            if (_num == null)
            {
                _num = new InvoiceNumber();
                _num.Number = 1;

                _db.InvoiceNumbers.Add(_num);
                _db.SaveChanges();
            }
            else
            {
                _num.Number += 1;

                _db.Entry(_num).State = EntityState.Modified;
                _db.SaveChanges();
            }

            //Incremented the order number....Now to return the general order number that will be displayed
            invoiceNumber = string.Format("{0}{1}", customerCode, _num.Number);

            return invoiceNumber;
        }

        public InvoiceLine CheckExistingLine(int orderId, int orderLineId)
        {
            return _db.InvoiceLines
                .FirstOrDefault(x => x.OrderId == orderId 
                && x.OrderLineId == orderLineId);
        }

        public void SaveInvoice(Invoice model)
        {
            if (model.Id > 0)
            {
                _db.Entry(model).State = EntityState.Modified;
                _db.SaveChanges();
            }
            else
            {
                _db.Invoices.Add(model);
                _db.SaveChanges();
            }
        }

        public Invoice GetInvoiceById(int id)
        {
            return _db.Invoices
                .Where(x => x.Id == id)
                .Include(y => y.InvoiceLines)
                .Include(y => y.Customer)
                .FirstOrDefault();
        }

        public Invoice GetOpenInvoice(int customerId)
        {
            return _db.Invoices
                .Where(x => x.CustomerId == customerId 
                && x.IsCompleted == false)
                .Include(y => y.InvoiceLines)
                .FirstOrDefault();
        }

        public List<Invoice> GetInvoicesCompletedToday()
        {
            List<Invoice> _list = new List<Invoice>();

            var endDate = DateTime.Today.AddDays(1).AddTicks(-1);

            var query = (from s in _db.Invoices
                         where (s.DateCompleted >= DateTime.Today && s.DateCompleted <= endDate) || s.IsCompleted == false
                         select s).ToList();

            query.ForEach(item =>
            {
                _list.Add(item);
            });

            return _list;
        }

        public List<Invoice> GetOpenInvoices()
        {
            return _db.Invoices
                .Where(x => x.IsCompleted == false)
                .Include(y => y.InvoiceLines)
                .ToList();
        }

        public List<Invoice> GetClosedInvoices()
        {
            return _db.Invoices
                .Where(x => x.IsCompleted == true)
                .OrderByDescending(x => x.DateCompleted)
                .Include(y => y.InvoiceLines)
                .ToList();
        }

        public List<Invoice> GetInvoices(int customerId)
        {
            return _db.Invoices
                .Where(x => x.CustomerId == customerId && x.IsCompleted == true)
                .ToList();
        }

        public List<Invoice> GetInvoicesWithLines(int customerId)
        {
            return _db.Invoices
                .Where(x => x.CustomerId == customerId && x.IsCompleted == true)
                .Include(y => y.InvoiceLines)
                .ToList();
        }

        public List<Customer> CustomersWithProforma()
        {
            List<Customer> _list = new List<Customer>();

            var query = (from inv in _db.Invoices
                         join cus in _db.Customers on inv.CustomerId equals cus.Id
                         where inv.IsCompleted == true
                         group cus by new { cus.Id, cus.TradingName } into grouped
                         select new
                         {
                             grouped.Key.Id,
                             grouped.Key.TradingName
                         }).ToList();

            foreach (var item in query) //retrieve each item and assign to list
            {
                _list.Add(new Customer()
                {
                    Id = item.Id,
                    TradingName = item.TradingName
                });
            }

            return _list;
        }

        public InvoiceLine GetInvoiceLineByOrderLineId(int orderId, int orderLineId)
        {
            return _db.InvoiceLines.FirstOrDefault(x => 
            x.OrderId == orderId &&
            x.OrderLineId == orderLineId);
        }

        public void SaveInvoiceLine(InvoiceLine model)
        {
            if (model.Id > 0)
            {
                _db.Entry(model).State = EntityState.Modified;
                _db.SaveChanges();
            }
            else
            {
                _db.InvoiceLines.Add(model);
                _db.SaveChanges();
            }
        }

        public void DeleteLine(InvoiceLine model)
        {
            _db.InvoiceLines.Remove(model);
            _db.SaveChanges();
        }

        public StaticPagedList<Invoice> GetInvoicesByCustomerIdPaged(int customerId, int page, int pageSize)
        {
            var invoices = _db.Invoices
                   .Where(x => x.CustomerId == customerId && x.IsCompleted)
                   .OrderByDescending(x => x.DateCompleted)
                   .Skip((page - 1) * pageSize)
                   .Take(pageSize);

            var count = _db.Invoices.Where(x => x.CustomerId == customerId && x.IsCompleted).Count();

            return new StaticPagedList<Invoice>(invoices, page, pageSize, count);
        }
    }
}