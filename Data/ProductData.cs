﻿using Suppliro.Context;
using Suppliro.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using Suppliro.Search;

namespace Suppliro.Data
{
    /// <summary>
    /// Product Data 
    /// </summary>
    public class ProductData
    {
        /// <summary>
        /// Database Context
        /// </summary>
        private DBContext _db;

        /// <summary>
        /// Product Data Constructor
        /// </summary>
        public ProductData()
        {
            _db = new DBContext();
        }

        /// <summary>
        /// Gets Product by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Product GetProductById(int id)
        {
            return _db.Products.Where(x => x.Id == id).FirstOrDefault();
        }

        /// <summary>
        /// Gets Stock Sheet by Customer Id
        /// </summary>
        /// <param name="id">Customer Id</param>
        /// <returns></returns>
        public List<Product> GetStockSheetByCustomerId(int id)
        {
            List<Product> _list = new List<Product>();

            var query = (from p in _db.Products
                         join s in _db.StockSheets on p.Id equals s.ProductId
                         where s.CustomerId == id && p.InStock == true
                         select p)
                         .Include(x => x.Supplier)
                         .Include(x => x.Category)
                         .OrderByDescending(x => x.Category.Name)
                         .ThenByDescending(x => x.Code)
                         .ToList();

            query.ForEach(item =>
            {
                _list.Add(item);
            });

            return _list;
        }

        /// <summary>
        /// Gets Stock Sheet by User Id
        /// </summary>
        /// <param name="id">User string Id</param>
        /// <returns></returns>
        public List<Product> GetStockSheetByUserId(string id)
        {
            List<Product> _list = new List<Product>();

            var query = (from p in _db.Products
                         join s in _db.StockSheets on p.Id equals s.ProductId
                         where s.UserId == id && p.InStock == true
                         select p).ToList();

            query.ForEach(item =>
            {
                _list.Add(item);
            });

            return _list;
        }

        public Product GetStockSheetItemById(int productId, int customerId, string userId)
        {
            return (from p in _db.Products
                    join s in _db.StockSheets on p.Id equals s.ProductId
                    where p.Id == productId && s.CustomerId == customerId && s.UserId == userId
                    select p).FirstOrDefault();
        }

        public bool RemoveStockSheetItem(int productId, int customerId, string userId)
        {
            bool _removed = false;

            StockSheet model = _db.StockSheets
                .FirstOrDefault(x => x.ProductId == productId
                && x.CustomerId == customerId
                && x.UserId == userId);

            if (model != null)
            {
                _db.StockSheets.Remove(model);
                _db.SaveChanges();

                _removed = true;
            }

            return _removed;
        }

        /// <summary>
        /// Saves or Updates a Stock Sheet
        /// </summary>
        /// <param name="model"></param>
        public void SaveStockSheetItem(StockSheet model)
        {
            if (model.Id > 0)
            {
                _db.Entry(model).State = EntityState.Modified;
                _db.SaveChanges();
            }
            else
            {
                _db.StockSheets.Add(model);
                _db.SaveChanges();
            }
        }
    }
}