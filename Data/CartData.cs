﻿using Suppliro.Context;
using Suppliro.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Suppliro.Data
{
    public class CartData
    {
        private DBContext _db;

        public CartData()
        {
            _db = new DBContext();
        }

        //CartId is the user Id
        public Cart GetCart(string cartId, int productId)
        {
            // Get the matching cart and product instances
            return _db.Carts
                .Where(c => c.CartId == cartId && c.ProductId == productId)
                .Include(x => x.Product)
                .FirstOrDefault();
        }

        public Cart GetCartItemByRecordId(int recordId)
        {
            // Get the matching cart and product instances
            return _db.Carts
                .Where(c => c.RecordId == recordId)
                .Include(x => x.Product)
                .FirstOrDefault();
        }

        public void CreateCart(Cart cart)
        {
            _db.Carts.Add(cart);
            _db.SaveChanges();
        }

        public void UpdateCart(Cart cart)
        {
            _db.Entry(cart).State = EntityState.Modified;
            _db.SaveChanges();
        }

        public void RemoveCart(Cart cart)
        {
            _db.Carts.Remove(cart);
            _db.SaveChanges();
        }

        public void EmptyCart(string shoppingCartId)
        {
            var cartItems = _db.Carts
                .Where(cart => cart.CartId == shoppingCartId);

            foreach (var cartItem in cartItems)
            {
                _db.Carts.Remove(cartItem);
            }
            // Save changes
            _db.SaveChanges();
        }

        public List<Cart> GetCartItems(string shoppingCartId)
        {
            return _db.Carts
                .Where(cart => cart.CartId == shoppingCartId)
                .Include(x => x.Product)
                .Include(x => x.Product.Supplier)
                .ToList();
        }

        public int GetCount(string shoppingCartId)
        {
            // Get the count of each item in the cart and sum them up
            int? count = (from cartItems in _db.Carts
                          where cartItems.CartId == shoppingCartId
                          select (int?)cartItems.Count).Sum();
            // Return 0 if all entries are null
            return count ?? 0;
        }
        public double GetTotal(string shoppingCartId)
        {
            double? total = (from cartItems in _db.Carts
                              where cartItems.CartId == shoppingCartId
                              select (int?)cartItems.Count *
                              cartItems.Product.MarkedUpPrice).Sum();

            return total ?? 0;
        }
    }
}