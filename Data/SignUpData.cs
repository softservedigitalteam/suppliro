﻿using Suppliro.Context;
using Suppliro.Entities;
using Suppliro.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Suppliro.Data
{
    public class SignUpData
    {
        private DBContext _db;

        public SignUpData()
        {
            _db = new DBContext();
        }

        public void SignUp(SignUpViewModel model)
        {
            SignUp _signUp = new SignUp();

            _signUp.BusinessName = model.BusinessName;
            _signUp.ContactPerson = model.ContactPerson;
            _signUp.Email = model.Email;
            _signUp.Number = model.Number;

            _db.SignUps.Add(_signUp);
            _db.SaveChanges();
        }
    }
}