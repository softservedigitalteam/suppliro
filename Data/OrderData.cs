﻿using PagedList;
using Suppliro.Context;
using Suppliro.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Suppliro.Data
{
    public class OrderData
    {
        private DBContext _db;

        public OrderData(DBContext context)
        {
            _db = context;
        }

        public void SaveOrder(Order model)
        {
            if (model.Id > 0)
            {
                _db.Entry(model).State = EntityState.Modified;
                _db.SaveChanges();
            }
            else
            {
                _db.Orders.Add(model);
                _db.SaveChanges();
            }
        }

        public string GenerateOrderNumber()
        {
            string _orderNumber = "";

            //There should only be one order number from which we track
            OrderNumber _num = _db.OrderNumbers.FirstOrDefault();

            if (_num == null)
            {
                _num = new OrderNumber();
                _num.Number = 1;

                _db.OrderNumbers.Add(_num);
                _db.SaveChanges();
            }
            else
            {
                _num.Number += 1;

                _db.Entry(_num).State = EntityState.Modified;
                _db.SaveChanges();
            }

            //Incremented the order number....Now to return the general order number that will be displayed
            _orderNumber = string.Format("SPLR{0}", _num.Number.ToString().PadLeft(10, '0'));

            return _orderNumber;
        }

        public Order GetOrderById(int orderId)
        {
            return _db.Orders
                .Where(x => x.Id == orderId)
                .Include(x => x.OrderLines)
                .Include(x => x.Customer)
                .FirstOrDefault();
        }

        public Order GetOrderByOrderNumber(string orderNumber)
        {
            return _db.Orders
                .Where(x => x.OrderNumber == orderNumber)
                .Include(x => x.OrderLines)
                .Include(x => x.Customer)
                .FirstOrDefault();
        }

        public StaticPagedList<Order> GetOrderByCustomerId(int customerId, int page, int pageSize)
        {
            var products = _db.Orders
                   .Where(x => x.CustomerId == customerId)
                   .OrderBy(p => p.PercentageComplete)
                   .ThenByDescending(x => x.DateCompleted)
                   .Skip((page - 1) * pageSize)
                   .Take(pageSize);

            var count = _db.Orders.Where(x => x.CustomerId == customerId).Count();

            return new StaticPagedList<Order>(products, page, pageSize, count);
        }

        public StaticPagedList<Order> GetClosedOrderByCustomerId(int customerId, int page, int pageSize)
        {
            var products = _db.Orders
                   .Where(x => x.CustomerId == customerId &&
                   x.IsComplete == true)
                   .OrderBy(p => p.DateCompleted)
                   .Skip((page - 1) * pageSize)
                   .Take(pageSize);

            var count = _db.Orders.Where(x => x.CustomerId == customerId).Count();

            return new StaticPagedList<Order>(products, page, pageSize, count);
        }


        public List<Order> GetOrdersCompletedToday()
        {
            List<Order> _list = new List<Order>();

            var endDate = DateTime.Today.AddDays(1).AddTicks(-1);

            var query = (from s in _db.Orders
                         where (s.DateCompleted >= DateTime.Today && s.DateCompleted <= endDate) || s.IsInvoiced == false 
                         select s).ToList();

            query.ForEach(item =>
            {
                _list.Add(item);
            });

            return _list;
        }

        public Order GetOpenOrderById(int orderId)
        {
            return _db.Orders
                .Where(x => x.Id == orderId && x.IsComplete == false)
                .Include(x => x.OrderLines.Select(y => y.Product))
                .Include(x => x.Customer)
                .FirstOrDefault();
        }

        public List<Order> GetOpenOrders()
        {
            return _db.Orders
                .Where(x => x.IsComplete == false || x.PercentageComplete < 100)
                .Include(x => x.OrderLines)
                .Include(x => x.Customer)
                .ToList();
        }

        public List<Order> GetOrdersNotInvoiced(int? id)
        {
            if(id.HasValue)
            {
                return _db.Orders
                .Where(x => x.Id == id.Value && x.IsInvoiced == false && x.IsComplete == true)
                .Include(x => x.OrderLines)
                .Include(x => x.Customer)
                .ToList();
            }

            //Only get orders which has been proforma'd
            return _db.Orders
                .Where(x => x.IsInvoiced == false && x.IsComplete == true)
                .Include(x => x.OrderLines)
                .Include(x => x.Customer)
                .ToList();
        }

        public List<Order> GetOrdersInvoiced(int? id)
        {
            if (id.HasValue)
            {
                return _db.Orders
                .Where(x => x.Id == id.Value && x.IsInvoiced == true && x.IsComplete == true)
                .Include(x => x.OrderLines)
                .Include(x => x.Customer)
                .ToList();
            }

            //Only get orders which has been proforma'd
            return _db.Orders
                .Where(x => x.IsInvoiced == true && x.IsComplete == true)
                .Include(x => x.OrderLines)
                .Include(x => x.Customer)
                .ToList();
        }

        public List<Order> GetClosedOrders()
        {
            List<Order> _list = new List<Order>();

            //Open orders wont be 100% complete
            _list = _db.Orders.Where(x => x.IsComplete == true).Include(y => y.OrderLines).ToList();

            return _list;
        }

        public Dictionary<int, int> GetOrdersWithGroupedProducts(int customerId)
        {
            Dictionary<int, int> products = new Dictionary<int, int>();

            var query = from o in _db.Orders
                        join l in _db.OrderLines on o.Id equals l.OrderId
                        where o.CustomerId == customerId
                        group l by l.ProductId into g
                        select new { ProductId = g.Key, Count = g.Count() };

            products = query.AsEnumerable()
                          .Select(o => new
                          {
                              o.ProductId,
                              o.Count,
                          }).OrderByDescending(x => x.Count).ToDictionary(t => t.ProductId, t => t.Count);

            return products;
        }

        public List<Order> GetOrdersForViewByUserId(string userId)
        {
            List<Order> _orders = new List<Order>();

            var query = from o in _db.Orders
                        where o.UserId == userId
                        orderby o.PercentageComplete ascending
                        select new { o.Id, o.OrderNumber, o.PercentageComplete, o.DatePlaced };

            _orders = query.AsEnumerable()
                          .Select(o => new Order
                          {
                              Id = o.Id,
                              OrderNumber = o.OrderNumber,
                              PercentageComplete = o.PercentageComplete,
                              DatePlaced = o.DatePlaced
                          }).ToList();

            return _orders;
        }

        public List<Order> GetOrderViewForSupplier(int supplierId)
        {
            List<Order> _orders = new List<Order>();

            //var query = from o in _db.Orders
            //            where o.UserId == userId
            //            orderby o.PercentageComplete ascending
            //            select new { o.OrderId, o.OrderNumber, o.PercentageComplete, o.DateCreated };

            //_orders = query.AsEnumerable()
            //              .Select(o => new Order
            //              {
            //                  OrderId = o.OrderId,
            //                  OrderNumber = o.OrderNumber,
            //                  PercentageComplete = o.PercentageComplete,
            //                  DateCreated = o.DateCreated
            //              }).ToList();

            return _orders;
        }

        #region OrderLines
        public List<OrderLine> GetOrderLineBySupplier(int supplierId)
        {
            List<OrderLine> _list = new List<OrderLine>();

            _list = _db.OrderLines.Where(x => x.SupplierId == supplierId && x.IsCompleted == false).ToList();

            //As it is being loaded in the screen we need to set the flag BusyOrdering to true so that when we update, we only update the ones that was actually ordered
            foreach(var line in _list)
            {
                _db.Entry(line).State = EntityState.Modified;
                _db.SaveChanges();
            }

            return _list;
        }

        public OrderLine GetOrderLineById(int id)
        {
            return _db.OrderLines.Where(x => x.Id == id).Include(y => y.Order).FirstOrDefault();
        }
        public OrderLineEdited GetOrderLineEditedByOrderLineId(int OrderLineId)
        {
            return _db.OrderLinesEdited.Where(x => x.OrderLineId == OrderLineId).Include(y => y.Order).FirstOrDefault();
        }

        public List<OrderLine> GetOrderLinesByOrderId(int id)
        {
            return _db.OrderLines
                .Where(x => x.OrderId == id
                && x.IsReplaced == false)
                .Include(y => y.Order)
                .ToList();
        }

        public List<OrderLineEdited> GetOrderLinesEditedByOrderId(int id)
        {
            return _db.OrderLinesEdited
                .Where(x => x.OrderId == id
                && x.IsReplaced == false)
                .Include(y => y.Order)
                .ToList();
        }

        public void UpdateLinesToCantOrder(int productId)
        {
            //Get all the BusyOrdering lines with the productId 
            List<OrderLine> _lines = _db.OrderLines.Where(x => x.IsCompleted == false && x.ProductId == productId).ToList();

            //Line cant be ordered therefor it is also completed
            foreach (var line in _lines)
            {
                line.IsCompleted = true;

                _db.Entry(line).State = EntityState.Modified;
                _db.SaveChanges();
            }
        }        

        public void SaveLine(OrderLine model)
        {
            if (model.Id > 0)
            {
                _db.Entry(model).State = EntityState.Modified;
                _db.SaveChanges();
            }
            else
            {
                _db.OrderLines.Add(model);
                _db.SaveChanges();
            }
        }
        public void SaveLineEdited(OrderLineEdited model)
        {
            if (model.Id > 0)
            {
                _db.Entry(model).State = EntityState.Modified;
                _db.SaveChanges();
            }
            else
            {
                _db.OrderLinesEdited.Add(model);
                _db.SaveChanges();
            }
        }

        public void DeleteLine(OrderLine mode)
        {
            _db.OrderLines.Remove(mode);
            _db.SaveChanges();
        }
        public void DeleteLineEdited(OrderLineEdited mode)
        {
            _db.OrderLinesEdited.Remove(mode);
            _db.SaveChanges();
        }

        //Save Order lines
        public void AddOrderLine(OrderLine orderLine)
        {
            _db.OrderLines.Add(orderLine);
            _db.SaveChanges();
        }

        //Save Order lines edited
        public void AddOrderLineEdited(OrderLineEdited orderLineEdited)
        {
            _db.OrderLinesEdited.Add(orderLineEdited);
            _db.SaveChanges();
        }
        #endregion
    }
}