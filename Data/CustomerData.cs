﻿using Suppliro.Context;
using Suppliro.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Suppliro.Data
{
    public class CustomerData
    {
        private DBContext _db;

        public CustomerData()
        {
            _db = new DBContext();
        }

        public Customer GetCustomerByUserId(string userId)
        {
            return _db.Customers.Where(x => x.UserId == userId).FirstOrDefault();
        }

        public Customer GetCustomerById(int id)
        {
            return _db.Customers.Where(x => x.Id == id).FirstOrDefault();
        }

        public void Save(Customer customer)
        {
            if (customer.Id > 0)
            {
                _db.Entry(customer).State = EntityState.Modified;
                _db.SaveChanges();
            }
            else
            {
                _db.Customers.Add(customer);
                _db.SaveChanges();
            }
        }
    }
}