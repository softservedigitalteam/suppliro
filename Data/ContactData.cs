﻿using Suppliro.Context;
using Suppliro.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Suppliro.Data
{
    public class ContactData
    {
        private DBContext _db;

        public ContactData(DBContext context)
        {
            _db = context;
        }

        public void Add(Contact model)
        {
            _db.Contacts.Add(model);
            _db.SaveChanges();
        }
    }
}