﻿using Suppliro.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Suppliro.ViewModels
{
    public class UserOrderViewModel
    {
        public List<Order> Orders { get; set; }
    }
}