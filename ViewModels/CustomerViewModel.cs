﻿using Suppliro.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Suppliro.ViewModels
{
    public class CustomerViewModel
    {
        public Customer Customer { get; set; }

        public Supplier Supplier { get; set; }
    }
}