﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Suppliro.ViewModels
{
    public class SignUpViewModel
    {
        [Display(Name = "Business Name")]
        public string BusinessName { get; set; }

        [Display(Name = "Contact Person")]
        public string ContactPerson { get; set; }

        [Display(Name = "Contact Number")]
        public string Number { get; set; }

        [Display(Name = "Email")]
        public string Email { get; set; }
    }
}