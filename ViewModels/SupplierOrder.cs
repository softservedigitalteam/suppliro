﻿using Suppliro.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Suppliro.ViewModels
{
    public class SupplierOrder
    {
        public Supplier Supplier { get; set; }

        public List<Order> Orders { get; set; }
    }
}