﻿using Suppliro.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Suppliro.ViewModels
{
    public class CustomerOrderViewModel
    {
        public Customer Customer { get; set; }

        public CompanyInformation CompanyInformation { get; set; }

        public List<OrderLine> OrderLines { get; set; }
    }
}