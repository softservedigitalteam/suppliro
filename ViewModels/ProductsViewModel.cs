﻿using Suppliro.Entities;
using Suppliro.Search;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Suppliro.ViewModels
{
    public class ProductsViewModel
    {
        //This for searching
        public List<SearchModel> Products { get; set; }
    }
}