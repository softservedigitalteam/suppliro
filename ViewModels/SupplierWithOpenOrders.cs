﻿using Suppliro.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Suppliro.ViewModels
{
    public class SupplierWithOpenOrders
    {
        public List<Supplier> Suppliers { get; set; }
    }
}