﻿using Suppliro.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Suppliro.ViewModels
{
    public class SupplierOrderLines
    {
        public Supplier Supplier { get; set; }

        public List<OrderLine> OrderLines { get; set; }
    }
}