﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Suppliro.Startup))]
namespace Suppliro
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
