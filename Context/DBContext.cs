﻿using Microsoft.AspNet.Identity.EntityFramework;
using Suppliro.Entities;
using Suppliro.Migrations;
using Suppliro.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace Suppliro.Context
{
    public class DBContext : IdentityDbContext<Models.ApplicationUser>
    {
        public DBContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public virtual DbSet<Cart> Carts { get; set; }
        public virtual DbSet<Category> Categories { get; set; }
        public virtual DbSet<CompanyInformation> CompanyInformations { get; set; }
        public virtual DbSet<Customer> Customers { get; set; }
        public virtual DbSet<Contact> Contacts { get; set; }
        public virtual DbSet<Employee> Employees { get; set; }
        public virtual DbSet<Invoice> Invoices { get; set; }
        public virtual DbSet<InvoiceLine> InvoiceLines { get; set; }
        public virtual DbSet<InvoiceNumber> InvoiceNumbers { get; set; }
        public virtual DbSet<Order> Orders { get; set; }
        public virtual DbSet<OrderLine> OrderLines { get; set; }
        public virtual DbSet<OrderLineEdited> OrderLinesEdited { get; set; }
        public virtual DbSet<OrderNumber> OrderNumbers { get; set; }
        public virtual DbSet<Product> Products { get; set; }
        public virtual DbSet<ProductHistory> ProductHistories { get; set; }
        public virtual DbSet<SignUp> SignUps { get; set; }
        public virtual DbSet<StockSheet> StockSheets { get; set; }
        public virtual DbSet<Supplier> Suppliers { get; set; }
        public virtual DbSet<SupplierOrder> SupplierOrders { get; set; }
        public virtual DbSet<SupplierOrderLine> SupplierOrderLines { get; set; }
        public virtual DbSet<SupplierOrderLineEdited> SupplierOrderLinesEdited { get; set; }
        public virtual DbSet<SupplierOrderNumber> SupplierOrderNumbers { get; set; }
        public virtual DbSet<PickUpForm> PickUpForms { get; set; }
        public virtual DbSet<PickUpFormLine> PickUpFormLines { get; set; }
        public virtual DbSet<UploadedProduct> UploadedProducts { get; set; }
        public virtual DbSet<UoM> UoMs { get; set; }
        public virtual DbSet<UoMMapper> UoMMappers { get; set; }
        
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
        }

        public static DBContext Create()
        {
            return new DBContext();
        }
    }
}