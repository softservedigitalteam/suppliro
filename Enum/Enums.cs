﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Suppliro.Enum
{
    public class Enums
    {
        public enum Businesstype
        {
            Restaurant = 1,
            Catering = 2
        }

        public enum EmailType
        {
            SupplierOrderEmail,
            ProFormaInvoice
        }

        public enum EmailSection
        {
            smtpAdmin
        }
    }
}