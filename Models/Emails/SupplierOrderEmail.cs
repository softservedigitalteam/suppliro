﻿using Postal;
using Suppliro.Areas.Admin.Models;
using Suppliro.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Suppliro.Models.Emails
{
    public class SupplierOrderEmail : Email
    {
        public string ToAddresses { get; set; }

        public string FromAddress { get; set; }

        public string Subject { get; set; }

        public Supplier Supplier { get; set; }

        public List<SupplierEmailOrderLine> EmailLines { get; set; }
    }
}