﻿
using Newtonsoft.Json;
using System;
using System.Web.Script.Serialization;

public static class JsonExtensions
{
    public static string ToJson(this Object obj)
    {
        return new JavaScriptSerializer().Serialize(obj);
    }
}