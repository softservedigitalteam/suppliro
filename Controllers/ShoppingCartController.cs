﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Suppliro.Context;
using Suppliro.Data;
using Suppliro.Entities;
using Suppliro.Manager;
using Suppliro.Models;
using Suppliro.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Suppliro.Controllers
{
    [Authorize]
    public class ShoppingCartController : Controller
    {
        // GET: ShoppingCart
        public ActionResult Index()
        {
            var cart = ShoppingCart.GetCart(this.HttpContext);

            // Set up our ViewModel
            var viewModel = new ShoppingCartViewModel
            {
                CartItems = cart.GetCartItems(),
                CartTotal = cart.GetTotal()
            };
            // Return the view
            return View(viewModel);
        }

        // GET: /Store/AddToCart/5
        public ActionResult AddToCart(int id, int count)
        {
            ProductData _productData = new ProductData();
            // Retrieve the product from the database
            var addedProduct = _productData.GetProductById(id);

            // Add it to the shopping cart
            var cart = ShoppingCart.GetCart(this.HttpContext);

            cart.AddToCart(addedProduct, count);

            // Go back to the main store page for more shopping
            return RedirectToAction("CartSummary");
        }
        //
        // AJAX: /ShoppingCart/RemoveFromCart/5
        [HttpPost]
        public ActionResult RemoveFromCart(int id)
        {
            CartData _cartData = new CartData();
            // Remove the item from the cart
            var cart = ShoppingCart.GetCart(this.HttpContext);

            // Get the name of the album to display confirmation
            Cart cartItem = _cartData.GetCartItemByRecordId(id);

            string productCode = cartItem.Product.Code;

            // Remove from cart
            _cartData.RemoveCart(cartItem);

            // Display the confirmation message
            var results = new ShoppingCartRemoveViewModel
            {
                Message = Server.HtmlEncode(productCode) +
                    " has been removed from your shopping cart.",
                CartTotal = cart.GetTotal(),
                CartCount = cart.GetCount(),
                DeleteId = id
            };
            return Json(results);
        }
        //
        // GET: /ShoppingCart/CartSummary
        public ActionResult CartSummary()
        {
            var cart = ShoppingCart.GetCart(this.HttpContext);

            ViewData["CartCount"] = cart.GetCount();
            ViewData["CartAmount"] = cart.GetTotal();
            return PartialView("CartSummary");
        }

        public ActionResult CreateOrder()
        {
            var _db = new DBContext();
            var store = new UserStore<ApplicationUser>(_db);
            var userManager = new UserManager<ApplicationUser>(store);
            ApplicationUser user = userManager.FindByName(User.Identity.Name);

            OrderManager ordMgr = new OrderManager(_db);
            ordMgr.BuildOrder(user, ShoppingCart.GetCart(this.HttpContext));

            return RedirectToAction("Index", "Home");
        }
    }
}