﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Suppliro.Areas.Admin.Manager;
using Suppliro.Context;
using Suppliro.Data;
using Suppliro.Entities;
using Suppliro.Manager;
using Suppliro.Models;
using Suppliro.Search;
using Suppliro.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Suppliro.Controllers
{
    [Authorize]
    public class ProductController : Controller
    {
        // GET: Product
        public ActionResult Index()
        {
            return View();
        }

        //[HttpPost]
        public ActionResult Products(string query)
        {
            if(string.IsNullOrWhiteSpace(query))
            {
                return RedirectToAction("Index", "Home");
            }

            StringBuilder newQuery = new StringBuilder();

            string[] words = query.Split(' ');
            foreach (string word in words)
            {
                newQuery.Append(string.Format("+{0} ", word));
            }

            // create default Lucene search index directory
            if (!Directory.Exists(GoLucene._luceneDir)) Directory.CreateDirectory(GoLucene._luceneDir);

            // perform Lucene search
            List<SearchModel> _searchResults;
                _searchResults = (GoLucene.Search(newQuery.ToString().TrimEnd())).ToList();
            if (string.IsNullOrEmpty(query) && !_searchResults.Any())
                _searchResults = GoLucene.GetAllIndexRecords().ToList();
            
            ProductsViewModel _productVM = new ProductsViewModel();
            _productVM.Products = _searchResults;

            //Add the search query so it can be reused on the product results page
            ViewBag.Query = query;

            return View(_productVM);
        }

        public ActionResult FBP()
        {
            ProductManager prodMgr = new ProductManager(new DBContext());
            var context = new DBContext();
            CustomerManager mgr = new CustomerManager(context);
            var store = new UserStore<ApplicationUser>(context);
            var userManager = new UserManager<ApplicationUser>(store);
            ApplicationUser user = userManager.FindById(User.Identity.GetUserId());

            var customer = mgr.GetCustomerByUserId(user.Id);

            return View(prodMgr.GetCustomerFrequentBoughtProduct(customer.Id));
        }

        public ActionResult StockSheet()
        {
            ProductManager prodMgr = new ProductManager(new DBContext());
            var context = new DBContext();
            CustomerManager mgr = new CustomerManager(context);
            var store = new UserStore<ApplicationUser>(context);
            var userManager = new UserManager<ApplicationUser>(store);
            ApplicationUser user = userManager.FindById(User.Identity.GetUserId());
            var customer = mgr.GetCustomerByUserId(user.Id);

            return View(prodMgr.GetStockSheet(customer.Id));
        }

        public ActionResult AddToStockSheet(int id)
        {
            ProductManager prodMgr = new ProductManager(new DBContext());
            var context = new DBContext();
            CustomerManager mgr = new CustomerManager(context);
            var store = new UserStore<ApplicationUser>(context);
            var userManager = new UserManager<ApplicationUser>(store);
            ApplicationUser user = userManager.FindById(User.Identity.GetUserId());
            var customer = mgr.GetCustomerByUserId(user.Id);

            return Json(prodMgr.AddProductToStockSheet(id, customer.Id, user.Id));
        }

        public ActionResult RemoveFromStockSheet(int id)
        {
            ProductManager prodMgr = new ProductManager(new DBContext());
            var context = new DBContext();
            CustomerManager mgr = new CustomerManager(context);
            var store = new UserStore<ApplicationUser>(context);
            var userManager = new UserManager<ApplicationUser>(store);
            ApplicationUser user = userManager.FindById(User.Identity.GetUserId());
            var customer = mgr.GetCustomerByUserId(user.Id);

            return Json(prodMgr.RemoveFromStockSheet(id, customer.Id, user.Id));
        }
    }
}