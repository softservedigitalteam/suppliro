﻿using OfficeOpenXml;
using Suppliro.Data;
using Suppliro.Entities;
using Suppliro.Search;
using Suppliro.ViewModels;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;

namespace Suppliro.Controllers
{
    [Authorize]
    public class AdminController : Controller
    {
        // Open Orders
        public ActionResult Index()
        {
            //SupplierData _supplierData = new SupplierData();

            //SupplierWithOpenOrders _swoo = new SupplierWithOpenOrders();  
            //_swoo.Suppliers = _supplierData.GetSuppliersWithOpenOrders();

            return View();
        }

        //GET: uses supplier id
        public ActionResult Order(int? id)
        {
            //SupplierData _supplierData = new SupplierData();
            //SupplierOrderLines _sol = new SupplierOrderLines();

            //if (id.HasValue && id.Value > 0)
            //{
            //    _sol.Supplier = _supplierData.GetSupplierById(id.Value);
            //    _sol.OrderLines = _supplierData.GetOrderLineBySupplier(id.Value);
            //}

            return View();
        }

        //Closed orders
        public ActionResult Closed()
        {
            return View();
        }

        //Technical Settings
        public ActionResult Settings()
        {
            return View();
        }

        public ActionResult CreateIndex()
        {
            ProductData _prdData = new ProductData();

            
            return RedirectToAction("Settings");
        }

        public ActionResult UploadPrices(FormCollection formCollection)
        {
            if (Request != null)
            {
                HttpPostedFileBase file = Request.Files["UploadedFile"];
                if ((file != null) && (file.ContentLength > 0) && !string.IsNullOrEmpty(file.FileName))
                {
                    string fileName = file.FileName;
                    string fileContentType = file.ContentType;
                    byte[] fileBytes = new byte[file.ContentLength];
                    var data = file.InputStream.Read(fileBytes, 0, Convert.ToInt32(file.ContentLength));
                    var productList = new List<UploadedProduct>();
                    using (var package = new ExcelPackage(file.InputStream))
                    {
                        var currentSheet = package.Workbook.Worksheets[0];
                        var noOfCol = currentSheet.Dimension.End.Column;
                        var noOfRow = currentSheet.Dimension.End.Row;
                        for (int rowIterator = 2; rowIterator >= noOfRow; rowIterator++)
                        {
                            //var user = new Users();
                            //user.FirstName = workSheet.Cells[rowIterator, 1].Value.ToString();
                            //user.LastName = workSheet.Cells[rowIterator, 2].Value.ToString();
                            //usersList.Add(user);
                        }
                    }
                }
            }

            return View("Index");
        }
    }
}