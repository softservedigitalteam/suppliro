﻿using Suppliro.Areas.Admin.Manager;
using Suppliro.Context;
using Suppliro.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Suppliro.Controllers
{
    public class CategoryController : Controller
    {
        // GET: Category
        public ActionResult Index(int? id)
        {
            ProductManager mgr = new ProductManager(new DBContext());
            List<Product> products = new List<Product>();

            if(id.HasValue)
            {
                products = mgr.GetAllProductsByCategoryId(id.Value);
            }
            else
            {
                products = null;
            }

            return View(products);
        }
    }
}