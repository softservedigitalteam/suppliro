﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Suppliro.Areas.Admin.Manager;
using Suppliro.Context;
using Suppliro.Data;
using Suppliro.Entities;
using Suppliro.Manager;
using Suppliro.Models;
using Suppliro.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Suppliro.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        [AllowAnonymous]
        public ActionResult Index()
        {
            if (!User.Identity.IsAuthenticated)
                return RedirectToAction("SignUp");

            CategoryManager mgr = new CategoryManager(new DBContext());
            return View(mgr.GetCategoryListViewModel());
        }

        [AllowAnonymous]
        public ActionResult SignUp()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult SignUp(SignUpViewModel model)
        {
            SignUpData _data = new SignUpData();
            _data.SignUp(model);
            ViewBag.Success = true;            
            
            return View(new SignUpViewModel());
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            return View();
        }

        public ActionResult SendMessage(Contact model)
        {
            var context = new DBContext();
            CustomerManager mgr = new CustomerManager(context);
            var store = new UserStore<ApplicationUser>(context);
            var userManager = new UserManager<ApplicationUser>(store);
            ApplicationUser user = userManager.FindByName(User.Identity.Name);
            var customer = mgr.GetCustomerByUserId(user.Id);

            model.CustomerId = customer.Id;

            ContactManager contactMgr = new ContactManager(context);
            contactMgr.AddMessage(model);

            return RedirectToAction("Index");
        }
    }
}