﻿using Suppliro.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using Suppliro.ViewModels;
using Suppliro.Manager;
using Microsoft.AspNet.Identity.EntityFramework;
using Suppliro.Models;
using Suppliro.Context;
using Microsoft.AspNet.Identity;
using Suppliro.Areas.Admin.ViewModels;
using Suppliro.Areas.Admin.Data;

namespace Suppliro.Controllers
{
    [Authorize]
    public class CustomerController : Controller
    {
        // GET: Customer
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Information()
        {
            var context = new DBContext();
            CustomerManager mgr = new CustomerManager(context);
            var store = new UserStore<ApplicationUser>(context);
            var userManager = new UserManager<ApplicationUser>(store);
            ApplicationUser user = userManager.FindById(User.Identity.GetUserId());

            var customer = mgr.GetCustomerByUserId(user.Id);

            return PartialView(customer);
        }

        [HttpPost]
        public ActionResult SaveInformation(Customer model)
        {
            var context = new DBContext();
            CustomerManager mgr = new CustomerManager(context);
            var store = new UserStore<ApplicationUser>(context);
            var userManager = new UserManager<ApplicationUser>(store);
            ApplicationUser user = userManager.FindById(User.Identity.GetUserId());
            mgr.SaveInformation(model, user.Id);
            return PartialView("Information");
        }

        public ActionResult Address()
        {
            var context = new DBContext();
            CustomerManager mgr = new CustomerManager(context);
            var store = new UserStore<ApplicationUser>(context);
            var userManager = new UserManager<ApplicationUser>(store);
            ApplicationUser user = userManager.FindById(User.Identity.GetUserId());

            return PartialView(mgr.GetCustomerByUserId(user.Id));
        }

        [HttpPost]
        public ActionResult SaveAddress(Customer model)
        {
            var context = new DBContext();
            CustomerManager mgr = new CustomerManager(context);
            var store = new UserStore<ApplicationUser>(context);
            var userManager = new UserManager<ApplicationUser>(store);
            ApplicationUser user = userManager.FindById(User.Identity.GetUserId());
            mgr.SaveAddress(model, user.Id);
            return PartialView("Address");
        }

        public ActionResult Invoice()
        {
            var context = new DBContext();
            CustomerManager mgr = new CustomerManager(context);
            InvoiceManager invMgr = new InvoiceManager(context);
            var store = new UserStore<ApplicationUser>(context);
            var userManager = new UserManager<ApplicationUser>(store);
            ApplicationUser user = userManager.FindById(User.Identity.GetUserId());
            var customer = mgr.GetCustomerByUserId(user.Id);

            var invoice = invMgr.GetInvoice(customer.Id) ?? new Invoice();

            return PartialView(invoice);
        }

        public ActionResult InvoiceHistory(string currentFilter, string searchString, int? page)
        {
            var context = new DBContext();
            CustomerManager mgr = new CustomerManager(context);
            InvoiceManager invMgr = new InvoiceManager(context);
            var store = new UserStore<ApplicationUser>(context);
            var userManager = new UserManager<ApplicationUser>(store);
            ApplicationUser user = userManager.FindById(User.Identity.GetUserId());
            var customer = mgr.GetCustomerByUserId(user.Id);

            if (searchString != null)
                page = 1;
            else
                searchString = currentFilter;

            ViewBag.CurrentFilter = searchString;

            int pageSize = 10;
            int pageNumber = (page ?? 1);
            return PartialView(invMgr.GetInvoicesByCustomerIdPaged(customer.Id, pageNumber, pageSize));
        }

        public ActionResult Invoices(string currentFilter, string searchString, int? page)
        {
            var context = new DBContext();
            CustomerManager mgr = new CustomerManager(context);
            OrderManager ordMgr = new OrderManager(context);
            var store = new UserStore<ApplicationUser>(context);
            var userManager = new UserManager<ApplicationUser>(store);
            ApplicationUser user = userManager.FindById(User.Identity.GetUserId());
            var customer = mgr.GetCustomerByUserId(user.Id);

            if (searchString != null)
                page = 1;
            else
                searchString = currentFilter;

            ViewBag.CurrentFilter = searchString;

            int pageSize = 10;
            int pageNumber = (page ?? 1);
            return PartialView(ordMgr.GetOrdersByCustomerId(customer.Id, pageNumber, pageSize));
        }

        [HttpGet]
        public ActionResult UpdatePassword()
        {
            return PartialView();
        }

        [HttpPost]
        public ActionResult UpdatePassword(PasswordModel model)
        {
            if (model != null && !string.IsNullOrWhiteSpace(model.Password))
            {
                var context = new DBContext();
                var store = new UserStore<ApplicationUser>(context);
                var userManager = new UserManager<ApplicationUser>(store);
                ApplicationUser user = userManager.FindById(User.Identity.GetUserId());

                userManager.RemovePassword(user.Id);
                userManager.AddPassword(user.Id, model.Password);

                context.SaveChanges();
            }

            return PartialView("UpdatePassword");
        }

        public ActionResult OrderLines(int id)
        {
            var context = new DBContext();
            CustomerManager mgr = new CustomerManager(context);
            OrderManager ordMgr = new OrderManager(context);
            var store = new UserStore<ApplicationUser>(context);
            var userManager = new UserManager<ApplicationUser>(store);
            ApplicationUser user = userManager.FindById(User.Identity.GetUserId());
            var customer = mgr.GetCustomerByUserId(user.Id);

            return PartialView(ordMgr.GetCustomerOrderViewModel(id, customer.Id));
        }

        public ActionResult CustomerInvoice(int id)
        {
            var context = new DBContext();
            CustomerManager mgr = new CustomerManager(context);
            InvoiceManager invMgr = new InvoiceManager(context);

            return PartialView("InvoicePartial", invMgr.GetInvoiceById(id));
        }

        public ActionResult DownloadInvoiceAsPDF(int id)
        {
            InvoiceManager mgr = new InvoiceManager(new DBContext());
            var model = mgr.GetInvoiceById(id);

            //Code to get content
            return new Rotativa.ActionAsPdf("InvoicePDF", new { id = id }) { FileName = model.InvoiceNumber + ".pdf" };
        }

        [AllowAnonymous]
        public ActionResult InvoicePDF(int id)
        {
            InvoiceManager mgr = new InvoiceManager(new DBContext());
            return View("~/Areas/Admin/Views/Invoice/ProFormaPDF.cshtml", mgr.GetProFormaViewModel(id));
        }

        public ActionResult DownloadOrderAsPDF(int id)
        {
            OrderManager mgr = new OrderManager(new DBContext());
            var model = mgr.GetOrderById(id);

            //Code to get content
            return new Rotativa.ActionAsPdf("OrderPDF", new { id = id }) { FileName = model.OrderNumber + ".pdf" };
        }

        [AllowAnonymous]
        public ActionResult OrderPDF(int id)
        {
            var context = new DBContext();

            OrderManager mgr = new OrderManager(context);
            CompanyInformationData ciData = new CompanyInformationData(context);

            var invoiceList = new List<Order>();
            invoiceList.Add(mgr.GetOrderById(id));

            CustomerInvoicesViewModel vm = new CustomerInvoicesViewModel
            {
                CompanyInfo = ciData.GetInformation(),
                Invoices = invoiceList
            };

            return View("~/Areas/Admin/Views/Invoice/InvoicePDF.cshtml", vm);
        }
    }
}