﻿using PagedList;
using Suppliro.Areas.Admin.Data;
using Suppliro.Areas.Admin.Manager;
using Suppliro.Areas.Admin.ViewModels;
using Suppliro.Context;
using Suppliro.Data;
using Suppliro.Entities;
using Suppliro.Models;
using Suppliro.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Suppliro.Manager
{
    public class OrderManager
    {
        DBContext _db;

        public OrderManager(DBContext context)
        {
            _db = context;
        }

        public StaticPagedList<Order> GetOrdersByCustomerId(int customerId, int pageNumber, int pageSize)
        {
            OrderData orderData = new OrderData(_db);
            return orderData.GetOrderByCustomerId(customerId, pageNumber, pageSize);
        }

        public StaticPagedList<Order> GetClosedOrdersByCustomerId(int customerId, int pageNumber, int pageSize)
        {
            OrderData orderData = new OrderData(_db);
            return orderData.GetClosedOrderByCustomerId(customerId, pageNumber, pageSize);
        }

        public CustomerOrderViewModel GetCustomerOrderViewModel(int orderId, int customerId)
        {
            OrderData orderData = new OrderData(_db);
            CustomerData custData = new CustomerData();
            CompanyInformationData compData = new CompanyInformationData(_db);

            CustomerOrderViewModel vm = new CustomerOrderViewModel
            {
                Customer = custData.GetCustomerById(customerId),
                CompanyInformation = compData.GetInformation(),
                OrderLines = orderData.GetOrderLinesByOrderId(orderId)
            };

            return vm;
        }

        public List<Order> GetOrdersCompletedToday()
        {
            OrderData data = new OrderData(_db);
            return data.GetOrdersCompletedToday().OrderByDescending(x => x.DatePlaced).ToList();
        }

        public Order GetOrderById(int id)
        {
            OrderData data = new OrderData(_db);
            return data.GetOrderById(id);
        }

        public Dictionary<int, int> GetOrdersWithGroupedProducts(int customerId)
        {
            OrderData data = new OrderData(_db);
            return data.GetOrdersWithGroupedProducts(customerId);
        }

        public CustomerInvoicesViewModel GetCustomerInvoicesViewModel(int? id)
        {
            OrderData orderData = new OrderData(_db);
            CompanyInformationData compData = new CompanyInformationData(_db);

            CustomerInvoicesViewModel vm = new CustomerInvoicesViewModel
            {
                CompanyInfo = compData.GetInformation(),
                Invoices = orderData.GetOrdersInvoiced(id)
            };

            return vm;
        }

        public CustomerInvoicesViewModelEdited GetCustomerInvoicesViewModelEdited(int? id)
        {
            OrderData orderData = new OrderData(_db);
            CompanyInformationData compData = new CompanyInformationData(_db);

            CustomerInvoicesViewModelEdited vm = new CustomerInvoicesViewModelEdited
            {
                CompanyInfo = compData.GetInformation(),
                Invoices = orderData.GetOrdersInvoiced(id)
            };

            return vm;
        }

        public CustomerInvoicesViewModel GetnvoicedViewModel(int? id)
        {
            OrderData orderData = new OrderData(_db);
            CompanyInformationData compData = new CompanyInformationData(_db);

            CustomerInvoicesViewModel vm = new CustomerInvoicesViewModel
            {
                CompanyInfo = compData.GetInformation(),
                Invoices = orderData.GetOrdersInvoiced(id)
            };

            return vm;
        }

        public List<Order> GetOrdersNotInvoiced(int? id)
        {
            OrderData orderData = new OrderData(_db);
            return orderData.GetOrdersNotInvoiced(id).OrderByDescending(x => x.DatePlaced).ToList();
        }

        public List<Order> GetOrdersInvoiced(int? id)
        {
            OrderData orderData = new OrderData(_db);
            return orderData.GetOrdersInvoiced(id).OrderByDescending(x => x.DatePlaced).ToList();
        }

        #region Build Order
        public void BuildOrder(ApplicationUser user, ShoppingCart cart)
        {
            //Data classes
            CustomerData _custData = new CustomerData();
            OrderData _orderData = new OrderData(_db);
            CompanyInformationData compData = new CompanyInformationData(_db);

            //Managers
            SupplierOrderManager splrOrderMgr = new SupplierOrderManager(_db);

            //Models
            Order _order = new Order();
            Customer _customer = _custData.GetCustomerByUserId(user.Id);
            CompanyInformation compInfo = compData.GetInformation();

            var cartItems = cart.GetCartItems();

            if (cartItems != null && cartItems.Count > 0 &&_customer != null )
            {
                //pre populate order with delivery details
                _order.TradingName = _customer.TradingName;
                _order.RegisteredName = _customer.RegisteredName;
                _order.VATNumber = _customer.VATNumber;
                _order.TelNumber = _customer.TelNumber;
                _order.BuyersName = _customer.BuyersName;
                _order.BuyersIDNumber = _customer.BuyersIDNumber;
                _order.EmailAddress = _customer.EmailAddress;

                _order.PhysicalAddress1 = _customer.PhysicalAddress1;
                _order.PhysicalAddress2 = _customer.PhysicalAddress2;
                _order.PhysicalPostalCode = _customer.PhysicalPostalCode;
                _order.PhysicalRegion = _customer.PhysicalRegion;
                _order.PhysicalTown = _customer.PhysicalTown;

                //Set order number
                _order.OrderNumber = _orderData.GenerateOrderNumber();

                //Set order information
                //_order.DatePlaced = Convert.ToDateTime(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                _order.DatePlaced = DateTime.Now;
                _order.CustomerId = _customer.Id;
                _order.CustomerName = _customer.TradingName;
                _order.UserId = user.Id;
                //Always preset to zero as nothing has been ordered yet
                _order.PercentageComplete = 0;

                //Save newly created order and then add lines
                _orderData.SaveOrder(_order);
                
                // Iterate over the items in the cart, 
                // adding the order details for each
                foreach (var item in cartItems)
                {
                    var uom = "";

                    if(item.Product.UoMValue > 0)
                    {
                        uom = string.Format("{0} x {1} ({2})", item.Product.MinOrder, item.Product.UoMValue, item.Product.Unit);
                    }
                    else
                    {
                        uom = string.Format("{0} ({1})", item.Product.MinOrder, item.Product.Unit);
                    }

                    var orderLine = new OrderLine
                    {
                        ProductId = item.ProductId,
                        OrderId = _order.Id,
                        ProductCode = string.IsNullOrWhiteSpace(item.Product.DisplayName) ? item.Product.Code : item.Product.DisplayName,
                        SupplierId = item.Product.Supplier.Id,
                        SupplierTradingName = item.Product.Supplier.TradingName,
                        UnitPrice = item.Product.MarkedUpPrice,
                        Unit = uom,
                        Quantity = item.Count,
                        Amount = Math.Round(item.Product.MarkedUpPrice * item.Count, 2),
                        VATAmount = Math.Round((item.Product.MarkedUpPrice * item.Count) * 0.14, 2)
                    };

                    _orderData.AddOrderLine(orderLine);
                }

                // Save the order
                _orderData.SaveOrder(_order);
                // Empty the shopping cart
                cart.EmptyCart();
                //Update Totals
                this.UpdateOrderTotals(_order.Id);
                //Build/Append to supplier order
                splrOrderMgr.BuildSupplierOrder(_order.Id);
            }
        }
        #endregion

        #region Update Order Totals
        public void UpdateOrderTotals(int orderId)
        {
            //Get order
            OrderData _orderData = new OrderData(_db);
            var order = _orderData.GetOrderById(orderId);

            double subTotal = 0;
            double VATAmount = 0;
            double Total = 0;

            order.OrderLines.ForEach(line =>
            {
                if (!line.IsReplaced)
                {
                    subTotal += Math.Round(line.Amount, 2);
                    VATAmount += Math.Round(line.VATAmount, 2);
                    Total += Math.Round(line.Amount + line.VATAmount, 2);
                }
            });

            order.SubTotal = subTotal;
            order.VATAmount = VATAmount;
            order.Total = Total;

            _orderData.SaveOrder(order);
        }
        #endregion

        #region Update order lines to ordered
        public void UpdateOrderLines(int customerId, List<SupplierOrderLine> supplierOrderLines)
        {
            OrderData orderData = new OrderData(_db);

            //Update order lines
            supplierOrderLines.ForEach(line =>
            {
                if(line.CustomerId == customerId)
                {
                    //find line by id
                    OrderLine orderLine = orderData.GetOrderLineById(line.OrderLineId);
                    orderLine.IsOrdered = true;
                    orderLine.IsCompleted = true;
                    orderData.SaveLine(orderLine);
                }
            });
        }
        #endregion

        #region UpdatePercentage
        public void UpdateOrderPercentages(int? id)
        {
            OrderData orderData = new OrderData(_db);

            List<Order> _orders = new List<Order>();

            if (id.HasValue)
                _orders.Add(orderData.GetOrderById(id.Value));
            else
                //Get all open orders
                _orders = orderData.GetOpenOrders();

            //loop through orders
            foreach (var order in _orders)
            {
                List<OrderLine> _lines = orderData.GetOrderLinesByOrderId(order.Id);
                //Get the total number of lines
                int lineCount = _lines.Count;

                //Get total number of completed lines
                int completedCount = _lines.Where(x => x.IsCompleted == true).Count();

                //if line count == number of completed lines then we update the order as completed and set percentage to 100
                if (lineCount == completedCount)
                {
                    order.DateCompleted = DateTime.Now;
                    order.IsComplete = true;
                    order.PercentageComplete = 100;
                }
                else
                {
                    decimal _percentageComplete = (((decimal)completedCount / (decimal)lineCount) * 100);
                    order.PercentageComplete = Math.Round((double)(_percentageComplete), 2);
                }

                if(order.PercentageComplete == 100)
                {
                    order.IsComplete = true;
                    order.DateCompleted = DateTime.Now;
                }

                orderData.SaveOrder(order);
            }
        }
        #endregion

        public void MarkOrderAsInvoiced(int id)
        {
            OrderData orderData = new OrderData(_db);

            var order = orderData.GetOrderById(id); 
            order.IsInvoiced = true;
            order.DateCompleted = DateTime.Now;

            orderData.SaveOrder(order);
        }

        public OrderLine GetOrderLineById(int id)
        {
            OrderData orderData = new OrderData(_db);
            return orderData.GetOrderLineById(id);
        }
        public OrderLineEdited GetOrderLineEditedByOrderLineId(int OrderLineId)
        {
            OrderData orderData = new OrderData(_db);
            return orderData.GetOrderLineEditedByOrderLineId(OrderLineId);
        }

        public void SaveOrderLine(OrderLine model)
        {
            OrderData orderData = new OrderData(_db);
            orderData.SaveLine(model);
        }

        public void DeleteOrderLine(OrderLine model)
        {
            OrderData orderData = new OrderData(_db);
            orderData.DeleteLine(model);
        }
        public void DeleteOrderLineEdited(OrderLineEdited model)
        {
            OrderData orderData = new OrderData(_db);
            orderData.DeleteLineEdited(model);
        }
    }
}