﻿using Suppliro.Context;
using Suppliro.Data;
using Suppliro.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Suppliro.Manager
{
    public class ContactManager
    {
        DBContext _db;

        public ContactManager(DBContext context)
        {
            _db = context;
        }

        public void AddMessage(Contact model)
        {
            ContactData data = new ContactData(_db);
            data.Add(model);
        }
    }
}