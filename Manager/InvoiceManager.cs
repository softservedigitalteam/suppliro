﻿using PagedList;
using Suppliro.Areas.Admin.Data;
using Suppliro.Areas.Admin.Manager;
using Suppliro.Areas.Admin.ViewModels;
using Suppliro.Context;
using Suppliro.Data;
using Suppliro.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Suppliro.Manager
{
    public class InvoiceManager
    {
        private DBContext _db;

        public InvoiceManager(DBContext context)
        {
            _db = context;
        }

        public StaticPagedList<Invoice> GetInvoicesByCustomerIdPaged(int customerId, int page, int pageSize)
        {
            InvoiceData _data = new InvoiceData(_db);
            return _data.GetInvoicesByCustomerIdPaged(customerId, page, pageSize);
        }

        public void BuildInvoice(int customerId, int supplierId, List<SupplierOrderLine> supplierOrderlines)
        {
            //Data
            InvoiceData invoiceData = new InvoiceData(_db);

            CompanyInformationData companyInfoData = new CompanyInformationData(_db);
            CompanyInformation companyInfo = companyInfoData.GetInformation();

            ProductManager prodMgr = new ProductManager(_db);

            //Find open proforma or create new one
            Invoice invoice = invoiceData.GetOpenInvoice(customerId);

            if (invoice == null || invoice.Id <= 0)
            {
                CustomerData customerData = new CustomerData();
                Customer customer = customerData.GetCustomerById(customerId);

                SupplierData supplierData = new SupplierData(_db);
                Supplier supplier = supplierData.GetSupplierById(supplierId);

                invoice = new Invoice();
                invoice.InvoiceNumber = invoiceData.GenerateInvoiceNumber(customer.CustomerCode);
                invoice.VATAmount = 0;
                invoice.SubTotalAmount = 0;
                invoice.TotalAmount = 0;
                invoice.DateCreated = DateTime.Now;

                //Customer
                invoice.CustomerId = customerId;
                invoice.CustomerCode = customer.CustomerCode;
                invoice.CustomerRegisteredName = customer.RegisteredName;
                invoice.CustomerTradingName = customer.TradingName;
                invoice.CustomerPhysicalAddress1 = customer.PhysicalAddress1;
                invoice.CustomerPhysicalAddress2 = customer.PhysicalAddress2;
                invoice.CustomerPhysicalPostalCode = customer.PhysicalPostalCode;
                invoice.CustomerPhysicalRegion = customer.PhysicalRegion;
                invoice.CustomerPhysicalTown = customer.PhysicalTown;

                //Company Information
                invoice.CompanyId = companyInfo.Id;
                invoice.CompanyName = companyInfo.TradingName;
                invoice.CompanyVATNumber = companyInfo.VATNumber;
                invoice.CompanyAddress1 = companyInfo.Address1;
                invoice.CompanyAddress2 = companyInfo.Address2;
                invoice.CompanyCity = companyInfo.City;
                invoice.CompanyPostalCode = companyInfo.PostalCode;
                invoice.CompanyRegion = companyInfo.Region;

                //Save 
                invoiceData.SaveInvoice(invoice);
            }

            supplierOrderlines.ForEach(line =>
            {
                if (line.CustomerId == customerId)
                {
                    if (invoice.InvoiceLines != null && invoice.InvoiceLines.Count > 0)
                    {
                        //Line exists so dont add it again
                        var existingLine = invoiceData.CheckExistingLine(line.OrderId, line.OrderLineId);

                        if (existingLine != null)
                            return;
                    }

                    var product = prodMgr.GetProductById(line.ProductId);

                    var subTotal = Math.Round(line.Quantity * product.MarkedUpPrice, 2);
                    var vatAmount = Math.Round((subTotal / 100) * companyInfo.VATPercentage, 2);
                    var total = Math.Round(subTotal + vatAmount, 2);

                    //Create new line
                    InvoiceLine newLine = new InvoiceLine
                    {
                        DateAdded = DateTime.Now,
                        InvoiceId = invoice.Id,
                        OrderId = line.OrderId,
                        OrderLineId = line.OrderLineId,
                        OrderNumber = line.OrderNumber,
                        ProductId = line.ProductId,
                        ProductCode = line.ProductCode,
                        ProductUnit = line.ProductUnit,
                        ProductUnitPrice = product.MarkedUpPrice,
                        Quantity = line.Quantity,
                        SubTotal = subTotal,
                        VATAmount = vatAmount,
                        TotalAmount = total
                    };

                    invoiceData.SaveInvoiceLine(newLine);
                }
            });

            invoiceData.SaveInvoice(invoice);

            //Update invoice totals
            UpdateOrderTotals(invoice.Id);
        }

        public void Save(Invoice model)
        {
            InvoiceData invoiceData = new InvoiceData(_db);
            invoiceData.SaveInvoice(model);
        }

        #region Update Order Totals
        public void UpdateOrderTotals(int invoiceId)
        {
            //Get order
            InvoiceData invoiceData = new InvoiceData(_db);
            var invoice = invoiceData.GetInvoiceById(invoiceId);

            double subTotal = 0;
            double VATAmount = 0;
            double Total = 0;

            invoice.InvoiceLines.ForEach(line =>
            {
                if (!line.IsReplaced)
                {
                    subTotal += Math.Round(line.SubTotal, 2);
                    VATAmount += Math.Round(line.VATAmount, 2);
                    Total += Math.Round(line.TotalAmount, 2);
                }
            });

            invoice.SubTotalAmount = subTotal;
            invoice.VATAmount = VATAmount;
            invoice.TotalAmount = Total;

            invoiceData.SaveInvoice(invoice);
        }
        #endregion

        public void MarkComplete(int id)
        {
            InvoiceData data = new InvoiceData(_db);

            var invoice = data.GetInvoiceById(id);

            invoice.DateCompleted = DateTime.Now;
            invoice.IsCompleted = true;

            data.SaveInvoice(invoice);
        }

        public Invoice GetInvoice(int customerId)
        {
            InvoiceData invoiceData = new InvoiceData(_db);
            return invoiceData.GetOpenInvoice(customerId);
        }

        public Invoice GetInvoiceById(int id)
        {
            InvoiceData data = new InvoiceData(_db);
            return data.GetInvoiceById(id);
        }

        public ProformaListViewModel CustomersWithProFormaInvoices()
        {
            InvoiceData invoiceData = new InvoiceData(_db);
            var viewModel = new ProformaListViewModel
            {
                Customers = invoiceData.CustomersWithProforma()
            };
            return viewModel;
        }

        public ProFormaViewModel GetProFormaViewModel(int id)
        {
            InvoiceData data = new InvoiceData(_db);
            CompanyInformationData compData = new CompanyInformationData(_db);

            ProFormaViewModel vm = new ProFormaViewModel
            {
                CompanyInfo = compData.GetInformation(),
                Invoice = data.GetInvoiceById(id)
            };

            return vm;
        }

        public List<Invoice> GetInvoicesCompletedToday()
        {
            InvoiceData data = new InvoiceData(_db);
            return data.GetInvoicesCompletedToday();
        }

        public List<Invoice> GetOpenInvoices()
        {
            InvoiceData invoiceData = new InvoiceData(_db);
            return invoiceData.GetOpenInvoices();
        }

        public List<Invoice> GetClosedInvoices()
        {
            InvoiceData invoiceData = new InvoiceData(_db);
            return invoiceData.GetClosedInvoices();
        }

        public InvoiceLine GetInvoiceLineByOrderLineId(int orderId, int orderLineId)
        {
            InvoiceData invoiceData = new InvoiceData(_db);
            return invoiceData.GetInvoiceLineByOrderLineId(orderId, orderLineId);
        }

        public void SaveLine(InvoiceLine model)
        {
            InvoiceData invoiceData = new InvoiceData(_db);
            invoiceData.SaveInvoiceLine(model);
        }

        public void DeleteLine(InvoiceLine model)
        {
            InvoiceData invoiceData = new InvoiceData(_db);
            invoiceData.DeleteLine(model);
        }
    }
}