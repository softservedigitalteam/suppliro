﻿using Postal;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Configuration;
using System.Net.Mail;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;

namespace Suppliro.Manager
{
    public static class EmailManager
    {
        public static void SendEmail(Enum.Enums.EmailType emailType, Enum.Enums.EmailSection emailSection, Postal.Email email)
        {
            var viewsPath = Path.GetFullPath(HostingEnvironment.MapPath(@"~/Views/Emails"));

            var engines = new ViewEngineCollection();
            if (!System.IO.File.Exists(viewsPath))
            {
                engines.Add(new FileSystemRazorViewEngine(viewsPath));
            }

            //Smtp Cliet
            SmtpSection smtpSection = (SmtpSection)ConfigurationManager.GetSection("mailSettings/" + emailSection.ToString());

            var client = new SmtpClient(smtpSection.Network.Host, smtpSection.Network.Port)
            {
                Credentials = new NetworkCredential(smtpSection.Network.UserName, smtpSection.Network.Password),
                EnableSsl = true
            };

            var emailService = new Postal.EmailService(engines, () => client);
            email.ViewName = emailType.ToString();

            // ********** COMMENT OUT EMAIL FOR TESTING **********
            //emailService.Send(email);
            // ********** COMMENT OUT EMAIL FOR TESTING **********
        }

        public static MailMessage TestEmail(Enum.Enums.EmailType emailType, Enum.Enums.EmailSection emailSection, Postal.Email email)
        {
            var emailService = new Postal.EmailService();

            return emailService.CreateMailMessage(email);
        }
    }
}