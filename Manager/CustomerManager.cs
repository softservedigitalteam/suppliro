﻿using Suppliro.Context;
using Suppliro.Data;
using Suppliro.Entities;
using Suppliro.Models;
using Suppliro.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Suppliro.Manager
{
    public class CustomerManager
    {
        DBContext _db;

        public CustomerManager(DBContext context)
        {
            _db = context;
        }

        public CustomerViewModel GetCustomerForView(ApplicationUser user)
        {
            CustomerViewModel _cusVM = new CustomerViewModel();

            //if(user.IsSupplier == true)
            //{
            //    SupplierData _data = new SupplierData();
            //    _cusVM.Supplier = _data.GetSupplierByUser(user.Id);
            //    //_cusVM.Customer = null;
            //}
            //else
            //{
            //    CustomerData _data = new CustomerData();
            //    _cusVM.Customer = _data.GetCustomerByUserId(user.Id);
            //   // _cusVM.Supplier = null;
            //}

            return _cusVM;
        }

        public Customer GetCustomerByUserId(string userId)
        {
            CustomerData _data = new CustomerData();
            var customer = _data.GetCustomerByUserId(userId) ?? new Customer();
            return customer;
        }

        public void SaveInformation(Customer model, string userId)
        {
            var customer = this.GetCustomerByUserId(userId);

            //Map Information properties and save
            customer.UserId = userId;
            customer.RegisteredName = model.RegisteredName;
            customer.TradingName = model.TradingName;
            customer.VATNumber = model.VATNumber;
            customer.TelNumber = model.TelNumber;
            customer.CellNumber = model.CellNumber;
            customer.BuyersName = model.BuyersName;
            customer.BuyersIDNumber = model.BuyersIDNumber;
            customer.EmailAddress = model.EmailAddress;
            customer.FaxNumber = model.FaxNumber;

            this.Save(customer);
        }

        public void SaveAddress(Customer model, string userId)
        {
            var customer = this.GetCustomerByUserId(userId);

            //Map Address properties and save
            customer.PhysicalAddress1 = model.PhysicalAddress1;
            customer.PhysicalAddress2 = model.PhysicalAddress2;
            customer.PhysicalPostalCode = model.PhysicalPostalCode;
            customer.PhysicalRegion = model.PhysicalRegion;
            customer.PhysicalTown = model.PhysicalTown;
            customer.PostalAddress1 = model.PostalAddress1;
            customer.PostalAddress2 = model.PostalAddress2;
            customer.PostalPostalCode = model.PostalPostalCode;
            customer.PostalRegion = model.PostalRegion;
            customer.PostalTown = model.PostalTown;

            this.Save(customer);
        }

        public void Save(Customer model)
        {
            CustomerData _data = new CustomerData();
            _data.Save(model);
        }
    }
}