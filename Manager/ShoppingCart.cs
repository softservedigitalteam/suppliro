﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Suppliro.Context;
using Suppliro.Data;
using Suppliro.Entities;
using Suppliro.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Suppliro.Manager
{
    public class ShoppingCart
    {
        string ShoppingCartId { get; set; }
        
        public static ShoppingCart GetCart(HttpContextBase context)
        {
            var cart = new ShoppingCart();
            cart.ShoppingCartId = cart.GetCartId(context);
            return cart;
        }

        public void AddToCart(Product product, int count)
        {
            CartData _cartData = new CartData();

            // Get the matching cart and album instances
            var cartItem = _cartData.GetCart(ShoppingCartId, product.Id);

            if (cartItem == null)
            {
                // Create a new cart item if no cart item exists
                cartItem = new Cart
                {
                    ProductId = product.Id,
                    CartId = ShoppingCartId,
                    Count = count,
                    DateCreated = DateTime.Now
                };

                _cartData.CreateCart(cartItem);
            }
            else
            {
                // If the item does exist in the cart, 
                // then update the quantity
                var existingCount = cartItem.Count;
                cartItem.Count = existingCount + count;
            }

            // Save changes
            _cartData.UpdateCart(cartItem);
        }

        public void RemoveFromCart(Cart cart)
        {
            CartData _cartData = new CartData();

            if (cart != null)
            {
                // Save changes
                _cartData.RemoveCart(cart);
            }
        }

        public void EmptyCart()
        {
            CartData _cartData = new CartData();

            _cartData.EmptyCart(ShoppingCartId);
        }

        public List<Cart> GetCartItems()
        {
            CartData _cartData = new CartData();

            return _cartData.GetCartItems(ShoppingCartId);
        }

        public int GetCount()
        {
            CartData _cartData = new CartData();

            return _cartData.GetCount(ShoppingCartId);
        }
        public double GetTotal()
        {
            CartData _cartData = new CartData();

            return _cartData.GetTotal(ShoppingCartId);
        }

        // Helper method to simplify shopping cart calls
        public static ShoppingCart GetCart(Controller controller)
        {
            return GetCart(controller.HttpContext);
        }

        // We're using HttpContextBase to allow access to cookies.
        public string GetCartId(HttpContextBase context)
        {
            if (context.Session["CartKey"] == null)
            {
                if (!string.IsNullOrWhiteSpace(context.User.Identity.Name))
                {
                    var store = new UserStore<ApplicationUser>(new DBContext());
                    var userManager = new UserManager<ApplicationUser>(store);
                    ApplicationUser user = userManager.FindByName(context.User.Identity.Name);

                    context.Session["CartKey"] = user.Id;
                }
            }

            return context.Session["CartKey"].ToString();
        }
    }
}