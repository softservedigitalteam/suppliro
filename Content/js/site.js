﻿
function printElement(elem, append, delimiter) {
    
    var domClone = elem.clone(true)[0];

    var $printSection = $(".printSection");

    if (!$printSection) {
        $printSection = document.createElement("div");
        $printSection.id = "printSection";
        document.body.appendChild($printSection);
    }

    if (append !== true) {
        $printSection.innerHTML = "";
    }

    else if (append === true) {
        if (typeof (delimiter) === "string") {
            $printSection.innerHTML += delimiter;
        }
        else if (typeof (delimiter) === "object") {
            $printSection.appendChlid(delimiter);
        }
    }

    var div = document.createElement("div");
    div.appendChild(domClone)
    debugger;
    $printSection.appendChild(div);
}