﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Suppliro.Areas.Admin.Models
{
    public class SupplierEmailOrderLine
    {
        public double Qty { get; set; }

        public string Unit { get; set; }

        public int ProductId { get; set; }

        public string Code { get; set; }
    }
}