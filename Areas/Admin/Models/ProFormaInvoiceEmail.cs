﻿using Postal;
using Suppliro.Areas.Admin.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Suppliro.Areas.Admin.Models
{
    public class ProFormaInvoiceEmail : Email
    {
        public string From { get; set; }

        public string To { get; set; }

        public string Subject { get; set; }

        public CustomerInvoiceViewModel Invoice { get; set; }
    }
}