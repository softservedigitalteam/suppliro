﻿using Suppliro.Areas.Admin.Manager;
using Suppliro.Areas.Admin.ViewModels;
using Suppliro.Context;
using Suppliro.Search;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Suppliro.Entities;
using System.ComponentModel.DataAnnotations;
using Suppliro.Data;
using Suppliro.Manager;
using Suppliro.Areas.Admin.Data;

namespace Suppliro.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class SupplierOrderController : Controller
    {
        // GET: Admin/SupplierOrder
        public ActionResult Index()
        {
            SupplierOrderManager mgr = new SupplierOrderManager(new DBContext());
            return View(mgr.GetSupplierOrdersPlacedToday());
        }

        public ActionResult History()
        {
            SupplierOrderManager mgr = new SupplierOrderManager(new DBContext());
            return View(mgr.GetSupplierOrdersExceptToday());
        }

        /// <summary>
        /// Returns the supplier order 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Order(int id)
        {
            SupplierOrderManager mgr = new SupplierOrderManager(new DBContext());
            var model = mgr.GetSupplierOrderById(id);
            if (model == null)
                return RedirectToAction("Index");

            return View("~/Areas/Admin/Views/SupplierOrder/Order.cshtml", model);
        }

        [HttpGet]
        public ActionResult GetSearchPartial(string id, string name)
        {
            ProductReplaceViewModel vm = new ProductReplaceViewModel
            {
                ProductId = Int32.Parse(id),
                ProductCode = name
            };
            return PartialView("~/Areas/Admin/Views/SupplierOrder/_SupplierOrderProductSearch.cshtml", vm);
        }

        [HttpGet]
        public ActionResult GetSupplierOrderProductReplacePartial(int iProductID, string strProductName, int iSupplierOrderLineID, int iSupplierOrderID)
        {
            SupplierOrderProductReplaceViewModel SupplierOrderProductReplaceViewModel = new SupplierOrderProductReplaceViewModel()
            {
                iProductID = iProductID,
                strProductCode = strProductName,
                iSupplierOrderLineID = iSupplierOrderLineID,
                iSupplierOrderID = iSupplierOrderID
            };

            CategoryManager cmgr = new CategoryManager(new DBContext());
            ProductManager pmgr = new ProductManager(new DBContext());

            //Get all product categories
            SupplierOrderProductReplaceViewModel.lstProductCategoryList = cmgr.GetAll();
            //Get all products by first category
            SupplierOrderProductReplaceViewModel.lstProductList = pmgr.GetAllProductsByCategoryId(SupplierOrderProductReplaceViewModel.lstProductCategoryList[0].Id);
            foreach (var item in SupplierOrderProductReplaceViewModel.lstProductList)
            {
                if (item.Supplier.TradingName != null)
                    item.DisplayName = item.Code + ": " + item.Supplier.TradingName;
                else
                    item.DisplayName = item.Code + ": ";
            }

            return PartialView("~/Areas/Admin/Views/SupplierOrder/SupplierOrderProductReplace.cshtml", SupplierOrderProductReplaceViewModel);
        }

        [HttpPost]
        public ActionResult SupplierOrderProductReplace(SupplierOrderProductReplaceViewModel SupplierOrderProductReplaceViewModel)
        {
            int x = 2;
            return null;
            //return PartialView("~/Areas/Admin/Views/SupplierOrder/SupplierOrderProductReplace.cshtml", SupplierOrderProductReplaceViewModel);
        }
        [HttpPost]
        public ActionResult ProductReplaceCategoryChange(int iSelectedProductCategory)
        {
            if (iSelectedProductCategory != 0)
            {
                ProductManager pmgr = new ProductManager(new DBContext());

                //Get all products by category
                List<Product> lstProducts = new List<Product>();
                lstProducts = pmgr.GetAllProductsByCategoryId(iSelectedProductCategory);
                foreach (var item in lstProducts)
                {
                    if (item.Supplier.TradingName != null)
                        item.DisplayName = item.Code + ": " + item.Supplier.TradingName;
                    else
                        item.DisplayName = item.Code + ": ";
                }

                return Json(new { lstProducts = lstProducts }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(null);
            }
        }

        [HttpGet]
        public ActionResult GetProductNewQuantityPartial(int iSupplierOrderID, int iSupplierOrderLineID, string strProductName)
        {
            ProductNewQuantity ProductNewQuantity = new ProductNewQuantity();
            ProductNewQuantity.iSupplierOrderID = iSupplierOrderID;
            ProductNewQuantity.iSupplierOrderLineID = iSupplierOrderLineID;
            ProductNewQuantity.strProductName = strProductName;

            return PartialView("~/Areas/Admin/Views/SupplierOrder/ProductNewQuantity.cshtml", ProductNewQuantity);
        }

        [HttpGet]
        public ActionResult GetSupplierOrderView(int supplierOrderID)
        {
            ViewBag.supplierOrderID = supplierOrderID;

            SupplierOrderManager mgr = new SupplierOrderManager(new DBContext());
            var model = mgr.GetSupplierOrderById(supplierOrderID);
            if (model == null)
                return RedirectToAction("Index");

            return PartialView("~/Areas/Admin/Views/SupplierOrder/SupplierOrderView.cshtml");
        }

        public void ProductNewQuantity(ProductNewQuantity ProductNewQuantity)
        {
            if (ProductNewQuantity.iProductQuantity > 0)
            {
                DBContext _db = new DBContext();
                SupplierOrderLineEdited SupplierOrderLineEdited = null;
                OrderLineEdited OrderLineEdited = null;

                SupplierOrderData SupplierOrderData = new SupplierOrderData(_db);
                OrderData OrderData = new OrderData(_db);
                OrderManager OrderManager = new OrderManager(_db);
                SupplierOrderManager SupplierOrderManager = new SupplierOrderManager(_db);
                InvoiceManager invMgr = new InvoiceManager(_db);

                //Get supplier order line quantity
                var supplierOrderLine = SupplierOrderData.GetSupplierOrderLineById(ProductNewQuantity.iSupplierOrderLineID);
                //Check if supplier order line edited exists for quantity change, if not, create new supplier order line edited 
                //Get supplier Order line edited
                SupplierOrderLineEdited = SupplierOrderData.GetSupplierOrderLineEditedBySupplierOrderLineId(ProductNewQuantity.iSupplierOrderLineID);
                if (SupplierOrderLineEdited == null)
                {
                    SupplierOrderLineEdited = new SupplierOrderLineEdited()
                    {
                        SupplierOrderLineId = supplierOrderLine.Id,
                        SupplierOrderId = supplierOrderLine.SupplierOrderId,
                        OrderId = supplierOrderLine.OrderId,
                        OrderLineId = supplierOrderLine.OrderLineId,
                        OrderNumber = supplierOrderLine.OrderNumber,
                        CustomerId = supplierOrderLine.CustomerId,
                        CustomerName = supplierOrderLine.CustomerName,
                        ProductId = supplierOrderLine.ProductId,
                        ProductCode = supplierOrderLine.ProductCode,
                        ProductUnit = supplierOrderLine.ProductUnit,
                        ProductPrice = supplierOrderLine.ProductPrice,
                        Quantity = supplierOrderLine.Quantity,
                    };
                }
                if (SupplierOrderLineEdited.Quantity == ProductNewQuantity.iProductQuantity)
                    SupplierOrderLineEdited.QuantityEdited = false;
                else
                    SupplierOrderLineEdited.QuantityEdited = true;

                //Set new supplier order line quantity
                supplierOrderLine.Quantity = ProductNewQuantity.iProductQuantity;
                //save changes
                SupplierOrderData.SaveOrderLine(supplierOrderLine);
                SupplierOrderData.SaveOrderLineEdited(SupplierOrderLineEdited);
                //Update Order Supplier Total
                SupplierOrderManager.CalculateSupplierOrderTotals(supplierOrderLine.SupplierOrderId);

                //Get order line
                var orderLine = OrderData.GetOrderLineById(supplierOrderLine.OrderLineId);
                //Check if order line edited exists for quantity change, if not, create new supplier order line edited 
                //Get Order line edited
                OrderLineEdited = OrderData.GetOrderLineEditedByOrderLineId(supplierOrderLine.OrderLineId);
                if (OrderLineEdited == null)
                {
                    OrderLineEdited = new OrderLineEdited
                    {
                        OrderLineId = orderLine.Id,
                        ProductId = orderLine.ProductId,
                        OrderId = orderLine.OrderId,
                        ProductCode = orderLine.ProductCode,
                        SupplierId = orderLine.SupplierId,
                        SupplierTradingName = orderLine.SupplierTradingName,
                        UnitPrice = orderLine.UnitPrice,
                        Unit = orderLine.Unit,
                        Quantity = orderLine.Quantity,
                        Amount = orderLine.Amount,
                        VATAmount = orderLine.VATAmount,
                    };
                }
                if (OrderLineEdited.Quantity == ProductNewQuantity.iProductQuantity)
                    OrderLineEdited.QuantityEdited = false;
                else
                    OrderLineEdited.QuantityEdited = true;

                //Set new order line quantity
                orderLine.Quantity = ProductNewQuantity.iProductQuantity;
                //Save changes
                OrderData.SaveLine(orderLine);
                OrderData.SaveLineEdited(OrderLineEdited);

                //Update Order Total
                OrderManager.UpdateOrderTotals(orderLine.Order.Id);
            }
        }

        public ActionResult SearchProducts(string id, string name, string query)
        {
            StringBuilder newQuery = new StringBuilder();

            string[] words = query.Split(' ');
            foreach (string word in words)
            {
                newQuery.Append(string.Format("+{0} ", word));
            }

            // create default Lucene search index directory
            if (!Directory.Exists(GoLucene._luceneDir)) Directory.CreateDirectory(GoLucene._luceneDir);

            // perform Lucene search
            List<SearchModel> _searchResults;
            _searchResults = (GoLucene.Search(newQuery.ToString().TrimEnd())).ToList();
            if (string.IsNullOrEmpty(query) && !_searchResults.Any())
                _searchResults = GoLucene.GetAllIndexRecords().ToList();

            ProductReplaceViewModel vm = new ProductReplaceViewModel
            {
                ProductId = Int32.Parse(id),
                ProductCode = name,
                Products = _searchResults
            };

            return PartialView("~/Areas/Admin/Views/SupplierOrder/_SupplierOrderProductSearch.cshtml", vm);
        }

        public ActionResult Remove(int id)
        {
            SupplierOrderManager SupplierOrderManager = new SupplierOrderManager(new DBContext());
            OrderManager OrderManager = new OrderManager(new DBContext());

            //Get supplier order data for order id
            SupplierOrderData SupplierOrderData = new SupplierOrderData(new DBContext());
            var line = SupplierOrderData.GetSupplierOrderLineById(id);

            //Get order id
            int orderId = line.OrderId;

            //First check supplier order line edited to check if it exists
            SupplierOrderManager.CheckSupplierOrderLineEdited(id);
            int supplierOrderId = SupplierOrderManager.RemoveSupplierOrderLine(id);

            //Calculate new totals
            SupplierOrderManager.CalculateSupplierOrderTotals(supplierOrderId);
            OrderManager.UpdateOrderTotals(orderId);

            return RedirectToAction("Order", new { id = supplierOrderId });
        }

        //Order Editing
        public ActionResult ProductAdd(int iSupplierOrderID)
        {
            SupplierOrderEditProductAdd SupplierOrderEditProductAdd = new SupplierOrderEditProductAdd();

            //Set order id
            SupplierOrderEditProductAdd.iOrderID = iSupplierOrderID;
            //Get supplier open orders
            SupplierOrderManager mgr = new SupplierOrderManager(new DBContext());
            var OpenOrders = mgr.GetOpenOrdersWithoutLines();

            CategoryManager cmgr = new CategoryManager(new DBContext());
            SupplierManager smgr = new SupplierManager(new DBContext());
            ProductManager pmgr = new ProductManager(new DBContext());

            //Order number list
            SupplierOrderEditProductAdd.lstOrderNumbers = new List<string>();
            //Get supplier Order Details
            List<SupplierOrder> lstSupplerOrdersList = new List<SupplierOrder>();
            if (OpenOrders.Count > 0)
                foreach (var item in OpenOrders)
                    lstSupplerOrdersList.Add(mgr.GetSupplierOrderById(item.Id));

            //Add order to order list
            if (lstSupplerOrdersList.Count > 0)
                foreach (var item in lstSupplerOrdersList)
                    if (item.SupplierOrderLines.Count > 0)
                        foreach (var item2 in item.SupplierOrderLines)
                            if (!(SupplierOrderEditProductAdd.lstOrderNumbers.Contains(item2.OrderNumber)))
                                SupplierOrderEditProductAdd.lstOrderNumbers.Add(item2.OrderNumber);

            //Get first Customer Name
            foreach(var item in lstSupplerOrdersList)
            {
                if (SupplierOrderEditProductAdd.strCustomerName == null || SupplierOrderEditProductAdd.strCustomerName == "")
                    SupplierOrderEditProductAdd.strCustomerName = item.SupplierOrderLines.Where(item2 => item2.OrderNumber == SupplierOrderEditProductAdd.lstOrderNumbers[0]).ToList()[0].CustomerName;
                else
                    break;
            }
            //Get all product categories
            SupplierOrderEditProductAdd.lstProductCategoryList = cmgr.GetAll();
            //Get all products by first category
            SupplierOrderEditProductAdd.lstProductList = pmgr.GetAllProductsByCategoryId(SupplierOrderEditProductAdd.lstProductCategoryList[0].Id);
            foreach(var item in SupplierOrderEditProductAdd.lstProductList)
            {
                item.DisplayName = item.Code + ": " + item.Supplier.TradingName;
            }
            //Get first product info
            SupplierOrderEditProductAdd.product = SupplierOrderEditProductAdd.lstProductList[0];

            return View(SupplierOrderEditProductAdd);
        }

        //Order Editing Add
        [HttpPost]
        public ActionResult ProductAdd(SupplierOrderEditProductAdd SupplierOrderEditProductAdd)
        {
            if (SupplierOrderEditProductAdd.iQuantity > 0)
            {
                //Database Context
                DBContext db = new DBContext();
                //Managers
                ProductManager pmgr = new ProductManager(db);
                SupplierOrderManager somgr = new SupplierOrderManager(db);
                OrderManager omgr = new OrderManager(db);

                var product = pmgr.GetProductById(SupplierOrderEditProductAdd.product.Id);

                OrderData orderData = new OrderData(new DBContext());
                var order = orderData.GetOrderByOrderNumber(SupplierOrderEditProductAdd.strSelectedOrderNumber);
                if (order != null)
                {
                    order.Edited = true;

                    var uom = "";

                    if (product.UoMValue > 0)
                    {
                        uom = string.Format("{0} x {1} ({2})", product.MinOrder, product.UoMValue, product.Unit);
                    }
                    else
                    {
                        uom = string.Format("{0} ({1})", product.MinOrder, product.Unit);
                    }

                    var orderLine = new OrderLine
                    {
                        ProductId = product.Id,
                        OrderId = order.Id,
                        ProductCode = string.IsNullOrWhiteSpace(product.DisplayName) ? product.Code : product.DisplayName,
                        SupplierId = product.Supplier.Id,
                        SupplierTradingName = product.Supplier.TradingName,
                        UnitPrice = product.MarkedUpPrice,
                        Unit = uom,
                        Quantity = SupplierOrderEditProductAdd.iQuantity,
                        Amount = Math.Round(product.MarkedUpPrice * SupplierOrderEditProductAdd.iQuantity, 2),
                        VATAmount = Math.Round((product.MarkedUpPrice * SupplierOrderEditProductAdd.iQuantity) * 0.14, 2)
                    };

                    orderData.AddOrderLine(orderLine);

                    //Second order line edited for copy (reference for edited order)
                    var orderLineEdited = new OrderLineEdited
                    {
                        OrderLineId = orderLine.Id,
                        ProductId = product.Id,
                        OrderId = order.Id,
                        ProductCode = string.IsNullOrWhiteSpace(product.DisplayName) ? product.Code : product.DisplayName,
                        SupplierId = product.Supplier.Id,
                        SupplierTradingName = product.Supplier.TradingName,
                        UnitPrice = product.MarkedUpPrice,
                        Unit = uom,
                        Quantity = SupplierOrderEditProductAdd.iQuantity,
                        Amount = Math.Round(product.MarkedUpPrice * SupplierOrderEditProductAdd.iQuantity, 2),
                        VATAmount = Math.Round((product.MarkedUpPrice * SupplierOrderEditProductAdd.iQuantity) * 0.14, 2)
                    };
                    
                    //second order line copy
                    orderData.AddOrderLineEdited(orderLineEdited);
                    //save order
                    orderData.SaveOrder(order);
                    //Update Totals
                    omgr.UpdateOrderTotals(order.Id);
                    //Build/Append to supplier order
                    somgr.BuildSupplierOrderEdit(order.Id, orderLine);

                    //Return order number for redirect
                    return Json(new { orderID = SupplierOrderEditProductAdd.iOrderID }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(null, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        public class SupplierOrderEditProductAdd
        {
            public int iOrderID { get; set; }
            public int iSelectedProductCategory { get; set; }
            public List<Category> lstProductCategoryList { get; set; }
            public int iSelectedProduct { get; set; }
            public List<Product> lstProductList { get; set; }
            public Product product { get; set; }
            public List<string> lstOrderNumbers { get; set; }
            public string strSelectedOrderNumber { get; set; }
            public string strCustomerName { get; set; }
            [Range(0, int.MaxValue, ErrorMessage = "The input should be a positive value.")]
            public int iQuantity { get; set; }
            public double dblActualQuantity { get; set; }
            public double dblActualPrice { get; set; }
        }

        [HttpPost]
        public ActionResult ProductCategoryChange(int iSelectedProductCategory)
        {
            if (iSelectedProductCategory != 0)
            {
                SupplierOrderEditProductAdd SupplierOrderEditProductAdd = new SupplierOrderEditProductAdd();
                CategoryManager cmgr = new CategoryManager(new DBContext());
                SupplierManager smgr = new SupplierManager(new DBContext());
                ProductManager pmgr = new ProductManager(new DBContext());

                //Get list of suppliers
                var lstSuppliers = smgr.GetAllSuppliers();

                //Get all product categories
                SupplierOrderEditProductAdd.lstProductCategoryList = cmgr.GetAll();
                //Get all products by first category
                SupplierOrderEditProductAdd.lstProductList = pmgr.GetAllProductsByCategoryId(iSelectedProductCategory);
                foreach (var item in SupplierOrderEditProductAdd.lstProductList)
                {
                    string strSupplierName = lstSuppliers.Suppliers.Where(x => x.Id == item.SupplierId).ToList()[0].TradingName;
                    if (strSupplierName != null)
                        item.DisplayName = item.Code + ": " + strSupplierName;
                }
                //Get first product info
                SupplierOrderEditProductAdd.product = SupplierOrderEditProductAdd.lstProductList[0];

                return Json(SupplierOrderEditProductAdd, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(null);
            }
        }

        [HttpPost]
        public ActionResult ProductChange(int iSelectedProduct)
        {
            if (iSelectedProduct != 0)
            {
                SupplierOrderEditProductAdd SupplierOrderEditProductAdd = new SupplierOrderEditProductAdd();
                ProductManager pmgr = new ProductManager(new DBContext());

                //Get product info
                SupplierOrderEditProductAdd.product = pmgr.GetProductById(iSelectedProduct);
                return Json(SupplierOrderEditProductAdd, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(null);
            }
        }

        [HttpPost]
        public ActionResult OrderNumberChange(string strOrderNumberSelect)
        {
            string strCustomerName = "";
            if (strOrderNumberSelect != "" && strOrderNumberSelect != "")
            {
                //Get supplier open orders
                SupplierOrderManager mgr = new SupplierOrderManager(new DBContext());
                var OpenOrders = mgr.GetOpenOrdersWithoutLines();

                //Get supplier Order Details
                List<SupplierOrder> lstSupplerOrdersList = new List<SupplierOrder>();
                if (OpenOrders.Count > 0)
                    foreach (var item in OpenOrders)
                        lstSupplerOrdersList.Add(mgr.GetSupplierOrderById(item.Id));

                //Get Customer Name
                foreach (var item in lstSupplerOrdersList)
                {
                    try
                    {
                        if (strCustomerName == null || strCustomerName == "")
                            strCustomerName = item.SupplierOrderLines.Where(item2 => item2.OrderNumber == strOrderNumberSelect).ToList()[0].CustomerName;
                        else
                            break;
                    }
                    catch (Exception ex) { }
                }
                return Json( new { strCustomerName = strCustomerName }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(null);
            }
        }
    }
}