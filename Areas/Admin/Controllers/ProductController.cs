﻿using OfficeOpenXml;
using Suppliro.Areas.Admin.Data;
using Suppliro.Areas.Admin.Manager;
using Suppliro.Areas.Admin.ViewModels;
using Suppliro.Context;
using Suppliro.Data;
using Suppliro.Entities;
using Suppliro.Search;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Suppliro.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class ProductController : Controller
    {
        // GET: Admin/Product
        public ActionResult Index()
        {
            DBContext context = new DBContext();
            ProductManager mgr = new ProductManager(context);
            return View(mgr.GetProductListViewModel());
        }

        // GET: Pproduc/Create
        public ActionResult Create()
        {
            DBContext context = new DBContext();
            ProductManager mgr = new ProductManager(context);
            return PartialView("Create", mgr.GetProductViewModel(null));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ProductViewModel model)
        {
            if (ModelState.IsValid)
            {
                DBContext context = new DBContext();
                ProductManager mgr = new ProductManager(context);
                mgr.SaveProduct(model);
                //Save product
                return Json(new { success = true });
            }

            return PartialView("Create", model);
        }

        public ActionResult Edit(int? id)
        {
            if (!id.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            DBContext context = new DBContext();
            ProductManager mgr = new ProductManager(context);
            ProductViewModel model = mgr.GetProductViewModel(id.Value);

            if (model == null)
            {
                return HttpNotFound();
            }
            return PartialView("Create", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Code,DisplayName,Description,SupplierId,CategoryId,InStock,OriginalPrice,MarkUpPercentage,MarkedUpPrice,MinOrder,Unit,MinOrderUnit,Vat")] ProductViewModel model)
        {
            if (ModelState.IsValid)
            {
                DBContext context = new DBContext();
                ProductManager mgr = new ProductManager(context);
                mgr.SaveProduct(model);

                //Save product
                return Json(new { success = true });
            }

            return PartialView("Create", model);
        }

        public ActionResult UploadProducts(FormCollection formCollection)
        {
            if (Request != null)
            {
                HttpPostedFileBase file = Request.Files["UploadedFile"];
                var supplierId = formCollection["SupplierId"];

                if ((file != null) && (file.ContentLength > 0) && !string.IsNullOrEmpty(file.FileName))
                {
                    string fileName = file.FileName;
                    string fileContentType = file.ContentType;
                    byte[] fileBytes = new byte[file.ContentLength];
                    var data = file.InputStream.Read(fileBytes, 0, Convert.ToInt32(file.ContentLength));
                    var productList = new List<Product>();
                    using (var package = new ExcelPackage(file.InputStream))
                    {
                        var currentSheet = package.Workbook.Worksheets;
                        var workSheet = currentSheet.First();
                        var noOfCol = workSheet.Dimension.End.Column;
                        var noOfRow = workSheet.Dimension.End.Row;

                        DBContext context = new DBContext();
                        ProductManager _prodMgr = new ProductManager(context);                        

                        for (int rowIterator = 1; rowIterator <= noOfRow; rowIterator++)
                        {
                            double minOrder = 1;
                            if(workSheet.Cells[rowIterator, 4].Value != null)
                                minOrder = Convert.ToDouble(workSheet.Cells[rowIterator, 4].Value.ToString());

                            Product prod = new Product();
                            prod.SupplierId = Convert.ToInt32(supplierId);
                            prod.CategoryName = workSheet.Cells[rowIterator, 1].Value.ToString();
                            prod.Code = workSheet.Cells[rowIterator, 2].Value.ToString();
                            prod.UoMDefaultCode = workSheet.Cells[rowIterator, 3].Value.ToString();                            
                            prod.MinOrder = minOrder;
                            prod.UoMValue = Convert.ToDouble(workSheet.Cells[rowIterator, 5].Value == null ? "0" : workSheet.Cells[rowIterator, 5].Value.ToString());
                            prod.Unit = workSheet.Cells[rowIterator, 6].Value.ToString();
                            prod.OriginalPrice = Convert.ToDouble(workSheet.Cells[rowIterator, 7].Value.ToString());

                            productList.Add(prod);
                        }

                        if (productList.Count > 0)
                            _prodMgr.UploadProducts(productList, supplierId);
                    }
                }
            }

            return RedirectToAction("Index");
        }

        public ActionResult UpdateProducts(FormCollection formCollection)
        {
            if (Request != null)
            {
                HttpPostedFileBase file = Request.Files["UploadedFile"];
                var supplierId = formCollection["SupplierId"];

                if ((file != null) && (file.ContentLength > 0) && !string.IsNullOrEmpty(file.FileName))
                {
                    string fileName = file.FileName;
                    string fileContentType = file.ContentType;
                    byte[] fileBytes = new byte[file.ContentLength];
                    var data = file.InputStream.Read(fileBytes, 0, Convert.ToInt32(file.ContentLength));
                    var productList = new List<Product>();
                    using (var package = new ExcelPackage(file.InputStream))
                    {
                        var currentSheet = package.Workbook.Worksheets;
                        var workSheet = currentSheet.First();
                        var noOfCol = workSheet.Dimension.End.Column;
                        var noOfRow = workSheet.Dimension.End.Row;

                        DBContext context = new DBContext();
                        ProductManager _prodMgr = new ProductManager(context);

                        for (int rowIterator = 1; rowIterator <= noOfRow; rowIterator++)
                        {
                            var code = workSheet.Cells[rowIterator, 1].Value;
                            var price = workSheet.Cells[rowIterator, 2].Value;

                            if (code != null || price != null)
                            {
                                Product prod = new Product();
                                prod.SupplierId = Convert.ToInt32(supplierId);
                                prod.Code = code.ToString();
                                prod.OriginalPrice = Convert.ToDouble(price);
                                productList.Add(prod);
                            }
                        }

                        if (productList.Count > 0)
                            _prodMgr.UpdateProducts(productList, supplierId);
                    }
                }
            }

            return RedirectToAction("Index");
        }

        public ActionResult OptimizeSearchIndex()
        {
            GoLucene.Optimize();
            return RedirectToAction("Index");
        }

        public ActionResult AddProductsSearchIndex()
        {
            DBContext context = new DBContext();
            ProductManager _mgr = new ProductManager(context);
            _mgr.UpdateSearchIndex();

            return RedirectToAction("Index");
        }

        public ActionResult ClearSearchIndex()
        {
            DBContext context = new DBContext();
            ProductManager _mgr = new ProductManager(context);
            _mgr.ClearSearchIndex();

            return RedirectToAction("Index");
        }

        
    }
}