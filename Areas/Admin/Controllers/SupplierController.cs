﻿using Suppliro.Areas.Admin.Manager;
using Suppliro.Context;
using Suppliro.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Suppliro.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class SupplierController : Controller
    {
        // GET: Admin/Supplier
        public ActionResult Index()
        {
            SupplierManager _mgr = new SupplierManager(new DBContext());
            return View(_mgr.GetAllSuppliers());
        }

        public ActionResult Edit(int? id)
        {
            SupplierManager _mgr = new SupplierManager(new DBContext());
            return View(_mgr.GetSupplier(id));
        }

        public ActionResult Add(Supplier model)
        {
            if (!ModelState.IsValid)
            {
                return View("Edit", model);
            }

            SupplierManager _mgr = new SupplierManager(new DBContext());
            model = _mgr.AddSupplier(model);
            return View("Edit", model);
        }
    }
}