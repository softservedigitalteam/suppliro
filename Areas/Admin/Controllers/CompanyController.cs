﻿using Suppliro.Areas.Admin.Data;
using Suppliro.Context;
using Suppliro.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Suppliro.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class CompanyController : Controller
    {
        // GET: Admin/Company
        public ActionResult Index()
        {
            CompanyInformationData data = new CompanyInformationData(new DBContext());
            var model = data.GetInformation();
            return View(model != null ? model : new CompanyInformation());
        }

        public ActionResult Update(CompanyInformation model)
        {
            CompanyInformationData data = new CompanyInformationData(new DBContext());
            data.Save(model);

            return RedirectToAction("Index");
        }
    }
}