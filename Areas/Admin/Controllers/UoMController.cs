﻿using Suppliro.Areas.Admin.Manager;
using Suppliro.Areas.Admin.ViewModels;
using Suppliro.Context;
using Suppliro.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Suppliro.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class UoMController : Controller
    {
        // GET: Admin/UoM
        public ActionResult Index()
        {
            UoMManager mgr = new UoMManager(new DBContext());
            return View(mgr.GetUoMAll());
        }

        public ActionResult EditUoM(int? id)
        {
            UoMManager mgr = new UoMManager(new DBContext());            
            return View(mgr.GetUoMViewModelById(id));
        }

        public ActionResult SaveUoM(UoMViewModel model)
        {
            UoMManager mgr = new UoMManager(new DBContext());
            mgr.SaveUoM(model.UoM);
            return RedirectToAction("Index");
        }

        public ActionResult AddMapping(int? id, int? uomId)
        {
            if(id.HasValue)
            {
                UoMManager mgr = new UoMManager(new DBContext());
                return View(mgr.GetUoMMapperVM(id.Value, uomId));
            }
            else
            {
                return RedirectToAction("Index");
            }
        }

        public ActionResult SaveMapping(UoMMapperViewModel model)
        {
            UoMManager mgr = new UoMManager(new DBContext());
            var uom = mgr.GetUoMById(model.UoMMapper.UoM.Id);
            var uomCompareTo = mgr.GetUoMById(model.UoMMapper.CompareToUoMId);

            var uomId = uom.Id;

            model.UoMMapper.UoM = null;

            model.UoMMapper.UoMId = uomId;
            model.UoMMapper.CompareToUoMCode = uomCompareTo.Code;

            mgr.SaveUoMMapper(model.UoMMapper);

            return RedirectToAction("EditUoM", new { id = uomId });
        }
    }
}