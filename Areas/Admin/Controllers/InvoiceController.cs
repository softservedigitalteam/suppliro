﻿using Suppliro.Areas.Admin.Data;
using Suppliro.Areas.Admin.Models;
using Suppliro.Areas.Admin.ViewModels;
using Suppliro.Context;
using Suppliro.Entities;
using Suppliro.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Suppliro.Data;

namespace Suppliro.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class InvoiceController : Controller
    {
        // GET: Admin/Invoice
        public ActionResult Index()
        {
            OrderManager ordMgr = new OrderManager(new DBContext());
            return View(ordMgr.GetOrdersCompletedToday());
        }

        /// <summary>
        /// Returns customers with open pro forma invoices
        /// </summary>
        /// <returns></returns>
        public ActionResult Customers()
        {
            InvoiceManager invMgr = new InvoiceManager(new DBContext());
            return View(invMgr.CustomersWithProFormaInvoices());
        }

        public ActionResult DownloadActionAsPDF(int? id)
        {
            OrderManager ordMgr = new OrderManager(new DBContext());
            var model = ordMgr.GetCustomerInvoicesViewModel(id);

            if (model == null || model.Invoices == null)
                return RedirectToAction("Index");

            //Code to get content
            return new Rotativa.ActionAsPdf("InvoicePDF", new { id = id }) { FileName = model.Invoices.FirstOrDefault().OrderNumber + ".pdf" };
        }

        public ActionResult DownloadActionAsPDFEdited(int? id)
        {
            OrderManager ordMgr = new OrderManager(new DBContext());
            OrderData OrderData = new OrderData(new DBContext());

            var model = ordMgr.GetCustomerInvoicesViewModelEdited(id);

            if (model == null || model.Invoices == null || model.Invoices.Count == 0)
                return RedirectToAction("Index");

            List<OrderLineEdited> lstOrderLineEdited = new List<OrderLineEdited>();

            foreach (var item in model.Invoices)
            {
                var lstOrderLineEditedList = OrderData.GetOrderLinesEditedByOrderId(item.Id);
                foreach (var item2 in lstOrderLineEditedList)
                    if (!(lstOrderLineEdited.Any(x => x.Id == item2.Id)))
                        lstOrderLineEdited.Add(item2);
            }

            model.lstOrderLineEditedAdded = new List<OrderLineEdited>();
            model.lstOrderLineEditedRemoved = new List<OrderLineEdited>();
            model.lstOrderLineEditedChanged = new List<OrderLineEdited>();

            foreach (var item in model.Invoices)
            {
                //Compare changes to original order line and order line edited
                if (lstOrderLineEdited.Count > 0 && item.OrderLines.Count > 0)
                {
                    foreach (var item2 in lstOrderLineEdited)
                    {
                        if (item.OrderLines.Any(x => x.Id == item2.OrderLineId))
                        {
                            if (!(model.lstOrderLineEditedAdded.Any(y => y.Id == item2.Id)))
                            {
                                model.lstOrderLineEditedAdded.Add(item2);
                                if (item2.QuantityEdited == true)
                                    model.lstOrderLineEditedChanged.Add(item2);
                            }
                        }
                        else
                        {
                            if (!(model.lstOrderLineEditedRemoved.Any(y => y.Id == item2.Id)))
                                model.lstOrderLineEditedRemoved.Add(item2);
                        }
                    }
                }
            }

            //Code to get content
            return new Rotativa.ActionAsPdf("InvoicePDFEdited", new { id = id }) { FileName = model.Invoices.FirstOrDefault().OrderNumber + ".pdf" };
        }

        [AllowAnonymous]
        public ActionResult InvoicePDF(int? id)
        {
            OrderManager ordMgr = new OrderManager(new DBContext());
            var model = ordMgr.GetCustomerInvoicesViewModel(id);

            return View(model);
        }

        [AllowAnonymous]
        public ActionResult InvoicePDFEdited(int? id)
        {
            OrderManager ordMgr = new OrderManager(new DBContext());
            OrderData OrderData = new OrderData(new DBContext());

            var model = ordMgr.GetCustomerInvoicesViewModelEdited(id);

            List<OrderLineEdited> lstOrderLineEdited = new List<OrderLineEdited>();

            foreach (var item in model.Invoices)
            {
                var lstOrderLineEditedList = OrderData.GetOrderLinesEditedByOrderId(item.Id);
                foreach (var item2 in lstOrderLineEditedList)
                    if (!(lstOrderLineEdited.Any(x => x.Id == item2.Id)))
                        lstOrderLineEdited.Add(item2);
            }

            model.lstOrderLineEditedAdded = new List<OrderLineEdited>();
            model.lstOrderLineEditedRemoved = new List<OrderLineEdited>();
            model.lstOrderLineEditedChanged = new List<OrderLineEdited>();

            foreach (var item in model.Invoices)
            {
                //Compare changes to original order line and order line edited
                if (lstOrderLineEdited.Count > 0 && item.OrderLines.Count > 0)
                {
                    foreach (var item2 in lstOrderLineEdited)
                    {
                        if (item.OrderLines.Any(x => x.Id == item2.OrderLineId))
                        {
                            if (!(model.lstOrderLineEditedAdded.Any(y => y.Id == item2.Id)))
                            {
                                model.lstOrderLineEditedAdded.Add(item2);
                                if (item2.QuantityEdited == true)
                                    model.lstOrderLineEditedChanged.Add(item2);
                            }
                        }
                        else
                        {
                            if (!(model.lstOrderLineEditedRemoved.Any(y => y.Id == item2.Id)))
                                model.lstOrderLineEditedRemoved.Add(item2);
                        }
                    }
                }
            }

            return View(model);
        }

        public ActionResult MakrInvoiced(int id)
        {
            OrderManager ordMgr = new OrderManager(new DBContext());
            ordMgr.MarkOrderAsInvoiced(id);
            return RedirectToAction("Index");
        }

        public ActionResult History()
        {
            OrderManager ordMgr = new OrderManager(new DBContext());
            return View(ordMgr.GetOrdersInvoiced(null));
        }

        public ActionResult HistoryAsPDF(int? id)
        {
            OrderManager ordMgr = new OrderManager(new DBContext());
            var model = ordMgr.GetnvoicedViewModel(id);

            //Code to get content
            return new Rotativa.ActionAsPdf("HistoryPDF", new { id = id }) { FileName = model.Invoices.FirstOrDefault().OrderNumber + ".pdf" };
        }

        [AllowAnonymous]
        public ActionResult HistoryPDF(int? id)
        {
            OrderManager ordMgr = new OrderManager(new DBContext());
            var model = ordMgr.GetnvoicedViewModel(id);

            return View(model);
        }

        public ActionResult ProFormas()
        {
            InvoiceManager mgr = new InvoiceManager(new DBContext());
            return View(mgr.GetInvoicesCompletedToday());
        }

        public ActionResult MarkComplete(int id)
        {
            InvoiceManager mgr = new InvoiceManager(new DBContext());
            mgr.MarkComplete(id);

            return RedirectToAction("ProFormas");
        }

        public ActionResult ProformaAsPDF(int id)
        {
            InvoiceManager mgr = new InvoiceManager(new DBContext());
            var model = mgr.GetProFormaViewModel(id);
            //Code to get content
            return new Rotativa.ActionAsPdf("ProformaPDF", new { id = id }) { FileName = model.Invoice.CustomerTradingName + ".pdf" };
        }

        [AllowAnonymous]
        public ActionResult ProformaPDF(int id)
        {
            InvoiceManager mgr = new InvoiceManager(new DBContext());

            return View(mgr.GetProFormaViewModel(id));
        }

        /// <summary>
        /// Returns pro forma for particular customer
        /// </summary>
        /// <param name="id">Customer Id</param>
        /// <returns></returns>
        public ActionResult Proforma(int id)
        {
            InvoiceManager mgr = new InvoiceManager(new DBContext());
            return View(mgr.GetInvoiceById(id));
        }

        public ActionResult ProFormaHistory()
        {
            InvoiceManager mgr = new InvoiceManager(new DBContext());
            return View(mgr.GetClosedInvoices());
        }
    }
}