﻿using Suppliro.Areas.Admin.Manager;
using Suppliro.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Suppliro.Areas.Admin.Controllers
{
    public class PickupController : Controller
    {
        // GET: Admin/Pickup
        public ActionResult Index()
        {
            PickupFormManager mgr = new PickupFormManager(new DBContext());
            return View(mgr.GetPickUpFormViewModel());
        }

        public ActionResult GetPickUpForm(int id)
        {
            PickupFormManager mgr = new PickupFormManager(new DBContext());
            return PartialView(mgr.GetPickUpFormById(id));
        }

        public ActionResult DownloadActionAsPDF(int id)
        {
            //Code to get content
            return new Rotativa.ActionAsPdf("PickupFormPDF", new { id = id }) { FileName = "PickupForm-" + DateTime.Now.ToString("dd/MM/yyyy") + ".pdf" };
        }

        [AllowAnonymous]
        public ActionResult PickupFormPDF(int id)
        {
            PickupFormManager mgr = new PickupFormManager(new DBContext());
            var model = mgr.GetPickUpFormById(id);

            mgr.MarkComplete(id);

            return View(model);
        }
    }
}