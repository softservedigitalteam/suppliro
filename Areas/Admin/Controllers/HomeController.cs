﻿using Postal;
using Suppliro.Areas.Admin.Data;
using Suppliro.Areas.Admin.Manager;
using Suppliro.Context;
using Suppliro.Entities;
using Suppliro.Manager;
using Suppliro.Models.Emails;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Configuration;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using Suppliro.Areas.Admin.ViewModels;

namespace Suppliro.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class HomeController : Controller
    {
        // GET: Admin/Home
        public ActionResult Index()
        {
            HomeIndexViewModel HomeIndexViewModel = new HomeIndexViewModel();
            SupplierOrderManager supplierOrderManager = new SupplierOrderManager(new DBContext());

            HomeIndexViewModel.lstSupplierOrder = supplierOrderManager.GetOpenOrdersWithoutLines();
            return View(HomeIndexViewModel);
        }

        //OLD SUPPLIER ORDER CODE
        public ActionResult SupplierOrder(int id)
        {
            SupplierOrderManager supplierOrderManager = new SupplierOrderManager(new DBContext());
            var supplierOrderBySupplierID = supplierOrderManager.GetSupplierOrderBySupplierId(id);
            return PartialView(supplierOrderManager.GetSupplierOrderBySupplierId(id));
        }

        public ActionResult SupplierOrderEdited(int id)
        {
            HomeIndexViewModel HomeIndexViewModel = new HomeIndexViewModel();
            SupplierOrderManager supplierOrderManager = new SupplierOrderManager(new DBContext());

            HomeIndexViewModel.SupplierOrder = supplierOrderManager.GetSupplierOrderBySupplierId(id);

            HomeIndexViewModel.lstSupplierOrderLineAdded = new List<SupplierOrderLineEdited>();
            HomeIndexViewModel.lstSupplierOrderLineRemoved = new List<SupplierOrderLineEdited>();
            HomeIndexViewModel.lstSupplierOrderLineChanged = new List<SupplierOrderLineEdited>();

            //Compare changes to original supplier order line and supplier order line edited
            if (HomeIndexViewModel.SupplierOrder.SupplierOrderLinesEdited.Count > 0 && HomeIndexViewModel.SupplierOrder.SupplierOrderLines.Count > 0)
            {
                foreach (var item in HomeIndexViewModel.SupplierOrder.SupplierOrderLinesEdited)
                {
                    if (HomeIndexViewModel.SupplierOrder.SupplierOrderLines.Any(x => x.Id == item.SupplierOrderLineId))
                    {
                        HomeIndexViewModel.lstSupplierOrderLineAdded.Add(item);
                        //check if quantity value has changed
                        if (item.QuantityEdited == true)
                            HomeIndexViewModel.lstSupplierOrderLineChanged.Add(item);
                    } 
                    else
                        HomeIndexViewModel.lstSupplierOrderLineRemoved.Add(item);
                }
            }

            return PartialView(HomeIndexViewModel);
        }

        public async Task<ActionResult> ProcessOrders(int id)
        {
            SupplierOrderManager supplierOrderManager = new SupplierOrderManager(new DBContext());
            await supplierOrderManager.ProcessOrders(id);

            return RedirectToAction("Index");
        }

        public ActionResult MarkComplete(int id)
        {
            SupplierOrderManager supplierOrderManager = new SupplierOrderManager(new DBContext());
            supplierOrderManager.MarkComplete(id);

            return RedirectToAction("Index");
        }

    }
}