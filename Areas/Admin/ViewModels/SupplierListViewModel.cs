﻿using Suppliro.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Suppliro.Areas.Admin.ViewModels
{
    public class SupplierListViewModel
    {
        public List<Supplier> Suppliers { get; set; }
    }
}