﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Suppliro.Entities;

namespace Suppliro.Areas.Admin.ViewModels
{
    public class HomeIndexViewModel
    {
        public List<SupplierOrder> lstSupplierOrder { get; set; }
        public SupplierOrder SupplierOrder { get; set; }
        public List<SupplierOrderLineEdited> lstSupplierOrderLineAdded { get; set; }
        public List<SupplierOrderLineEdited> lstSupplierOrderLineRemoved { get; set; }
        public List<SupplierOrderLineEdited> lstSupplierOrderLineChanged { get; set; }
    }
}
