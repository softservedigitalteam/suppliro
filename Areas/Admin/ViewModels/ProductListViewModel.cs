﻿using Suppliro.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Suppliro.Areas.Admin.ViewModels
{
    public class ProductListViewModel
    {
        public List<Product> Products { get; set; }

        public int SupplierId { get; set; }

        public IEnumerable<SelectListItem> Suppliers { get; set; }
    }
}