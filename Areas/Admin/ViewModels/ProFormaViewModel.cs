﻿using Suppliro.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Suppliro.Areas.Admin.ViewModels
{
    public class ProFormaViewModel
    {
        public CompanyInformation CompanyInfo { get; set; }

        public Invoice Invoice { get; set; }
    }
}