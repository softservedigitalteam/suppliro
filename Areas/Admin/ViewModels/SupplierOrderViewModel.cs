﻿using Suppliro.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Suppliro.Areas.Admin.ViewModels
{
    public class SupplierOrderViewModel
    {
        public SupplierOrder SupplierOrder { get; set; }
        public List<SupplierOrderLineEdited> lstSupplierOrderLinesAdded { get; set; }
        public List<SupplierOrderLineEdited> lstSupplierOrderLinesChanged { get; set; }
        public List<SupplierOrderLineEdited> lstSupplierOrderLinesRemoved { get; set; }
    }
}
