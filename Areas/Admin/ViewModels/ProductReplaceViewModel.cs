﻿using Suppliro.Search;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Suppliro.Areas.Admin.ViewModels
{
    public class ProductReplaceViewModel
    {
        public int ProductId { get; set; }

        public string ProductCode { get; set; }

        public List<SearchModel> Products { get; set; }
    }
}