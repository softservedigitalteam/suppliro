﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Suppliro.Areas.Admin.ViewModels
{
    public class ProductNewQuantity
    {
        public string strProductName { get; set; }
        public int iSupplierOrderID { get; set; }
        public int iSupplierOrderLineID { get; set; }
        public int iProductQuantity { get; set; }
    }
}
