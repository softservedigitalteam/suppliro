﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Suppliro.Entities;

namespace Suppliro.Areas.Admin.ViewModels
{
    public class SupplierOrderProductReplaceViewModel
    {
        public int iSupplierOrderID { get; set; }
        public int iProductID { get; set; }
        public string strProductCode { get; set; }
        public int iSupplierOrderLineID { get; set; }
        public int iSelectedProductCategory { get; set; }
        public List<Category> lstProductCategoryList { get; set; }
        public int iSelectedProduct { get; set; }
        public List<Product> lstProductList { get; set; }
        [Range(0, int.MaxValue, ErrorMessage = "The input should be a positive value.")]
        public int iQuantity { get; set; }
    }
}
