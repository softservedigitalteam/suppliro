﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Suppliro.Areas.Admin.ViewModels
{
    public class ProductViewModel
    {
        public int Id { get; set; }

        [Display(Name = "Product Code")]
        public string Code { get; set; }

        [Display(Name = "Display Name")]
        public string DisplayName { get; set; }

        [Display(Name = "Description")]
        public string Description { get; set; }

        [Display(Name = "Original Price")]
        public double? OriginalPrice { get; set; }

        [Display(Name = "Mark Up Percentage")]
        public double MarkUpPercentage { get; set; }

        [Display(Name = "Marked Up Price")]
        public double MarkedUpPrice { get; set; }

        [Display(Name = "Unit")]
        public int UoMId { get; set; }

        public IEnumerable<SelectListItem> UoMs { get; set; }

        [Display(Name = "Map To Unit Of Measure")]
        public int UoMDefaultId { get; set; }

        public IEnumerable<SelectListItem> DefaultUoMs { get; set; }

        public string UoMDefaultCode { get; set; }

        public double UoMDefaultValue { get; set; }

        [Display(Name = "Calculated UoM Price")]
        public string UoMDefaultPriceValue { get; set; }

        [Display(Name = "Minimum Order Unit")]
        public string MinOrderUnit { get; set; }

        [Display(Name = "Minimum Order")]
        public double MinOrder { get; set; }

        [Display(Name = "Vat")]
        public double Vat { get; set; }

        [Display(Name = "In Stock")]
        public bool InStock { get; set; }
        

        [Display(Name = "Category")]
        public int CategoryId { get; set; }

        public IEnumerable<SelectListItem> Categories { get; set; }

        [Display(Name = "Supplier")]
        public int SupplierId { get; set; }

        public IEnumerable<SelectListItem> Suppliers { get; set; }
    }
}