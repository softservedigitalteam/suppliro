﻿using Suppliro.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Suppliro.Areas.Admin.ViewModels
{
    public class CustomerInvoicesViewModelEdited
    {
        public CompanyInformation CompanyInfo { get; set; }

        public List<Order> Invoices { get; set; }
        public List<OrderLineEdited> lstOrderLineEditedAdded { get; set; }
        public List<OrderLineEdited> lstOrderLineEditedRemoved { get; set; }
        public List<OrderLineEdited> lstOrderLineEditedChanged { get; set; }
    }
}
