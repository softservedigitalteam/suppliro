﻿using Suppliro.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Suppliro.Areas.Admin.ViewModels
{
    public class UoMMapperViewModel
    {
        public UoMMapper UoMMapper { get; set; }

        public IEnumerable<SelectListItem> UoMs { get; set; }
    }
}