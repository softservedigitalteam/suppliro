﻿using Suppliro.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Suppliro.Areas.Admin.ViewModels
{
    public class ProformaListViewModel
    {
        public List<Customer> Customers { get; set; }
    }
}