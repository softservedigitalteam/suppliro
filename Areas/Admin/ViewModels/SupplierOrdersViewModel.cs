﻿using Suppliro.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Suppliro.Areas.Admin.ViewModels
{
    public class SupplierOrdersViewModel
    {
        public List<Supplier> SuppliersWithOpenOrders { get; set; }

        public List<Supplier> SupplierOrdersToday { get; set; }
    }
}