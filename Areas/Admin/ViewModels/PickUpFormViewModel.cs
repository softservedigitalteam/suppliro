﻿using Suppliro.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Suppliro.Areas.Admin.ViewModels
{
    public class PickUpFormViewModel
    {
        public PickUpForm CurrentPickUpForm { get; set; }

        public List<PickUpForm> PickUpFormHistory { get; set; }
    }
}