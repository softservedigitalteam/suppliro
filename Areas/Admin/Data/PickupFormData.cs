﻿using Suppliro.Context;
using Suppliro.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace Suppliro.Areas.Admin.Data
{
    public class PickupFormData
    {
        private DBContext _db;

        public PickupFormData(DBContext context)
        {
            _db = context;
        }

        public void Detach(PickUpForm model)
        {
            _db.Entry(model).State = EntityState.Detached;
        }

        public PickUpForm Update(PickUpForm model)
        {
            if (model.Id > 0)
            {
                _db.Entry(model).State = EntityState.Modified;
                _db.SaveChanges();
            }
            else
            {
                _db.PickUpForms.Add(model);
                _db.SaveChanges();
            }

            return model;
        }

        public PickUpForm GetOpenPickUpForm()
        {
            return _db.PickUpForms
                .Where(x => x.IsComplete == false)
                .FirstOrDefault();
        }

        public List<PickUpForm> GetClosedPickUpForms()
        {
            return _db.PickUpForms
                .Where(x => x.IsComplete == true)
                .OrderByDescending(x => x.DateCompleted)
                .ToList();
        }

        public List<PickUpForm> GetAllClosed()
        {
            return _db.PickUpForms
                .Where(x => x.IsComplete == true)
                .ToList();
        }

        public PickUpForm GetPickUpFormById(int id)
        {
            return _db.PickUpForms
                .Where(x => x.Id == id)
                .Include(x => x.PickUpFormLines)
                .FirstOrDefault();                
        }
    }
}