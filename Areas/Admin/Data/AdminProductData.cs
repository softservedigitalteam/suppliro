﻿using Suppliro.Context;
using Suppliro.Entities;
using Suppliro.Manager;
using Suppliro.Search;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace Suppliro.Areas.Admin.Data
{
    public class AdminProductData
    {
        private DBContext _db;

        public AdminProductData(DBContext context)
        {
            _db = context;
        }

        public List<Product> GetAllProducts()
        {
            List<Product> _products = new List<Product>();

            try
            {
                _products = _db.Products
                    .OrderBy(x => x.Code)
                    .Include(x => x.Category)
                    .Include(x => x.Supplier)
                    .ToList();
            } catch (Exception ex)
            { }

            return _products;
        }

        public List<Product> GetAllProductsBySupplierId(int supplierId)
        {
            List<Product> _products = new List<Product>();

            _products = _db.Products
                .Where(x => x.SupplierId == supplierId)
                .Include(x => x.Category)
                .Include(x => x.Supplier)
                .ToList();

            return _products;
        }

        public List<Product> GetAllProductsByCategoryId(int categoryId)
        {
            List<Product> _products = new List<Product>();

            _products = _db.Products
                .Where(x => x.CategoryId == categoryId)
                .Include(x => x.Category)
                .Include(x => x.Supplier)
                .ToList();

            return _products;
        }

        public Product GetProductById(int id)
        {
            return _db.Products
                .Where(x => x.Id == id)
                .Include(x => x.Category)
                .Include(x => x.Supplier)
                .FirstOrDefault();
        }

        public Product GetProductByIdForFSB(int id)
        {
            return _db.Products
                .Where(x => x.Id == id && x.InStock == true)
                .Include(x => x.Category)
                .Include(x => x.Supplier)
                .FirstOrDefault();
        }

        public void SaveProductList(List<Product> modelList)
        {
            foreach (var prod in modelList)
            {
                if (prod.Id > 0)
                {
                    _db.Entry(prod).State = System.Data.Entity.EntityState.Modified;
                }
                else
                {
                    _db.Products.Add(prod);
                }
            }

            _db.SaveChanges();
        }

        public Product SaveProduct(Product model)
        {
            if (model.Id > 0)
            {
                _db.Entry(model).State = System.Data.Entity.EntityState.Modified;
                _db.SaveChanges();
            }
            else
            {
                _db.Products.Add(model);
                _db.SaveChanges();
            }

            return GetProductById(model.Id);
        }

        public List<SearchModel> GetAllProductsForLuceneSearch()
        {
            List<SearchModel> _schMdl = new List<SearchModel>();

            var model = (from p in _db.Products.Include(x => x.Category).Include(x => x.Supplier)
                       where p.InStock == true
                       orderby p.Code, p.UoMDefaultPriceValue, p.Category, p.Supplier
                       select new SearchModel()
                       {
                           ProductId = p.Id,
                           Code = string.IsNullOrEmpty(p.DisplayName) ? p.Code : p.DisplayName,
                           SupplierCode = p.Supplier.RegisteredName,
                           CategoryCode = p.Category.Name,
                           MarkedUpPrice = p.MarkedUpPrice,
                           MinOrder = p.MinOrder,
                           Unit = p.Unit,
                           UoMValue = p.UoMValue,                           
                           UoMDefaultPriceValue = p.UoMDefaultPriceValue
                       }).ToList();

            model.ForEach(x =>
            {
                x.UoMDefaultPriceValueDecimal = 
                !string.IsNullOrWhiteSpace(x.UoMDefaultPriceValue) 
                ? Regex.Replace(x.UoMDefaultPriceValue, "[^.0-9]", "") 
                : string.Empty;
            });

            _schMdl = model.OrderBy(x => x.CategoryCode).ThenBy(x => x.UoMDefaultPriceValueDecimal).ThenBy(x => x.Code).ToList();

            return _schMdl;
        }
    }
}