﻿using Suppliro.Context;
using Suppliro.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Suppliro.Areas.Admin.Data
{
    public class SupplierData
    {
        private DBContext _db;

        public SupplierData(DBContext context)
        {
            _db = context;
        }

        public List<Supplier> GetAll()
        {
            return _db.Suppliers.ToList();
        }
        public Supplier GetSupplierById(int id)
        {
            return _db.Suppliers.Where(x => x.Id == id && x.IsActive == true).FirstOrDefault();
        }

        public IEnumerable<SelectListItem> GetSuppliersDropDown()
        {
            var suppliers = _db.Suppliers
                        .Select(x =>
                                new SelectListItem
                                {
                                    Value = x.Id.ToString(),
                                    Text = x.RegisteredName
                                });

            return new SelectList(suppliers, "Value", "Text");
        }

        public Supplier EditSupplier(Supplier model)
        {
            model.IsActive = true;
            if (model.Id > 0)
            {
                _db.Entry(model).State = System.Data.Entity.EntityState.Modified;
                _db.SaveChanges();
            }
            else
            {
                if (!string.IsNullOrEmpty(model.EmailAddress) && !string.IsNullOrEmpty(model.RegisteredName))
                {
                    _db.Suppliers.Add(model);
                    _db.SaveChanges();
                }
            }

            return model;
        }

        public List<OrderLine> GetOrderLineBySupplier(int supplierId)
        {
            List<OrderLine> _list = new List<OrderLine>();

            var query = (from l in _db.OrderLines
                         where l.IsCompleted == false && l.SupplierId == supplierId
                         group l by new { l.ProductId, l.ProductCode, l.UnitPrice } into g
                         select new
                         {
                             g.Key.ProductId,
                             g.Key.ProductCode,
                             g.Key.UnitPrice,
                             Quantity = g.Sum(x => x.Quantity)
                         }).ToList();

            foreach (var item in query) //retrieve each item and assign to list
            {
                _list.Add(new OrderLine()
                {
                    ProductId = item.ProductId,
                    ProductCode = item.ProductCode,
                    UnitPrice = item.UnitPrice,
                    Quantity = item.Quantity
                });
            }

            return _list;
        }

        public List<Supplier> GetSuppliersWithClosedOrders()
        {
            List<Supplier> _list = new List<Supplier>();

            var query = (from s in _db.Suppliers
                         join l in _db.OrderLines on s.Id equals l.SupplierId
                         where l.IsCompleted == true
                         group s by new { s.Id, s.TradingName } into grouped
                         select new
                         {
                             grouped.Key.Id,
                             grouped.Key.TradingName
                         }).ToList();

            foreach (var item in query) //retrieve each item and assign to list
            {
                _list.Add(new Supplier()
                {
                    Id = item.Id,
                    TradingName = item.TradingName
                });
            }

            return _list;
        }
    }
}