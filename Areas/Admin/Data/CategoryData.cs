﻿using Suppliro.Context;
using Suppliro.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Suppliro.Areas.Admin.Data
{
    public class CategoryData
    {
        private DBContext _db;

        public CategoryData(DBContext context)
        {
            _db = context;
        }

        public List<Category> GetAll()
        {
            return _db.Categories
                .OrderBy(x => x.Name)
                .ToList();
        }

        public Category GetCategoryByName(string name)
        {
            return _db.Categories
                .Where(x => x.Name == name)
                .FirstOrDefault();
        }

        public Category GetCategoryById(int id)
        {
            return _db.Categories
                .Where(x => x.Id == id)
                .FirstOrDefault();
        }

        public IEnumerable<SelectListItem> GetCategoryDropDown()
        {
            var suppliers = _db.Categories
                        .Select(x =>
                                new SelectListItem
                                {
                                    Value = x.Id.ToString(),
                                    Text = x.Name
                                });

            return new SelectList(suppliers, "Value", "Text");
        }

        public Category UpdateCategory(Category model)
        {
            if (model.Id > 0)
            {
                _db.Entry(model).State = System.Data.Entity.EntityState.Modified;
                _db.SaveChanges();
            }
            else
            {
                _db.Categories.Add(model);
                _db.SaveChanges();
            }

            return model;
        }
    }
}