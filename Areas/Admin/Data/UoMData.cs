﻿using Suppliro.Context;
using Suppliro.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Web.Mvc;

namespace Suppliro.Areas.Admin.Data
{
    public class UoMData
    {
        private DBContext _db;

        public UoMData(DBContext context)
        {
            _db = context;
        }

        #region Unit of Measure
        public UoM UpdateUoM(UoM model)
        {
            if (!string.IsNullOrWhiteSpace(model.Code))
            {
                if (model.Id > 0)
                {
                    _db.Entry(model).State = System.Data.Entity.EntityState.Modified;
                    _db.SaveChanges();
                }
                else
                {
                    _db.UoMs.Add(model);
                    _db.SaveChanges();
                }
            }

            return model;
        }

        public IEnumerable<SelectListItem> GetUoMDropDown()
        {
            var UoMs = _db.UoMs
                        .Select(x =>
                                new SelectListItem
                                {
                                    Value = x.Id.ToString(),
                                    Text = x.Code
                                });

            return new SelectList(UoMs, "Value", "Text");
        }

        public List<UoM> GetUoMAll()
        {
            return _db.UoMs
                .Include(x => x.UoMMappers)
                .ToList();
        }

        public UoM GetUoMById(int id)
        {
            return _db.UoMs
                .Where(x => x.Id == id)
                .Include(x => x.UoMMappers)
                .FirstOrDefault();
        }
        #endregion

        #region Unit of Measure Mapper
        public UoMMapper UpdateUoMMapper(UoMMapper model)
        {
            if (model.Id > 0)
            {
                _db.Entry(model).State = System.Data.Entity.EntityState.Modified;
                _db.SaveChanges();
            }
            else
            {
                _db.UoMMappers.Add(model);
                _db.SaveChanges();
            }

            return model;
        }

        public List<UoMMapper> GetUoMMapperAll(int uomId)
        {
            return _db.UoMMappers
                .Where(x => x.UoMId == uomId)
                .Include(x => x.UoM)
                .ToList();
        }

        public UoMMapper GetUoMMapperById(int id)
        {
            return _db.UoMMappers
                .Where(x => x.Id == id)
                .Include(x => x.UoM)
                .FirstOrDefault();
        }
        #endregion
    }
}