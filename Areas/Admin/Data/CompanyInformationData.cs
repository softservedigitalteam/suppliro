﻿using Suppliro.Context;
using Suppliro.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Suppliro.Areas.Admin.Data
{
    public class CompanyInformationData
    {
        private DBContext _db;

        public CompanyInformationData(DBContext context)
        {
            _db = context;
        }

        public CompanyInformation GetInformation()
        {
            return _db.CompanyInformations.FirstOrDefault();
        }

        public CompanyInformation Save(CompanyInformation model)
        {
            if (model.Id > 0)
            {
                _db.Entry(model).State = System.Data.Entity.EntityState.Modified;
                _db.SaveChanges();
            }
            else
            {
                _db.CompanyInformations.Add(model);
                _db.SaveChanges();
            }

            return model;
        }
    }
}