﻿using Suppliro.Context;
using Suppliro.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Suppliro.Areas.Admin.Data
{
    public class SupplierOrderData
    {
        private DBContext _db;

        public SupplierOrderData(DBContext context)
        {
            _db = context;
        }

        public string GenerateOrderNumber()
        {
            string _orderNumber = "";

            //There should only be one order number from which we track
            SupplierOrderNumber _num = _db.SupplierOrderNumbers.FirstOrDefault();

            if (_num == null)
            {
                _num = new SupplierOrderNumber();
                _num.Number = 1;

                _db.SupplierOrderNumbers.Add(_num);
                _db.SaveChanges();
            }
            else
            {
                _num.Number += 1;

                _db.Entry(_num).State = EntityState.Modified;
                _db.SaveChanges();
            }

            //Incremented the order number....Now to return the general order number that will be displayed
            _orderNumber = _num.Number.ToString();

            return _orderNumber;
        }

        public List<Supplier> GetSuppliersWithOpenOrders()
        {
            List<Supplier> _list = new List<Supplier>();

            var query = (from s in _db.Suppliers
                         join l in _db.SupplierOrders on s.Id equals l.SupplierId
                         where l.IsEmailed == false && l.IsComplete == false
                         select s).ToList();

            query.ForEach(item =>
            {
                _list.Add(item);
            });

            return _list;
        }

        public List<Supplier> GetSupplierOrdersToday()
        {
            List<Supplier> _list = new List<Supplier>();

            var endDate = DateTime.Today.AddDays(1).AddTicks(-1);

            var query = (from s in _db.Suppliers
                         join l in _db.SupplierOrders on s.Id equals l.SupplierId
                         where l.IsEmailed == true && l.IsComplete == true
                         && l.DateCompleted >= DateTime.Today && l.DateCompleted <= endDate
                         select s).ToList();

            query.ForEach(item =>
            {
                _list.Add(item);
            });

            return _list;
        }

        public SupplierOrder GetSupplierOrderById(int id)
        {
            return _db.SupplierOrders
            .Where(x => x.Id == id)
            .Include(y => y.SupplierOrderLines.Select(x => x.Customer))
            .Include(z => z.Supplier)
            .FirstOrDefault();
        }

        public SupplierOrderLine GetSupplierOrderLineById(int id)
        {
            return _db.SupplierOrderLines
                .FirstOrDefault(x => x.Id == id);
        }
        public SupplierOrderLineEdited GetSupplierOrderLineEditedBySupplierOrderLineId(int SupplierOrderLineId)
        {
            return _db.SupplierOrderLinesEdited
                .FirstOrDefault(x => x.SupplierOrderLineId == SupplierOrderLineId);
        }

        public void SaveOrderLine(SupplierOrderLine model)
        {
            if (model.Id > 0)
            {
                _db.Entry(model).State = EntityState.Modified;
                _db.SaveChanges();
            }
            else
            {
                _db.SupplierOrderLines.Add(model);
                _db.SaveChanges();
            }
        }
        public void SaveOrderLineEdited(SupplierOrderLineEdited model)
        {
            if (model.Id > 0)
            {
                _db.Entry(model).State = EntityState.Modified;
                _db.SaveChanges();
            }
            else
            {
                _db.SupplierOrderLinesEdited.Add(model);
                _db.SaveChanges();
            }
        }

        public void Save(SupplierOrder model)
        {
            if (model.Id > 0)
            {
                _db.Entry(model).State = EntityState.Modified;
                _db.SaveChanges();
            }
            else
            {
                _db.SupplierOrders.Add(model);
                _db.SaveChanges();
            }
        }

        public SupplierOrder GetSupplierOpenOrderBySupplierId(int supplierId)
        {
            return _db.SupplierOrders
                .Where(x => x.SupplierId == supplierId && x.IsComplete == false && x.IsEmailed == false)
                .Include(w => w.SupplierOrderLines.Select(x => x.Customer))
                .Include(y => y.SupplierOrderLinesEdited.Select(x => x.Customer))
                .Include(z => z.Supplier)
                .FirstOrDefault();
        }

        public SupplierOrder GetSupplierOpenOrderEditedBySupplierId(int supplierId)
        {
            return _db.SupplierOrders
                .Where(x => x.SupplierId == supplierId && x.IsComplete == false && x.IsEmailed == false)
                .Include(y => y.SupplierOrderLinesEdited.Select(x => x.Customer))
                .Include(z => z.Supplier)
                .FirstOrDefault();
        }

        public SupplierOrder GetSupplierOpenOrderByOrderLineId(int orderLineId)
        {
            return _db.SupplierOrders
                .Where(x => x.SupplierOrderLines.Any(ol => ol.OrderLineId == orderLineId))
                .Include(y => y.SupplierOrderLines.Select(x => x.Customer))
                .Include(z => z.Supplier)
                .FirstOrDefault();
        }

        public SupplierOrder GetSupplierOpenOrderById(int id)
        {
            return _db.SupplierOrders
                .Where(x => x.Id == id && x.IsEmailed == false && x.IsComplete == false)
                .Include(y => y.SupplierOrderLines.Select(x => x.Customer))
                .Include(z => z.Supplier)
                .FirstOrDefault();
        }

        public List<SupplierOrder> GetOpenOrdersNotMailed()
        {
            return _db.SupplierOrders
                .Where(x => x.IsEmailed == false && x.IsComplete == false)
                .Include(y => y.SupplierOrderLines)
                .ToList();
        }

        public List<SupplierOrder> GetOpenOrdersWithoutLines()
        {
            return _db.SupplierOrders
                .Where(x => x.IsComplete == false)
                .Include(x => x.Supplier)
                .ToList();
        }

        public List<SupplierOrder> GetOpenOrders()
        {
            return _db.SupplierOrders
                .Where(x => x.IsComplete == false).
                Include(y => y.SupplierOrderLines)
                .ToList();
        }

        public List<SupplierOrder> GetAll()
        {
            return _db.SupplierOrders
                .OrderByDescending(x => x.DateCreated)
                .Include(y => y.Supplier)
                .ToList();
        }

        public List<SupplierOrder> GetSupplierOrdersPlacedToday()
        {
            var endDate = DateTime.Today.AddDays(1).AddTicks(-1);

            return _db.SupplierOrders
                .Where(x => x.DateCompleted >= DateTime.Today && x.DateCompleted <= endDate)
                .OrderByDescending(x => x.DateCreated)
                .Include(y => y.Supplier)
                .ToList();
        }

        public List<SupplierOrder> GetSupplierOrdersExceptToday()
        {
            var endDate = DateTime.Today;

            return _db.SupplierOrders
                .Where(x => x.DateCompleted < endDate)
                .OrderByDescending(x => x.DateCreated)
                .Include(y => y.Supplier)
                .ToList();
        }

        public void RemoveSupplierOrder(SupplierOrder model)
        {
            _db.SupplierOrders.Remove(model);
            _db.SaveChanges();
        }

        public void RemoveLine(SupplierOrderLine model)
        {
            _db.SupplierOrderLines.Remove(model);
            _db.SaveChanges();
        }

        public void RemoveLineEdited(SupplierOrderLineEdited model)
        {
            _db.SupplierOrderLinesEdited.Remove(model);
            _db.SaveChanges();
        }
    }
}