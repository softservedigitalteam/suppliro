﻿using Suppliro.Areas.Admin.Data;
using Suppliro.Areas.Admin.ViewModels;
using Suppliro.Context;
using Suppliro.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Suppliro.Areas.Admin.Manager
{
    public class UoMManager
    {
        private DBContext _db;
        private UoMData data;

        public UoMManager(DBContext context)
        {
            _db = context;
            data = new UoMData(_db);
        }

        public List<UoM> GetUoMAll()
        {
            return data.GetUoMAll();
        }

        public UoM GetUoMById(int id)
        {
            return data.GetUoMById(id);
        }

        public UoMViewModel GetUoMViewModelById(int? id)
        {
            var uom = new UoM();

            if (id.HasValue)
                uom = data.GetUoMById(id.Value);

            UoMViewModel vm = new UoMViewModel
            {
                UoM = uom
            };

            if (id.HasValue)
            {
                var UoMs = this.GetUUoMMapperAll(id.Value)
                              .Select(x =>
                                      new SelectListItem
                                      {
                                          Value = x.Id.ToString(),
                                          Text = x.CompareToUoMCode
                                      });
                vm.UoMs = UoMs;
            }
            else
            {
                vm.UoMs = null;
            }

            return vm;
        }

        public void SaveUoM(UoM model)
        {
            data.UpdateUoM(model);
        }

        public UoMMapperViewModel GetUoMMapperVM(int uomId, int? id)
        {
            UoMMapperViewModel vm = new UoMMapperViewModel();

            if (id.HasValue)
            {
                vm.UoMMapper = data.GetUoMMapperById(id.Value);
            }
            else
            {
                vm.UoMMapper = new UoMMapper
                {
                    UoMId = uomId,
                    UoM = data.GetUoMById(uomId)
                };
            }

            var UoMs = this.GetUoMAll()
                           .Where(x => x.Id != uomId)
                           .Select(x =>
                                   new SelectListItem
                                   {
                                       Value = x.Id.ToString(),
                                       Text = x.Code
                                   });
            vm.UoMs = UoMs;

            return vm;
        }

        public List<UoMMapper> GetUUoMMapperAll(int uomId)
        {
            return data.GetUoMMapperAll(uomId);
        }

        public void SaveUoMMapper(UoMMapper model)
        {
            data.UpdateUoMMapper(model);
        }
    }
}