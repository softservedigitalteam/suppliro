using Postal;
using Suppliro.Areas.Admin.Data;
using Suppliro.Areas.Admin.Models;
using Suppliro.Areas.Admin.ViewModels;
using Suppliro.Context;
using Suppliro.Data;
using Suppliro.Entities;
using Suppliro.Manager;
using Suppliro.Models.Emails;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Configuration;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;

namespace Suppliro.Areas.Admin.Manager
{
    public class SupplierOrderManager
    {
        private DBContext _db;
        private SupplierOrderData data;

        public SupplierOrderManager(DBContext context)
        {
            _db = context;
            data = new SupplierOrderData(_db);
        }

        #region Supplier Order View Model
        public SupplierOrdersViewModel GetSupplierOrderViewModel()
        {
            return new SupplierOrdersViewModel
            {
                SuppliersWithOpenOrders = data.GetSuppliersWithOpenOrders(),
                SupplierOrdersToday = data.GetSupplierOrdersToday()
            };

        }
        #endregion

        #region Build Supplier Order
        public void BuildSupplierOrder(int orderId)
        {
            //Data
            OrderData orderData = new OrderData(_db);

            //Managers
            SupplierManager splrMgr = new SupplierManager(_db);
            PickupFormManager pfMgr = new PickupFormManager(_db);
            ProductManager prodMgr = new ProductManager(_db);

            //Models
            Order order = orderData.GetOpenOrderById(orderId);
            List<Supplier> suppliers = new List<Supplier>();

            //Variables
            Supplier supplier = new Supplier();

            //There is a open order so we can't create the supplier order
            if (order != null && order.Id > 0)
            {
                //Get distinct suppliers from order
                List<int> supplierIds = order.OrderLines.Select(x => x.SupplierId).Distinct().ToList();
                var pickUpForm = pfMgr.GetOpenPickUpForm();

                if (pickUpForm == null)
                {
                    pickUpForm = new PickUpForm
                    {
                        IsComplete = false,
                        IsEmailed = false,
                        DateCompleted = DateTime.Now,
                        DateCreated = DateTime.Now,
                        DateMailed = DateTime.Now
                    };

                    pfMgr.Update(pickUpForm);
                }

                //Perform actions on each distinct supplier
                foreach (int supplierId in supplierIds)
                {
                    //find an open order or create one
                    SupplierOrder splrOrder = data.GetSupplierOpenOrderBySupplierId(supplierId) ?? new SupplierOrder();
                    supplier = splrMgr.GetSupplier(supplierId);

                    //No open order so we have to create one
                    if (splrOrder.Id <= 0)
                    {
                        splrOrder.SupplierId = supplier.Id;
                        splrOrder.OrderNumber = data.GenerateOrderNumber();
                        splrOrder.SupplierEmails = supplier.EmailAddress;
                        splrOrder.DateCreated = DateTime.Now;

                        data.Save(splrOrder);
                    }

                    if (splrOrder.SupplierOrderLines == null)
                        splrOrder.SupplierOrderLines = new List<SupplierOrderLine>();

                    if (pickUpForm.PickUpFormLines == null)
                        pickUpForm.PickUpFormLines = new List<PickUpFormLine>();

                    Product prod = new Product();

                    //Append the order lines
                    foreach (var orderLine in order.OrderLines.Where(x => x.SupplierId == supplierId))
                    {
                        prod = prodMgr.GetProductById(orderLine.ProductId);

                        SupplierOrderLine splrOrderLine = new SupplierOrderLine
                        {
                            SupplierOrderId = splrOrder.Id,
                            OrderId = order.Id,
                            OrderLineId = orderLine.Id,
                            OrderNumber = order.OrderNumber,
                            CustomerId = order.CustomerId,
                            CustomerName = order.CustomerName,
                            ProductId = orderLine.ProductId,
                            ProductCode = orderLine.ProductCode,
                            ProductUnit = orderLine.Unit,
                            ProductPrice = orderLine.Product.OriginalPrice.Value,
                            Quantity = orderLine.Quantity
                        };

                        splrOrder.SupplierOrderLines.Add(splrOrderLine);

                        var prodCode = string.IsNullOrEmpty(prod.DisplayName) ? prod.Code : string.Format("({0} - {1})", prod.Code, prod.DisplayName);

                        PickUpFormLine pfLine = new PickUpFormLine
                        {
                            OrderId = orderLine.OrderId,
                            OrderLineId = orderLine.Id,
                            CustomerCode = orderLine.Order.CustomerName,
                            CustomerId = orderLine.Order.CustomerId,
                            PickUpFormId = pickUpForm.Id,
                            ProductCode = prodCode,
                            ProductId = orderLine.ProductId,
                            Qty = orderLine.Quantity,
                            SupplierId = orderLine.SupplierId,
                            SupplierTradingName = orderLine.SupplierTradingName,
                            ProductUnit = orderLine.Unit
                        };

                        pickUpForm.PickUpFormLines.Add(pfLine);
                    }

                    data.Save(splrOrder);

                    //Update total
                    this.CalculateSupplierOrderTotals(splrOrder.Id);
                }

                pfMgr.Update(pickUpForm);
            }
        }
        public void BuildSupplierOrderEdit(int orderId, OrderLine orderLine)
        {
            //Data
            OrderData orderData = new OrderData(_db);

            //Managers
            SupplierManager splrMgr = new SupplierManager(_db);
            PickupFormManager pfMgr = new PickupFormManager(_db);
            ProductManager prodMgr = new ProductManager(_db);

            //Models
            Order order = orderData.GetOpenOrderById(orderId);
            List<Supplier> suppliers = new List<Supplier>();

            //Variables
            Supplier supplier = new Supplier();

            //There is a open order so we can't create the supplier order
            if (order != null && order.Id > 0)
            {
                //Get order line supplier
                var orderLineSupplierID = orderLine.SupplierId;

                var pickUpForm = pfMgr.GetOpenPickUpForm();

                if (pickUpForm == null)
                {
                    pickUpForm = new PickUpForm
                    {
                        IsComplete = false,
                        IsEmailed = false,
                        DateCompleted = DateTime.Now,
                        DateCreated = DateTime.Now,
                        DateMailed = DateTime.Now
                    };

                    pfMgr.Update(pickUpForm);
                }

                //Perform actions on supplier
                if (orderLineSupplierID > 0)
                {
                    //find an open order or create one
                    SupplierOrder splrOrder = data.GetSupplierOpenOrderBySupplierId(orderLineSupplierID) ?? new SupplierOrder();
                    //Edited
                    splrOrder.Edited = true;

                    supplier = splrMgr.GetSupplier(orderLineSupplierID);

                    //No open order so we have to create one
                    if (splrOrder.Id <= 0)
                    {
                        splrOrder.SupplierId = supplier.Id;
                        splrOrder.OrderNumber = data.GenerateOrderNumber();
                        splrOrder.SupplierEmails = supplier.EmailAddress;
                        splrOrder.DateCreated = DateTime.Now;

                        data.Save(splrOrder);
                    }

                    if (splrOrder.SupplierOrderLines == null)
                        splrOrder.SupplierOrderLines = new List<SupplierOrderLine>();
                    if (splrOrder.SupplierOrderLinesEdited == null)
                        splrOrder.SupplierOrderLinesEdited = new List<SupplierOrderLineEdited>();

                    if (pickUpForm.PickUpFormLines == null)
                        pickUpForm.PickUpFormLines = new List<PickUpFormLine>();

                    Product prod = new Product();

                    //Append the order line
                    if (orderLine != null)
                    {
                        prod = prodMgr.GetProductById(orderLine.ProductId);
                        orderLine.Product = prod;

                        SupplierOrderLine splrOrderLine = new SupplierOrderLine
                        {
                            SupplierOrderId = splrOrder.Id,
                            OrderId = order.Id,
                            OrderLineId = orderLine.Id,
                            OrderNumber = order.OrderNumber,
                            CustomerId = order.CustomerId,
                            CustomerName = order.CustomerName,
                            ProductId = orderLine.ProductId,
                            ProductCode = orderLine.ProductCode,
                            ProductUnit = orderLine.Unit,
                            ProductPrice = orderLine.Product.OriginalPrice.Value,
                            Quantity = orderLine.Quantity
                        };

                        splrOrder.SupplierOrderLines.Add(splrOrderLine);

                        data.Save(splrOrder);

                        //Add supplier order line edited (for edited reference)
                        SupplierOrderLineEdited splrOrderLineEdited = new SupplierOrderLineEdited
                        {
                            SupplierOrderLineId = splrOrderLine.Id,
                            SupplierOrderId = splrOrder.Id,
                            OrderId = order.Id,
                            OrderLineId = orderLine.Id,
                            OrderNumber = order.OrderNumber,
                            CustomerId = order.CustomerId,
                            CustomerName = order.CustomerName,
                            ProductId = orderLine.ProductId,
                            ProductCode = orderLine.ProductCode,
                            ProductUnit = orderLine.Unit,
                            ProductPrice = orderLine.Product.OriginalPrice.Value,
                            Quantity = orderLine.Quantity
                        };

                        splrOrder.SupplierOrderLinesEdited.Add(splrOrderLineEdited);

                        var prodCode = string.IsNullOrEmpty(prod.DisplayName) ? prod.Code : string.Format("({0} - {1})", prod.Code, prod.DisplayName);

                        PickUpFormLine pfLine = new PickUpFormLine
                        {
                            OrderId = orderLine.OrderId,
                            OrderLineId = orderLine.Id,
                            CustomerCode = orderLine.Order.CustomerName,
                            CustomerId = orderLine.Order.CustomerId,
                            PickUpFormId = pickUpForm.Id,
                            ProductCode = prodCode,
                            ProductId = orderLine.ProductId,
                            Qty = orderLine.Quantity,
                            SupplierId = orderLine.SupplierId,
                            SupplierTradingName = orderLine.SupplierTradingName,
                            ProductUnit = orderLine.Unit
                        };

                        pickUpForm.PickUpFormLines.Add(pfLine);
                    }

                    data.Save(splrOrder);

                    //Update total
                    this.CalculateSupplierOrderTotals(splrOrder.Id);
                }

                pfMgr.Update(pickUpForm);
            }
        }
        #endregion

        public void CalculateSupplierOrderTotals(int id)
        {
            ProductData _prodData = new ProductData();
            double orderValue = 0;
            double orderMarkedUpValue = 0;

            //Get Supplier order
            SupplierOrder order = data.GetSupplierOrderById(id);

            if (order != null)
            {
                //set values to null
                order.TotalValue = 0;
                order.TotalMarkedupValue = 0;

                if (order.SupplierOrderLines != null)
                {
                    order.SupplierOrderLines.ForEach(x =>
                    {
                        if (!x.IsReplaced)
                        {
                            var product = _prodData.GetProductById(x.ProductId);

                        //Update price
                        orderValue += Math.Round(x.Quantity * product.OriginalPrice.Value, 2);
                            orderMarkedUpValue += Math.Round(x.Quantity * product.MarkedUpPrice, 2);

                        //Update UoM
                        var uom = product.Unit;
                            if (product.UoMValue > 0)
                            {
                                uom = string.Format("{0} x {1} ({2})", product.MinOrder, product.UoMValue, product.Unit);
                            }
                            else
                            {
                                uom = string.Format("{0} ({1})", product.MinOrder, product.Unit);
                            }

                            x.ProductUnit = uom;
                        }
                    });
                }

                order.TotalValue = orderValue;
                order.TotalMarkedupValue = orderMarkedUpValue;

                data.Save(order);
            }
        }

        #region Process Orders
        public Task ProcessOrders(int? supplierOrderId)
        {
            //Data
            SupplierData supplierData = new SupplierData(_db);
            ProductManager prodMgr = new ProductManager(_db);

            List<SupplierOrder> supplierOrders = new List<SupplierOrder>();

            //This method can process all open supplier orders or just open order by supplier
            if (supplierOrderId.HasValue)
                supplierOrders.Add(data.GetSupplierOrderById(supplierOrderId.Value));

            if (supplierOrders != null)
            {
                //foreach order mail the order out
                foreach (var supplierOrder in supplierOrders)
                {
                    if (supplierOrder != null)
                    {
                        try
                        {
                            Supplier supplier = supplierData.GetSupplierById(supplierOrder.SupplierId);
                            List<SupplierEmailOrderLine> emailLines = new List<SupplierEmailOrderLine>();

                            string prodCode = string.Empty;
                            var prod = new Product();

                            //group together unique products to mail out
                            foreach (var line in supplierOrder.SupplierOrderLines)
                            {
                                prod = prodMgr.GetProductById(line.ProductId);
                                prodCode = string.IsNullOrEmpty(prod.DisplayName) ? prod.Code : prod.DisplayName;

                                var orderEmailline = new SupplierEmailOrderLine
                                {
                                    Code = prodCode,
                                    Qty = line.Quantity,
                                    ProductId = line.ProductId,
                                    Unit = line.ProductUnit
                                };

                                emailLines.Add(orderEmailline);
                            }

                            try
                            {
                                //Send Email
                                SupplierOrderEmail theEmail = new SupplierOrderEmail();
                                theEmail.ToAddresses = supplier.EmailAddress;
                                theEmail.Subject = string.Format("Suppliro Order {0} for COLLECTION", DateTime.Now.ToString("dd/MM/yy"));
                                theEmail.Supplier = supplier;
                                theEmail.EmailLines = emailLines.OrderByDescending(x => x.Code).ToList();

                                //From address, subject and main message gets set in the email views

                                // ********** COMMENT OUT EMAIL FOR TESTING **********
                                //EmailManager.SendEmail(Enum.Enums.EmailType.SupplierOrderEmail, Enum.Enums.EmailSection.smtpAdmin, theEmail);
                                // ********** COMMENT OUT EMAIL FOR TESTING  **********

                                //Update the order as mailed
                                supplierOrder.IsEmailed = true;
                                supplierOrder.DateMailed = DateTime.Now;
                            }
                            catch (Exception e)
                            {
                                //The mail couldn't be sent for this supplier so we need to know 
                                //Update the order as mailed
                                supplierOrder.IsEmailed = false;
                            }

                            data.Save(supplierOrder);

                            InvoiceManager invoiceManager = new InvoiceManager(_db);
                            OrderManager orderManager = new OrderManager(_db);
                            foreach (var custId in supplierOrder.SupplierOrderLines.Select(x => x.CustomerId).Distinct())
                            {
                                //Append the lines to each customers proforma invoice
                                invoiceManager.BuildInvoice(custId, supplier.Id, supplierOrder.SupplierOrderLines.Where(x => x.CustomerId == custId).ToList());

                                //Update the customers orderlines as emailed
                                orderManager.UpdateOrderLines(custId, supplierOrder.SupplierOrderLines.Where(x => x.CustomerId == custId).ToList());
                            }

                            orderManager.UpdateOrderPercentages(null);
                        }
                        catch (Exception e)
                        { }
                    }
                }
            }

            return Task.FromResult(0);
        }
        #endregion

        public SupplierOrder GetSupplierOrderBySupplierId(int supplierId)
        {
            return data.GetSupplierOpenOrderBySupplierId(supplierId);
        }
        public SupplierOrder GetSupplierOrderEditedBySupplierId(int supplierId)
        {
            return data.GetSupplierOpenOrderEditedBySupplierId(supplierId);
        }

        public SupplierOrder GetSupplierOrderById(int id)
        {
            return data.GetSupplierOrderById(id);
        }

        public List<SupplierOrder> GetOpenOrdersWithoutLines()
        {
            return data.GetOpenOrdersWithoutLines();
        }

        public List<SupplierOrder> GetSupplierOrdersPlacedToday()
        {
            return data.GetSupplierOrdersPlacedToday();
        }

        public List<SupplierOrder> GetSupplierOrdersExceptToday()
        {
            return data.GetSupplierOrdersExceptToday();
        }

        public List<SupplierOrder> GetAll()
        {
            return data.GetAll();
        }

        public void MarkComplete(int id)
        {
            OrderManager ordMgr = new OrderManager(_db);

            var order = data.GetSupplierOrderById(id);

            order.IsComplete = true;
            order.DateCompleted = DateTime.Now;

            data.Save(order);

            InvoiceManager invoiceManager = new InvoiceManager(_db);
            foreach (var custId in order.SupplierOrderLines.Select(x => x.CustomerId).Distinct())
            {
                //Append the lines to each customers proforma invoice
                invoiceManager.BuildInvoice(custId, order.SupplierId, order.SupplierOrderLines.Where(x => x.CustomerId == custId).ToList());

                //Update the customers orderlines as complete
                ordMgr.UpdateOrderLines(custId, order.SupplierOrderLines.Where(x => x.CustomerId == custId).ToList());
            }

            ordMgr.UpdateOrderPercentages(null);
        }

        private void RemoveSupplierOrderLine(SupplierOrderLine model)
        {
            if (model == null)
                return;

            PickupFormManager pfMgr = new PickupFormManager(_db);

            var order = data.GetSupplierOrderById(model.SupplierOrderId);

            //Delete line
            data.RemoveLine(model);

            if (order != null)
            {
                //Doesnt have anymore lines there for we delete it
                if (order.SupplierOrderLines != null && order.SupplierOrderLines.Count == 0)
                {
                    data.RemoveSupplierOrder(order);
                }
            }
        }

        #region Replace product
        public int ReplaceProduct(int orderLineId, int oldProductId, int newProductId, int? qty)
        {
            //Data          
            OrderManager ordMgr = new OrderManager(_db);
            ProductManager prodMgr = new ProductManager(_db);
            InvoiceManager invMgr = new InvoiceManager(_db);
            CompanyInformationData compData = new CompanyInformationData(_db);

            var supplierOrderLine = data.GetSupplierOrderLineById(orderLineId);
            var existingSupplierOrder = data.GetSupplierOrderById(supplierOrderLine.SupplierOrderId);

            //Get Product
            var product = prodMgr.GetProductById(newProductId);
            product.Code = string.IsNullOrEmpty(product.DisplayName) ? product.Code : product.DisplayName;

            var compInfo = compData.GetInformation();

            //////////////////////////////////////////////////////////////////////////////////////////
            ////////////////    UOM SECTION
            //////////////////////////////////////////////////////////////////////////////////////////        

            var uom = product.Unit;
            if (product.UoMValue > 0)
            {
                uom = string.Format("{0} x {1} ({2})", product.MinOrder, product.UoMValue, product.Unit);
            }
            else
            {
                uom = string.Format("{0} ({1})", product.MinOrder, product.Unit);
            }

            //////////////////////////////////////////////////////////////////////////////////////////
            ////////////////    ORDER AND ORDER LINE
            //////////////////////////////////////////////////////////////////////////////////////////

            var orderLine = ordMgr.GetOrderLineById(supplierOrderLine.OrderLineId);

            var quantity = qty ?? orderLine.Quantity;
            int orderId = orderLine.OrderId;
            int poLineId = orderLine.Id;

            //Add new orderline to existing order
            OrderLine newOrderLine = new OrderLine
            {
                ProductId = product.Id,
                OrderId = orderLine.OrderId,
                ProductCode = product.Code,
                SupplierId = existingSupplierOrder.SupplierId,
                SupplierTradingName = existingSupplierOrder.Supplier.TradingName,
                UnitPrice = product.MarkedUpPrice,
                IsCompleted = true,
                IsOrdered = true,
                Unit = uom,
                Quantity = quantity,
                Amount = Math.Round((product.MarkedUpPrice * quantity), 2),
                VATAmount = Math.Round(((product.MarkedUpPrice * quantity)) * 0.14, 2)
            };

            //Add new line
            ordMgr.SaveOrderLine(newOrderLine);

            //Update percentages with newly added product
            ordMgr.UpdateOrderPercentages(orderId);

            //////////////////////////////////////////////////////////////////////////////////////////
            ////////////////    INVOICE SECTION
            //////////////////////////////////////////////////////////////////////////////////////////

            //Mark Order Line as replaced
            supplierOrderLine.IsReplaced = true;
            supplierOrderLine.DateReplaced = DateTime.Now;
            supplierOrderLine.ReplacedProductId = product.Id;
            supplierOrderLine.ReplacedProductCode = product.Code;
            data.SaveOrderLine(supplierOrderLine);

            //Update totals
            CalculateSupplierOrderTotals(supplierOrderLine.SupplierOrderId);

            //Remove from proforma
            var invoiceLine = invMgr.GetInvoiceLineByOrderLineId(orderId, poLineId);
            if (invoiceLine != null)
            {
                var invoiceId = invoiceLine.InvoiceId;

                //Mark Invoice Line as Replaced
                invoiceLine.IsReplaced = true;
                invoiceLine.DateReplaced = DateTime.Now;
                invoiceLine.ReplacedProductId = product.Id;
                invoiceLine.ReplacedProductCode = product.Code;
                invMgr.SaveLine(invoiceLine);

                //Get Invoice
                var invoice = invMgr.GetInvoiceById(invoiceId);

                //Add new line
                var subTotal = Math.Round(quantity * product.MarkedUpPrice, 2);
                var vatAmount = Math.Round((subTotal / 100) * compInfo.VATPercentage, 2);
                var total = Math.Round(subTotal + vatAmount, 2);

                //Create new line
                InvoiceLine newLine = new InvoiceLine
                {
                    DateAdded = DateTime.Now,
                    InvoiceId = invoice.Id,
                    OrderId = newOrderLine.OrderId,
                    OrderLineId = newOrderLine.Id,
                    OrderNumber = newOrderLine.Order.OrderNumber,
                    ProductId = newOrderLine.ProductId,
                    ProductCode = newOrderLine.ProductCode,
                    ProductUnit = product.Unit,
                    ProductUnitPrice = product.MarkedUpPrice,
                    Quantity = newOrderLine.Quantity,
                    SubTotal = subTotal,
                    VATAmount = vatAmount,
                    TotalAmount = total
                };

                //Save invoice line
                invoice.InvoiceLines.Add(newLine);

                //Update proforma values
                invMgr.UpdateOrderTotals(invoiceId);
            }

            //Remove From Order Line
            orderLine.IsReplaced = true;
            orderLine.DateReplaced = DateTime.Now;
            orderLine.ReplacedProductId = product.Id;
            orderLine.ReplacedProductCode = product.Code;
            ordMgr.SaveOrderLine(orderLine);

            //Update order values
            ordMgr.UpdateOrderTotals(newOrderLine.OrderId);

            return existingSupplierOrder.Id;
        }
        #endregion

        public int RemoveSupplierOrderLine(int id)
        {
            var line = data.GetSupplierOrderLineById(id);

            OrderManager ordMgr = new OrderManager(_db);
            InvoiceManager invMgr = new InvoiceManager(_db);

            var orderLine = ordMgr.GetOrderLineById(line.OrderLineId);
            if (orderLine != null)
            {
                //Update order values
                ordMgr.UpdateOrderTotals(orderLine.OrderId);

                //Update percentages with newly added product
                ordMgr.UpdateOrderPercentages(orderLine.OrderId);

                //Remove from proforma
                var invoiceLine = invMgr.GetInvoiceLineByOrderLineId(orderLine.OrderId, orderLine.Id);
                if (invoiceLine != null)
                {
                    invMgr.DeleteLine(invoiceLine);

                    //Update proforma values
                    invMgr.UpdateOrderTotals(invoiceLine.InvoiceId);
                }

                //Remove From Order
                ordMgr.DeleteOrderLine(orderLine);
            }

            data.RemoveLine(line);

            return line.SupplierOrderId;
        }
        public void CheckSupplierOrderLineEdited(int SupplierOrderLineId)
        {
            //Get Supplier Order line and suplier order line edited
            var supplierOrderLine = data.GetSupplierOrderLineById(SupplierOrderLineId);
            var supplierOrderLineEdited = data.GetSupplierOrderLineEditedBySupplierOrderLineId(SupplierOrderLineId);

            if (supplierOrderLineEdited == null && supplierOrderLine != null)
            {
                SupplierOrderLineEdited SupplierOrderLineEdited = new SupplierOrderLineEdited();

                SupplierOrderLineEdited.SupplierOrderLineId = supplierOrderLine.Id;
                SupplierOrderLineEdited.SupplierOrderId = supplierOrderLine.SupplierOrderId;
                SupplierOrderLineEdited.OrderId = supplierOrderLine.OrderId;
                SupplierOrderLineEdited.OrderLineId = supplierOrderLine.OrderLineId;
                SupplierOrderLineEdited.OrderNumber = supplierOrderLine.OrderNumber;
                SupplierOrderLineEdited.CustomerId = supplierOrderLine.CustomerId;
                SupplierOrderLineEdited.CustomerName = supplierOrderLine.CustomerName;
                SupplierOrderLineEdited.CustomerColor = supplierOrderLine.CustomerColor;
                SupplierOrderLineEdited.ProductId = supplierOrderLine.ProductId;
                SupplierOrderLineEdited.ProductCode = supplierOrderLine.ProductCode;
                SupplierOrderLineEdited.ProductPrice = supplierOrderLine.ProductPrice;
                SupplierOrderLineEdited.ProductUnit = supplierOrderLine.ProductUnit;
                SupplierOrderLineEdited.Quantity = supplierOrderLine.Quantity;
                SupplierOrderLineEdited.IsReplaced = supplierOrderLine.IsReplaced;
                SupplierOrderLineEdited.DateReplaced = supplierOrderLine.DateReplaced;
                SupplierOrderLineEdited.ReplacedProductId = supplierOrderLine.ReplacedProductId;
                SupplierOrderLineEdited.ReplacedProductCode = supplierOrderLine.ReplacedProductCode;

                _db.SupplierOrderLinesEdited.Add(SupplierOrderLineEdited);
                _db.SaveChanges();
            }

            //Check order line and order line edited
            OrderManager ordMgr = new OrderManager(_db);

            //Get Order Line and order line edited
            var orderLine = ordMgr.GetOrderLineById(supplierOrderLine.OrderLineId);
            var orderLineEdited = ordMgr.GetOrderLineEditedByOrderLineId(supplierOrderLine.OrderLineId);

            if (orderLineEdited == null && orderLine != null)
            {
                OrderLineEdited OrderLineEdited = new OrderLineEdited();

                OrderLineEdited.OrderLineId = orderLine.Id;
                OrderLineEdited.OrderId = orderLine.OrderId;
                OrderLineEdited.ProductId = orderLine.ProductId;
                OrderLineEdited.ProductCode = orderLine.ProductCode;
                OrderLineEdited.SupplierId = orderLine.SupplierId;
                OrderLineEdited.SupplierTradingName = orderLine.SupplierTradingName;
                OrderLineEdited.Unit = orderLine.Unit;
                OrderLineEdited.Quantity = orderLine.Quantity;
                OrderLineEdited.UnitPrice = orderLine.UnitPrice;
                OrderLineEdited.Amount = orderLine.Amount;
                OrderLineEdited.VATAmount = orderLine.VATAmount;
                OrderLineEdited.IsCompleted = orderLine.IsCompleted;
                OrderLineEdited.IsOrdered = orderLine.IsOrdered;
                OrderLineEdited.IsReplaced = orderLine.IsReplaced;
                OrderLineEdited.ReplacedProductId = orderLine.ReplacedProductId;
                OrderLineEdited.ReplacedProductCode = orderLine.ReplacedProductCode;
                OrderLineEdited.DateReplaced = orderLine.DateReplaced;

                _db.OrderLinesEdited.Add(OrderLineEdited);
                _db.SaveChanges();
            }
        }
    }
}