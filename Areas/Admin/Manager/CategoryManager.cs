﻿using Suppliro.Areas.Admin.Data;
using Suppliro.Context;
using Suppliro.Entities;
using Suppliro.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Suppliro.Areas.Admin.Manager
{
    public class CategoryManager
    {
        private DBContext _db;

        public CategoryManager(DBContext context)
        {
            _db = context;
        }

        public List<Category> GetAll()
        {
            CategoryData _data = new CategoryData(_db);
            return _data.GetAll();
        }

        public CategoryListViewModel GetCategoryListViewModel()
        {
            CategoryData _data = new CategoryData(_db);

            CategoryListViewModel vm = new CategoryListViewModel
            {
                CategoryList = SplitList(_data.GetAll(), null)
            };

            return vm;
        }

        /// <summary>
        /// Breaks the list into groups with each group containing no more than the specified group size
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="values">The values.</param>
        /// <param name="groupSize">Size of the group.</param>
        /// <returns></returns>
        public static List<List<T>> SplitList<T>(IEnumerable<T> values, int? maxCount = null)
        {
            int groupSize = (values.Count() - 1) / 3 + 1;

            List<List<T>> result = new List<List<T>>();
            // Quick and special scenario
            if (values.Count() <= groupSize)
            {
                result.Add(values.ToList());
            }
            else
            {
                List<T> valueList = values.ToList();
                int startIndex = 0;
                int count = valueList.Count;
                int elementCount = 0;

                while (startIndex < count && (!maxCount.HasValue || (maxCount.HasValue && startIndex < maxCount)))
                {
                    elementCount = (startIndex + groupSize > count) ? count - startIndex : groupSize;
                    result.Add(valueList.GetRange(startIndex, elementCount));
                    startIndex += elementCount;
                }
            }


            return result;
        }

        public Category GetCategoryByName(string name)
        {
            CategoryData _data = new CategoryData(_db);
            return _data.GetCategoryByName(name);
        }

        public Category GetCategoryById(int id)
        {
            CategoryData _data = new CategoryData(_db);
            return _data.GetCategoryById(id);
        }

        public Category AddCategory(Category model)
        {
            CategoryData _data = new CategoryData(_db);
            return _data.UpdateCategory(model);
        }
    }
}