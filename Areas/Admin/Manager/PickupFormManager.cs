﻿using Suppliro.Areas.Admin.Data;
using Suppliro.Areas.Admin.ViewModels;
using Suppliro.Context;
using Suppliro.Entities;
using System;
using System.Linq;

namespace Suppliro.Areas.Admin.Manager
{
    public class PickupFormManager
    {
        private DBContext _db;
        private PickupFormData data;

        public PickupFormManager(DBContext context)
        {
            _db = context;
            data = new PickupFormData(_db);
        }

        public PickUpForm GetOpenPickUpForm()
        {
            return data.GetOpenPickUpForm();
        }

        public PickUpFormViewModel GetPickUpFormViewModel()
        {
            PickUpFormViewModel vm = new PickUpFormViewModel
            {
                CurrentPickUpForm = data.GetOpenPickUpForm(),
                PickUpFormHistory = data.GetClosedPickUpForms()
            };

            return vm;
        }

        public PickUpForm GetPickUpFormById(int id)
        {
            PickUpForm model = data.GetPickUpFormById(id);

            model = new PickUpForm
                   {
                       Id = model.Id,
                       DateMailed = model.DateMailed,
                       DateCompleted = model.DateCompleted,
                       DateCreated = model.DateCreated,
                       IsComplete = model.IsComplete,
                       IsEmailed = model.IsEmailed,
                       PickUpFormLines = model.PickUpFormLines
                                       .OrderBy(y => y.SupplierTradingName)
                                       .ThenBy(y => y.ProductCode)
                                       .ThenBy(y => y.CustomerCode).ToList()
                   };

            return model;
        }

        public void MarkComplete(int id)
        {
            PickupFormData data = new PickupFormData(_db);

            var model = data.GetPickUpFormById(id);

            if (!model.IsComplete)
            {
                model.IsComplete = true;
                model.DateCompleted = DateTime.Now;
            }
            
            data.Update(model);
        }

        public void Update(PickUpForm model)
        {
            data.Update(model);
        }
    }
}