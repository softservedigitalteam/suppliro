﻿using Suppliro.Areas.Admin.Data;
using Suppliro.Areas.Admin.ViewModels;
using Suppliro.Context;
using Suppliro.Data;
using Suppliro.Entities;
using Suppliro.Manager;
using Suppliro.Search;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Suppliro.Areas.Admin.Manager
{
    public class ProductManager
    {
        private DBContext _db;

        public ProductManager(DBContext context)
        {
            _db = context;
        }
        public Product GetProductById(int id)
        {
            AdminProductData _prdData = new AdminProductData(_db);
            return _prdData.GetProductById(id);
        }

        public List<Product> GetAllProductsByCategoryId(int id)
        {
            AdminProductData data = new AdminProductData(_db);
            return data.GetAllProductsByCategoryId(id);
        }
        public List<Product> GetAllProducts()
        {
            AdminProductData _prdData = new AdminProductData(_db);
            return _prdData.GetAllProducts();
        }
        public void SaveProduct(Product model)
        {
            AdminProductData _prdData = new AdminProductData(_db);
            _prdData.SaveProduct(model);
        }

        public void SaveProduct(ProductViewModel model)
        {
            AdminProductData _prdData = new AdminProductData(_db);
            UoMManager uomMgr = new UoMManager(_db);
            CategoryManager catMgr = new CategoryManager(_db);
            var uomList = uomMgr.GetUoMAll();
            Product product = new Product();

            if (model.Id > 0)
                product = _prdData.GetProductById(model.Id);

            product.Id = model.Id;
            product.Code = model.Code;
            product.DisplayName = model.DisplayName;
            product.Description = model.Description;
            product.OriginalPrice = model.OriginalPrice;
            product.MarkUpPercentage = model.MarkUpPercentage;
            product.UoMId = model.UoMId;
            product.MinOrder = model.MinOrder;
            product.InStock = model.InStock;
            product.CategoryId = model.CategoryId;
            product.SupplierId = model.SupplierId;

            //Set UoM
            product.Unit = uomList.FirstOrDefault(x => x.Id == model.UoMId).Code ?? string.Empty;

            //Set Category
            var category = catMgr.GetCategoryById(model.CategoryId);
            product.CategoryName = category == null ? category.Name : string.Empty;

            try
            {
                if (model.UoMId > 0)
                {
                    //Set product UoM
                    var uom = uomList.Where(x => x.Id == model.UoMId).FirstOrDefault();
                    if (uom != null)
                    {
                        product.UoMId = uom.Id;
                        if (uom.DefaultUoMId > 0)
                        {
                            //Then we apply default value
                            var defaultUoM = uomList.Where(x => x.Id == uom.UoMMappers.FirstOrDefault(y => y.Id == uom.DefaultUoMId).UoMId).FirstOrDefault().UoMMappers.FirstOrDefault();
                            if (defaultUoM != null)
                            {
                                product.UoMDefaultId = defaultUoM.Id;
                                product.UoMDefaultCode = defaultUoM.CompareToUoMCode;

                                //If a product is sold in a case / packs then we calculate the price on a product level and then apply 
                                //the UoM Conversion
                                if (product.UoMValue > 0)
                                {
                                    var unitPrice = product.MarkedUpPrice / product.MinOrder;
                                    product.UoMDefaultPrice = Math.Round((unitPrice / product.UoMValue) * defaultUoM.Value, 2);
                                }
                                else
                                {
                                    product.UoMDefaultPrice = Math.Round((product.MarkedUpPrice / product.MinOrder)
                                        * defaultUoM.Value, 2);
                                }
                            }
                        }
                        else
                        {
                            //No default so we work out with itself
                            product.UoMDefaultCode = uom.Code;
                            product.UoMDefaultId = uom.Id;
                            product.UoMDefaultPrice = Math.Round(product.MarkedUpPrice / product.MinOrder, 2);
                        }

                        product.UoMDefaultPriceValue = string.Format("R {0}/{1}", product.UoMDefaultPrice, product.UoMDefaultCode);
                    }
                }

                this.CalucualteMarkUpValues(product);

                var savedProd = _prdData.SaveProduct(product);

                GoLucene.AddUpdateLuceneIndex(new SearchModel()
                {
                    ProductId = savedProd.Id,
                    Code = string.IsNullOrWhiteSpace(savedProd.DisplayName) ? savedProd.Code : savedProd.DisplayName,
                    SupplierCode = savedProd.Supplier.RegisteredName,
                    CategoryCode = savedProd.Category.Name,
                    MarkedUpPrice = savedProd.MarkedUpPrice,
                    MinOrder = savedProd.MinOrder,
                    Unit = savedProd.Unit,
                    UoMValue = savedProd.UoMValue,
                    UoMDefaultPriceValue = savedProd.UoMDefaultPriceValue
                });
            }
            catch (Exception ex)
            {

            }

        }

        public ProductViewModel GetProductViewModel(int? id)
        {
            ProductViewModel _vm = new ProductViewModel();

            AdminProductData _prodData = new AdminProductData(_db);
            CategoryData _catData = new CategoryData(_db);
            SupplierData _supData = new SupplierData(_db);
            UoMData _uomData = new UoMData(_db);

            Product prod = new Product();
            if (id.HasValue)
                prod = _prodData.GetProductById(id.Value);

            _vm.Id = prod.Id;
            _vm.Code = prod.Code;
            _vm.DisplayName = prod.DisplayName;
            _vm.Description = prod.Description;
            _vm.OriginalPrice = prod.OriginalPrice;
            _vm.MarkedUpPrice = prod.MarkedUpPrice;
            _vm.MarkUpPercentage = prod.MarkUpPercentage;
            _vm.MinOrder = prod.MinOrder;
            _vm.InStock = prod.InStock;
            _vm.CategoryId = prod.CategoryId;
            _vm.SupplierId = prod.SupplierId;
            _vm.UoMId = prod.UoMId;
            _vm.UoMDefaultId = prod.UoMDefaultId;
            _vm.UoMDefaultCode = prod.UoMDefaultCode;
            _vm.UoMDefaultPriceValue = prod.UoMDefaultPriceValue;

            _vm.Categories = _catData.GetCategoryDropDown();
            _vm.Suppliers = _supData.GetSuppliersDropDown();
            _vm.UoMs = _uomData.GetUoMDropDown();

            return _vm;
        }

        public ProductListViewModel GetProductListViewModel()
        {
            ProductListViewModel _vm = new ProductListViewModel();
            SupplierData _splData = new SupplierData(_db);
            AdminProductData _prdData = new AdminProductData(_db);

            _vm.Products = _prdData.GetAllProducts();
            _vm.Suppliers = _splData.GetSuppliersDropDown();

            return _vm;
        }

        public void UpdateProducts(List<Product> Products, string supplierId)
        {
            List<Product> existingProduct = this.FindExistingProduct(Convert.ToInt32(supplierId));
            CategoryManager _cgtMgr = new CategoryManager(_db);

            //This will only run once in the apps lifetime
            var category = _cgtMgr.GetCategoryByName("Default");
            if (category == null)
            {
                Category cat = new Category
                {
                    Name = "Default"
                };
                _cgtMgr.AddCategory(cat);
                category = cat;
            }

            try
            {
                List<Product> updatedProducs = new List<Product>();
                foreach (var product in Products)
                {
                    var exisitingProd = existingProduct.Where(x => x.Code.ToLower() == product.Code.ToLower()).FirstOrDefault();
                    //Doesn't exist so add it
                    if (exisitingProd == null)
                    {
                        product.Category = category;
                        product.MinOrder = 1;
                        CalucualteMarkUpValues(product);
                        updatedProducs.Add(product);
                    }
                    else
                    {
                        //Does exist but no price so should be out of stock
                        if (product.OriginalPrice == null || product.OriginalPrice <= 0)
                        {
                            exisitingProd.InStock = false;
                        }
                        //Set new price
                        exisitingProd.OriginalPrice = product.OriginalPrice;
                        exisitingProd.InStock = true;

                        CalucualteMarkUpValues(exisitingProd);
                        updatedProducs.Add(exisitingProd);

                        //We remove all products which have been updated
                        existingProduct.Remove(exisitingProd);
                    }
                }

                //Save all the products
                this.SaveProductList(updatedProducs);

                //Which ever products are left have not been included in the file therefore they are out of stock and should be updated accordingly
                existingProduct.ForEach(x => x.InStock = false);

                this.SaveProductList(existingProduct);

                //Update Search Index
                UpdateSearchIndex();
            }
            catch (Exception ex)
            {

            }
        }

        public void UploadProducts(List<Product> Products, string supplierId)
        {
            CategoryManager _cgtMgr = new CategoryManager(_db);
            UoMManager uomMgr = new UoMManager(_db);
            var categoryList = _cgtMgr.GetAll();
            var uomList = uomMgr.GetUoMAll();
            List<Product> FindExistingProduct = this.FindExistingProduct(Convert.ToInt32(supplierId));
            List<Product> ProductsToSaveList = new List<Product>();

            try
            {
                foreach (var product in Products)
                {
                    try
                    {
                        //Find category and if it doesn't exist then create it
                        var uploadedCat = product.CategoryName;
                        Category _cat = categoryList.Where(x => x.Name == uploadedCat).FirstOrDefault();

                        if (_cat == null)
                        {
                            _cat = new Category
                            {
                                Name = product.CategoryName,
                                Description = product.CategoryName
                            };

                            _cgtMgr.AddCategory(_cat);
                            categoryList.Add(_cat);
                        }

                        //See if product exists
                        Product prod = FindExistingProduct.Where(x => x.Code.ToLower().Trim() == product.Code.ToLower().Trim()).FirstOrDefault();
                        if (prod == null)
                        {
                            CalucualteMarkUpValues(product);
                            product.Category = _cat;

                            //Set product UoM
                            var uom = uomList.Where(x => x.Code.ToLower().Equals(product.Unit.ToLower())).FirstOrDefault();
                            if (uom != null)
                            {
                                product.UoMId = uom.Id;
                                if (uom.DefaultUoMId > 0)
                                {
                                    //Then we apply default value
                                    var defaultUoM = uomList.Where(x => x.Id == uom.UoMMappers.FirstOrDefault(y => y.Id == uom.DefaultUoMId).UoMId).FirstOrDefault().UoMMappers.FirstOrDefault();
                                    if (defaultUoM != null)
                                    {
                                        product.UoMDefaultId = defaultUoM.Id;
                                        product.UoMDefaultCode = defaultUoM.CompareToUoMCode;

                                        //If a product is sold in a case / packs then we calculate the price on a product level and then apply 
                                        //the UoM Conversion
                                        if (product.UoMValue > 0)
                                        {
                                            var unitPrice = product.MarkedUpPrice / product.MinOrder;
                                            product.UoMDefaultPrice = Math.Round((unitPrice / product.UoMValue) * defaultUoM.Value, 2);
                                        }
                                        else
                                        {
                                            product.UoMDefaultPrice = Math.Round((product.MarkedUpPrice / product.MinOrder)
                                                * defaultUoM.Value, 2);
                                        }
                                    }
                                }
                                else
                                {
                                    //No default so we work out with itself
                                    product.UoMDefaultCode = uom.Code;
                                    product.UoMDefaultId = uom.Id;
                                    product.UoMDefaultPrice = Math.Round(product.MarkedUpPrice / product.MinOrder, 2);
                                }

                                product.UoMDefaultPriceValue = string.Format("R {0}/{1}", product.UoMDefaultPrice, product.UoMDefaultCode);
                            }

                            product.InStock = true;

                            ProductsToSaveList.Add(product);
                        }
                    }
                    catch (Exception ex)
                    {

                    }
                }

                //Save new products
                this.SaveProductList(ProductsToSaveList);

                //Update Search Index
                UpdateSearchIndex();
            }
            catch (Exception ex)
            {

            }
        }

        public void ClearSearchIndex()
        {
            GoLucene.ClearLuceneIndex();
        }

        public void UpdateSearchIndex()
        {
            AdminProductData _prdData = new AdminProductData(_db);
            GoLucene.AddUpdateLuceneIndex(_prdData.GetAllProductsForLuceneSearch());
        }

        private void SaveProductList(List<Product> modelList)
        {
            AdminProductData _prdData = new AdminProductData(_db);
            _prdData.SaveProductList(modelList);
        }

        private Product CalucualteMarkUpValues(Product model)
        {
            //Calculate MarkUp percentage
            if (model.OriginalPrice > 200)
            {
                var per = 0.045;
                model.MarkUpPercentage = Math.Round(Convert.ToDouble(per - (1.25 * ((model.OriginalPrice - 200) / 100000))), 2);
            }
            else
                model.MarkUpPercentage = 0.045;

            model.MarkedUpPrice = Math.Round(Convert.ToDouble(model.OriginalPrice + (model.OriginalPrice * model.MarkUpPercentage)), 2);

            return model;
        }

        public List<Product> GetCustomerFrequentBoughtProduct(int customerId)
        {
            OrderManager ordMgr = new OrderManager(_db);
            AdminProductData prodData = new AdminProductData(_db);
            Dictionary<int, int> productIds = ordMgr.GetOrdersWithGroupedProducts(customerId);
            List<Product> products = new List<Product>();


            foreach (var prod in productIds)
            {
                var product = prodData.GetProductByIdForFSB(prod.Key);
                if (product != null)
                {
                    product.Code = string.IsNullOrEmpty(product.DisplayName) ? product.Code : product.DisplayName;
                    products.Add(product);
                }
            }

            return products;
        }

        private List<Product> FindExistingProduct(int supplierId)
        {
            AdminProductData _prdData = new AdminProductData(_db);
            return _prdData.GetAllProductsBySupplierId(supplierId);
        }

        public List<Product> GetStockSheet(int customerId)
        {
            ProductData _data = new ProductData();

            var list = _data.GetStockSheetByCustomerId(customerId);

            list.ForEach(x =>
            {
                x.Code = string.IsNullOrEmpty(x.DisplayName) ? x.Code : x.DisplayName;
            });

            return list;
        }

        public bool AddProductToStockSheet(int productId, int customerId, string userId)
        {
            bool _isAdded = false;
            ProductData _data = new ProductData();
            //we find the existing stock sheet item to see if it exists
            var stockSheetItem = _data.GetStockSheetItemById(productId, customerId, userId);

            //we add item to stock sheet
            if(stockSheetItem == null)
            {
                var newItem = new StockSheet
                {
                    ProductId = productId,
                    UserId = userId,
                    CustomerId = customerId,
                    DateAdded = DateTime.Now
                };

                _data.SaveStockSheetItem(newItem);
                _isAdded = true;
            }

            return _isAdded;
        }

        public bool RemoveFromStockSheet(int productId, int customerId, string userId)
        {
            ProductData _data = new ProductData();
            return _data.RemoveStockSheetItem(productId, customerId, userId);
        }
    }
}