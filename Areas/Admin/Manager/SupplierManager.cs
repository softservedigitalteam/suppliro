﻿using Suppliro.Areas.Admin.Data;
using Suppliro.Areas.Admin.ViewModels;
using Suppliro.Context;
using Suppliro.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Suppliro.Areas.Admin.Manager
{
    public class SupplierManager
    {
        private DBContext _db;

        public SupplierManager(DBContext context)
        {
            _db = context;
        }

        public Supplier GetSupplier(int? id)
        {
            Supplier _supplier = new Supplier();

            //Edit Supplier
            if (id.HasValue)
            {
                SupplierData _data = new SupplierData(_db);
                _supplier = _data.GetSupplierById(id.Value);
            }
            else
                //Add new supplier
                _supplier = new Supplier();

            return _supplier;
        }

        public Supplier AddSupplier(Supplier model)
        {
            SupplierData _data = new SupplierData(_db);
            _data.EditSupplier(model);
            return model;
        }

        public SupplierListViewModel GetAllSuppliers()
        {
            SupplierData _data = new SupplierData(_db);
            SupplierListViewModel _vm = new SupplierListViewModel
            {
                Suppliers = _data.GetAll()
            };
            return _vm;
        }
    }
}