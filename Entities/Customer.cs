﻿using Suppliro.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Suppliro.Entities
{
    public class Customer
    {
        [Key]
        public int Id { get; set; }

        public string UserId { get; set; }

        public string CustomerCode { get; set; }

        public string Color { get; set; }

        [ForeignKey("UserId")]
        public ApplicationUser User { get; set; }

        [Display(Name = "Trading Name")]
        public string TradingName { get; set; }

        [Display(Name = "Registered Name")]
        public string RegisteredName { get; set; }

        [Display(Name = "VAT Number")]
        public string VATNumber { get; set; }

        [Display(Name = "Tel Number")]
        public string TelNumber { get; set; }

        [Display(Name = "Cellphone")]
        public string CellNumber { get; set; }

        [Display(Name = "Buyer Name")]
        public string BuyersName { get; set; }

        [Display(Name = "Buyer ID")]
        public string BuyersIDNumber { get; set; }

        [Display(Name = "Email")]
        public string EmailAddress { get; set; }

        [Display(Name = "Fax")]
        public string FaxNumber { get; set; }

        #region Physical Address
        [Display(Name = "Physical Address")]
        public string PhysicalAddress1 { get; set; }

        [Display(Name = "Physical Address")]
        public string PhysicalAddress2 { get; set; }

        [Display(Name = "Postal Code")]
        public string PhysicalPostalCode { get; set; }

        [Display(Name = "Region")]
        public string PhysicalRegion { get; set; }

        [Display(Name = "City")]
        public string PhysicalTown { get; set; }

        #endregion

        #region Postal Address
        [Display(Name = "Postal Address")]
        public string PostalAddress1 { get; set; }

        [Display(Name = "Postal Address")]
        public string PostalAddress2 { get; set; }

        [Display(Name = "Postal Code")]
        public string PostalPostalCode { get; set; }

        [Display(Name = "Region")]
        public string PostalRegion { get; set; }

        [Display(Name = "City")]
        public string PostalTown { get; set; }

        #endregion
    }
}