﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Suppliro.Entities
{
    public class Employee
    {
        [Key]
        public int Id { get; set; }

        public int CompanyId { get; set; }

        public string Name { get; set; }

        public string Surname { get; set; }

        public int IDNumber { get; set; }

        public string Email { get; set; }

        public string PersonalEmail { get; set; }

        public string Position { get; set; }

        public bool IsDirector { get; set; }

        public string CelNumber { get; set; }

        public string TelNumber { get; set; }
    }
}