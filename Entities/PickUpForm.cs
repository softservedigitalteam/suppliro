﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Suppliro.Entities
{
    public class PickUpForm
    {
        public int Id { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime DateCompleted { get; set; }

        public bool IsComplete { get; set; }

        public bool IsEmailed { get; set; }

        public DateTime DateMailed { get; set; }

        public List<PickUpFormLine> PickUpFormLines { get; set; }
    }
}