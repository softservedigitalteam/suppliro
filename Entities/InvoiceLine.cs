﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Suppliro.Entities
{
    public class InvoiceLine
    {
        [Key]
        public int Id { get; set; }

        public int InvoiceId { get; set; }

        public int OrderId { get; set; }

        public int OrderLineId { get; set; }

        public string OrderNumber { get; set; }

        public DateTime DateAdded { get; set; }

        public int ProductId { get; set; }

        public string ProductCode { get; set; }

        public string ProductUnit { get; set; }

        public double ProductUnitPrice { get; set; }

        public double Quantity { get; set; }

        public double SubTotal { get; set; }

        public double VATAmount { get; set; }

        public double TotalAmount { get; set; }

        public bool IsReplaced { get; set; }

        public DateTime? DateReplaced { get; set; }

        public int? ReplacedProductId { get; set; }

        public string ReplacedProductCode { get; set; }
    }
}