﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Suppliro.Entities
{
    public class SupplierOrderNumber
    {
        public int Id { get; set; }

        public int Number { get; set; }
    }
}