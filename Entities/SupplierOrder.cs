﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Suppliro.Entities
{
    public class SupplierOrder
    {
        [Key]
        public int Id { get; set; }

        public int SupplierId { get; set; }

        public string SupplierEmails { get; set; }

        public Supplier Supplier { get; set; }

        public string OrderNumber { get; set; }

        public DateTime DateCreated { get; set; } 
        
        public bool IsEmailed { get; set; }

        public bool IsComplete { get; set; }
        
        public DateTime? DateMailed { get; set; }       

        public string SupplierInvoiceNumber { get; set; }

        public double? TotalValue { get; set; }

        public double? TotalMarkedupValue { get; set; }

        public DateTime? DateCompleted { get; set; }

        public List<SupplierOrderLine> SupplierOrderLines { get; set; }
        public List<SupplierOrderLineEdited> SupplierOrderLinesEdited { get; set; }
        public bool Edited { get; set; }
    }
}