﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Suppliro.Entities
{
    public class SupplierOrderLine
    {
        [Key]
        public int Id { get; set; }

        public int SupplierOrderId { get; set; }

        public int OrderId { get; set; }

        public int OrderLineId { get; set; }      
        
        public string OrderNumber { get; set; }  

        public int CustomerId { get; set; }

        public Customer Customer { get; set; }

        public string CustomerName { get; set; }

        public string CustomerColor { get; set; }

        public bool IsReplaced { get; set; }

        public DateTime? DateReplaced { get; set; }

        public int? ReplacedProductId { get; set; }

        public string ReplacedProductCode { get; set; }

        public int ProductId { get; set; }

        public string ProductCode { get; set; }

        public double ProductPrice { get; set; }

        public string ProductUnit { get; set; }

        public double Quantity { get; set; }
    }
}