﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Suppliro.Entities
{
    public class UoM
    {
        public int Id { get; set; }

        [Display(Name = "Unit of Measure")]
        public string Code { get; set; }

        public int DefaultUoMId { get; set; }

        public List<UoMMapper> UoMMappers { get; set; }
    }
}