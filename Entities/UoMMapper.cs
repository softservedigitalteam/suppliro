﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Suppliro.Entities
{
    public class UoMMapper
    {
        public int Id { get; set; }

        public int UoMId { get; set; }

        public UoM UoM { get; set; }

        public int CompareToUoMId { get; set; }

        public string CompareToUoMCode { get; set; }

        public double Value { get; set; }
    }
}