﻿using Suppliro.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Suppliro.Entities
{
    public class Product
    {
        [Key]
        public int Id { get; set; }

        public string Code { get; set; }

        public string DisplayName { get; set; }

        public string Description { get; set; }        

        public double? OriginalPrice { get; set; }

        public double MarkUpPercentage { get; set; }

        public double MarkedUpPrice { get; set; }

        public int UoMId { get; set; }

        public string Unit { get; set; }

        public int UoMDefaultId { get; set; }

        public string UoMDefaultCode { get; set; }

        public double UoMDefaultPrice { get; set; }

        public string UoMDefaultPriceValue { get; set; }

        public double UoMValue { get; set; }

        public double MinOrder { get; set; }      

        public double Vat { get; set; }

        public bool InStock { get; set; }
        
        public int CategoryId { get; set; }

        public string CategoryName { get; set; }

        [ForeignKey("CategoryId")]
        public Category Category { get; set; }

        public int SupplierId { get; set; }

        [ForeignKey("SupplierId")]
        public Supplier Supplier { get; set; }

        public int? RatingCount { get; set; }

        public decimal? RatingValue { get; set; }

        public decimal? RatingAverage { get; set; }
    }
}