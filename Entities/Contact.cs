﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Suppliro.Entities
{
    public class Contact
    {
        public int Id { get; set; }

        public string FullName { get; set; }
        
        public int? CustomerId { get; set; }

        public Customer Customer { get; set; }

        public string Email { get; set; }

        public string Message { get; set; }
    }
}