﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Suppliro.Entities
{
    public class CompanyInformation
    {
        [Key]
        public int Id { get; set; }

        [Display(Name = "Trading Name")]
        public string TradingName { get; set; }

        [Display(Name = "Registered Name")]
        public string RegisteredName { get; set; }

        [Display(Name = "VAT Number")]
        public string VATNumber { get; set; }

        [Display(Name = "Tel Number")]
        public string TelNumber { get; set; }

        public double VATPercentage { get; set; }

        #region Account Information
        [Display(Name = "Account Name")]
        public string AccountName { get; set; }

        [Display(Name = "Account Number")]
        public string AccountNumber { get; set; }

        [Display(Name = "Bank Name")]
        public string BankName { get; set; }

        [Display(Name = "Branch Code")]
        public string BranchCode { get; set; }

        [Display(Name = "Branch Name")]
        public string BranchName { get; set; }
        #endregion

        #region Address
        [Display(Name = "Physical Address")]
        public string Address1 { get; set; }

        [Display(Name = "Physical Address")]
        public string Address2 { get; set; }

        [Display(Name = "Postal Code")]
        public string PostalCode { get; set; }

        [Display(Name = "Region")]
        public string Region { get; set; }

        [Display(Name = "City")]
        public string City { get; set; }

        #endregion

        public List<Employee> Employees { get; set; }
    }
}