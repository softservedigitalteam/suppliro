﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Suppliro.Entities
{
    public class OrderNumber
    {
        public int OrderNumberId { get; set; }

        public int Number { get; set; }
    }
}