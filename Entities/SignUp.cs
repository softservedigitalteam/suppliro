﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Suppliro.Entities
{
    public class SignUp
    {
        [Key]
        public int Id { get; set; }

        public string BusinessName { get; set; }

        public string ContactPerson { get; set; }

        public string Number { get; set; }

        public string Email { get; set; }
    }
}