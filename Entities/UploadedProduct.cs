﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Suppliro.Entities
{
    public class UploadedProduct
    {
        public int UploadedProductId { get; set; }

        public int ProductId { get; set; }

        public int SupplierId { get; set; }

        [ForeignKey("SupplierId")]
        public Supplier Supplier { get; set; }

        public string Category { get; set; }

        public string Code { get; set; }

        public string Unit { get; set; }

        public decimal MinOrder { get; set; }

        public double Price { get; set; }
    }
}