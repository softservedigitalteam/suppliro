﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Suppliro.Entities
{
    public class ProductHistory
    {
        [Key]
        public int Id { get; set; }

        public int ProductId { get; set; }

        public double Price { get; set; }

        public DateTime Date { get; set; }
    }
}