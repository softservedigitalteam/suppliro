﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Suppliro.Entities
{
    public class PickUpFormLine
    {
        public int Id { get; set; }

        public int PickUpFormId { get; set; }

        public int OrderId { get; set; }

        public int OrderLineId { get; set; }

        public int SupplierId { get; set; }

        public Supplier Supplier { get; set; }

        public string SupplierTradingName { get; set; }

        public int CustomerId { get; set; }

        public Customer Customer { get; set; }

        public string CustomerCode { get; set; }

        public int ProductId { get; set; }

        public string ProductUnit { get; set; }

        public string ProductCode { get; set; }

        public double Qty { get; set; }
    }
}