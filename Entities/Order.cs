﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Suppliro.Entities
{
    public class Order
    {
        [Key]
        public int Id { get; set; }

        public string OrderNumber { get; set; }

        public DateTime DatePlaced { get; set; }

        public double SubTotal { get; set; }

        public double VATAmount { get; set; }

        public double Total { get; set; }

        //We have company information for this particular order as details of a company may change
        public string UserId { get; set; }        

        public bool IsComplete { get; set; }

        public DateTime? DateCompleted { get; set; }

        public bool IsInvoiced { get; set; }

        public double PercentageComplete { get; set; }

        public int CustomerId { get; set; }

        public string CustomerName { get; set; }

        public virtual Customer Customer { get; set; }

        [Display(Name = "Trading Name")]
        public string TradingName { get; set; }

        [Display(Name = "Registered Name")]
        public string RegisteredName { get; set; }

        [Display(Name = "VAT Number")]
        public string VATNumber { get; set; }

        [Display(Name = "Tel Number")]
        public string TelNumber { get; set; }

        [Display(Name = "Buyer Name")]
        public string BuyersName { get; set; }

        [Display(Name = "Buyer ID")]
        public string BuyersIDNumber { get; set; }

        [Display(Name = "Email")]
        public string EmailAddress { get; set; }

        #region Physical Address
        [Display(Name = "Physical Address")]
        public string PhysicalAddress1 { get; set; }

        public string PhysicalAddress2 { get; set; }

        [Display(Name = "Postal Code")]
        public string PhysicalPostalCode { get; set; }

        [Display(Name = "Region")]
        public string PhysicalRegion { get; set; }

        [Display(Name = "City")]
        public string PhysicalTown { get; set; }
        #endregion

        public bool Edited { get; set; }
        public List<OrderLine> OrderLines { get; set; }
    }
}