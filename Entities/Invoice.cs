﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Suppliro.Entities
{
    public class Invoice
    {
        [Key]
        public int Id { get; set; }

        public string InvoiceNumber { get; set; }
        
        public double VATAmount { get; set; }

        public double SubTotalAmount { get; set; }

        public double TotalAmount { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime? DateCompleted { get; set; }

        public DateTime? DateEmailed { get; set; }

        public bool IsCompleted { get; set; }

        #region Customer Data
        public int CustomerId { get; set; }

        public string CustomerCode { get; set; }

        public virtual Customer Customer { get; set; }

        [Display(Name = "Customer Trading Name")]
        public string CustomerTradingName { get; set; }

        [Display(Name = "Customer Registered Name")]
        public string CustomerRegisteredName { get; set; }

        [Display(Name = "Customer VAT Number")]
        public string CustomerVATNumber { get; set; }
        
        [Display(Name = "Customer Physical Address")]
        public string CustomerPhysicalAddress1 { get; set; }

        public string CustomerPhysicalAddress2 { get; set; }

        [Display(Name = "Customer Postal Code")]
        public string CustomerPhysicalPostalCode { get; set; }

        [Display(Name = "Customer Region")]
        public string CustomerPhysicalRegion { get; set; }

        [Display(Name = "Customer City")]
        public string CustomerPhysicalTown { get; set; }
        #endregion

        #region Company Data
        public int CompanyId { get; set; }

        public virtual CompanyInformation Company { get; set; }

        [Display(Name = "Company Trading Name")]
        public string CompanyName { get; set; }

        [Display(Name = "Company VAT Number")]
        public string CompanyVATNumber { get; set; }

        [Display(Name = "Company Physical Address")]
        public string CompanyAddress1 { get; set; }

        public string CompanyAddress2 { get; set; }

        [Display(Name = "Company Postal Code")]
        public string CompanyPostalCode { get; set; }

        [Display(Name = "Company Region")]
        public string CompanyRegion { get; set; }

        [Display(Name = "Company City")]
        public string CompanyCity { get; set; }
        #endregion

        public List<InvoiceLine> InvoiceLines { get; set; }
    }
}