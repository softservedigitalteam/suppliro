﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Suppliro.Entities
{
    public class OrderLine
    {
        [Key]
        public int Id { get; set; }
        
        public int OrderId { get; set; }

        public int ProductId { get; set; }

        public string ProductCode { get; set; }

        public int SupplierId { get; set; }

        public string SupplierTradingName { get; set; }

        [ForeignKey("SupplierId")]
        public Supplier Supplier { get; set; }  
        
        public string Unit { get; set; }          

        public double Quantity { get; set; }

        public double UnitPrice { get; set; }

        public double Amount { get; set; }

        public double VATAmount { get; set; }

        public bool IsCompleted { get; set; }

        public bool IsOrdered { get; set; }

        public bool IsReplaced { get; set; }

        public int? ReplacedProductId { get; set; }

        public string ReplacedProductCode { get; set; }

        public DateTime? DateReplaced { get; set; }

        [ForeignKey("ProductId")]
        public virtual Product Product { get; set; }

        [ForeignKey("OrderId")]
        public virtual Order Order { get; set; }
    }
}